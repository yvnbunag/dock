import { createRootPathNormalizer } from '#/utils/normalizers'

import type table from 'table'

type TableData = Array<Array<unknown>>
type DataFormatter = (data: TableData)=> TableData

const rootPathNormalizer = createRootPathNormalizer()
const makeDirectoryDeterministic: DataFormatter
  = (rows) => rows.map((columns) => columns.map(rootPathNormalizer))

export function create(): (typeof table) {
  const actualTable: typeof table = jest.requireActual('table')
  const dataFormatters: Array<DataFormatter> = [makeDirectoryDeterministic]

  return {
    ...actualTable,
    table(data, ...args) {
      const formattedData = dataFormatters.reduce(
        (accumulatedData, format) => format(accumulatedData),
        data,
      )

      return actualTable.table(formattedData, ...args)
    },
  }
}
