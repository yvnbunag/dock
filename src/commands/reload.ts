import * as commander from 'commander'

import { reload } from '~/handlers'
import { createActionHandler, decorateWithDefaults } from '~/commands'
import { requireUse, requireUsedConfigFile } from '~/middlewares'
import { logDuration } from '~/lib/decorators'

import type { Create } from '@/command'

export const name = 'reload'

const description = 'Reload used project'

export const create: Create = (provisions) => {
  const handler = createActionHandler(
    logDuration(reload.handle, provisions.emitter, 'Reload'),
    reload.complete,
    provisions,
    [requireUse, requireUsedConfigFile],
  )
  const command = new commander.Command()
    .command(name)
    .description(description)
    .action(handler)

  return decorateWithDefaults(command, provisions)
}
