# Exit Codes Reference

## 0 - Success

Indicates a successful command execution

Also indicates a system completion

## 100 - Failed

Indicates a generic script failure

## 101 - Process Failed

Indicates a generic command handling failure

## 102 - Unknown

Indicates that the parameter/s are unknown

## 103 - Unsupported

Indicates that the parameter/s are unsupported

## 104 - Invalid

Indicates that the parameter/s are invalid and should follow specification

## 105 - Conflict

Indicates that operation with the parameter/s may not proceed due to a conflict
with another parameter

## 106 - Not Found

Indicates that the parameter/s does not exist

## 107 - Incorrect Usage

Indicates the incorrect usage of a command

## 108 - Incomplete

Indicates that the parameter/s are incomplete
