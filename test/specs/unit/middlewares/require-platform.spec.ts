import { requirePlatforms } from '~/middlewares'

import { Recorder } from '#/utils/recorder'
import {
  optionMediator as optionMediatorFactory,
  environmentReader as environmentReaderFactory,
} from '#/utils/mocks/lib'
import { emitter as emitterFactory } from '#/utils/mocks/clients'
import { NOOP } from '#/utils/general-helpers'
import { createOptions } from '#/utils/command'

const recorder = new Recorder()
const emitter = emitterFactory
  .create({ exit: <emitterFactory.Overrides['exit']>NOOP })
  .subscribe(emitterFactory.createRecorderHandler(recorder))
const optionMediator = optionMediatorFactory.create()
const environmentReader = environmentReaderFactory.create()
const provisions = { emitter, optionMediator, environmentReader }
const options = createOptions({ provisions })

describe(`'feature' argument is not provided`, () => {
  beforeEach(() => {
    jest.clearAllMocks()
    recorder.clearRecording()
    provisions.environmentReader.resetPlatform()
  })

  it('should continue if platform is supported', () => {
    const intercept = requirePlatforms(['linux'])

    provisions.environmentReader.setPlatform('linux')
    intercept([], options)

    expect(recorder.recording).toBeFalsy()
  })

  it('should exit with error if platform is not supported', () => {
    const intercept = requirePlatforms(['linux'])

    provisions.environmentReader.setPlatform('darwin')

    intercept([], options)

    expect(recorder.recording).toMatchSnapshot()
  })
})

describe(`'feature' argument is provided`, () => {
  beforeEach(() => {
    jest.clearAllMocks()
    recorder.clearRecording()
    provisions.environmentReader.resetPlatform()
  })

  it('should continue if platform is supported', () => {
    const intercept = requirePlatforms(['linux'])

    provisions.environmentReader.setPlatform('linux')
    intercept([], options)

    expect(recorder.recording).toBeFalsy()
  })

  it('should exit with error if platform is not supported', () => {
    const intercept = requirePlatforms(['linux'], 'custom feature name')

    provisions.environmentReader.setPlatform('darwin')

    intercept([], options)

    expect(recorder.recording).toMatchSnapshot()
  })
})
