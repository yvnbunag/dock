import os from 'os'

import * as root from 'app-root-path'

import { EnvironmentReader } from '~/lib/environment-reader'

import { createRootPathNormalizer } from '#/utils/normalizers'

const normalizeRootPath = createRootPathNormalizer()

describe('Properties', () => {
  describe('platform', () => {
    const platformSpy = jest.spyOn(os, 'platform')
    const environmentReader = new EnvironmentReader()

    beforeAll(() => {
      platformSpy.mockReturnValueOnce('mock-platform' as NodeJS.Platform)
    })
    afterAll(() => { platformSpy.mockRestore() })

    it('should get platform from os', () => {
      expect(environmentReader.platform).toMatchInlineSnapshot(
        `"mock-platform"`,
      )
    })
  })

  describe('memoize', () => {
    it('should return true if memoize is not defined in environment', () => {
      const environmentReader = new EnvironmentReader({ env: {} })

      expect(environmentReader.memoize).toBe(true)
    })

    it('should return true if memoize is true in environment', () => {
      const process = { env: { memoize: 'true' } }
      const environmentReader = new EnvironmentReader(process)

      expect(environmentReader.memoize).toBe(true)
    })

    it('should return false if memoize is false in environment', () => {
      const process = { env: { memoize: 'false' } }
      const environmentReader = new EnvironmentReader(process)

      expect(environmentReader.memoize).toBe(false)
    })

    it('should return false if memoize is neither true or false', () => {
      const process = { env: { memoize: 'unknown' } }
      const environmentReader = new EnvironmentReader(process)

      expect(environmentReader.memoize).toBe(false)
    })
  })

  describe('runPath', () => {
    it('should return current run path', () => {
      const environmentReader = new EnvironmentReader()
      const { runPath } = environmentReader

      expect(normalizeRootPath(runPath)).toMatchSnapshot()
    })
  })

  describe('runDirectory', () => {
    it('should return current run directory', () => {
      const cwd = jest.fn(() => `${root.path}/test`)
      const environmentReader = new EnvironmentReader({ cwd })
      const { runDirectory } = environmentReader

      expect(normalizeRootPath(runDirectory)).toMatchSnapshot()
    })
  })

  describe('scriptDirectory', () => {
    it('should return current script directory', () => {
      const environmentReader = new EnvironmentReader()
      const { scriptDirectory } = environmentReader

      expect(normalizeRootPath(scriptDirectory)).toMatchSnapshot()
    })
  })
})
