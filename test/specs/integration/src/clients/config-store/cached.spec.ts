import * as configStore from '~/clients/config-store/cached'
import * as mockConfigStore from '#/utils/mocks/clients/config-store'

import { getFirstArguments } from '#/utils/mock-helpers'

describe('ConfigProvider', () => {
  describe('Methods', () => {
    describe('get', () => {
      const source = mockConfigStore.createConfigProvider()
      const value = 'get-value'

      beforeAll(() => {
        source.get.mockResolvedValue(value)
      })

      describe('Value is not cached', () => {
        const cached = new configStore.ConfigProvider(source)
        let result: unknown

        beforeAll(async () => {
          result = await cached.get('uncached-key')
        })

        it('should get value from source', () => {
          expect(source.get).toHaveBeenCalledTimes(1)
        })

        it('should return value', () => {
          expect(result).toBe(value)
        })
      })

      describe('Value is cached', () => {
        const cached = new configStore.ConfigProvider(source)
        let cachedResult: unknown, result: unknown

        beforeAll(async () => {
          cachedResult = await cached.get('cached-key')

          jest.clearAllMocks()

          result = await cached.get('cached-key')
        })

        it('should not get value from source', () => {
          expect(source.get).not.toHaveBeenCalled()
        })

        it('should return cached value', async () => {
          expect(result).toBe(cachedResult)
        })
      })
    })

    describe('set', () => {
      const source = mockConfigStore.createConfigProvider()
      const cached = new configStore.ConfigProvider(source)
      const key = 'set-key'
      const value = 'set-value'
      let result: unknown

      beforeAll(async () => {
        source.set.mockResolvedValue(value)

        result = await cached.set(key, value)
      })

      it('should return set value', () => {
        expect(result).toBe(value)
      })

      it('should store value', () => {
        expect(source.set).toHaveBeenCalledTimes(1)
        expect(getFirstArguments(source.set)).toMatchSnapshot()
      })

      it('should cache value', async () => {
        const cachedValue = await cached.get(key)

        expect(cachedValue).toBe(value)
        expect(source.get).not.toHaveBeenCalled()
      })
    })

    describe('getAll', () => {
      const source = mockConfigStore.createConfigProvider()
      const key = 'get-all-key'
      const value = 'get-all-value'
      const config = { [key]: value }

      beforeAll(() => {
        source.getAll.mockResolvedValue(config)
      })

      describe('Config is not cached', () => {
        const cached = new configStore.ConfigProvider(source)
        let result: unknown

        beforeAll(async () => {
          result = await cached.getAll()
        })

        it('should get config from source', () => {
          expect(source.getAll).toHaveBeenCalledTimes(1)
        })

        it('should return config', () => {
          expect(result).toBe(config)
        })
      })

      describe('Config is partially cached', () => {
        const cached = new configStore.ConfigProvider(source)
        let result: unknown

        beforeAll(async () => {
          jest.clearAllMocks()

          await cached.get(key)

          result = await cached.getAll()
        })

        it('should get config from source', () => {
          expect(source.getAll).toHaveBeenCalledTimes(1)
        })

        it('should return config', () => {
          expect(result).toBe(config)
        })
      })

      describe('Config is cached', () => {
        const cached = new configStore.ConfigProvider(source)
        let cachedResult: unknown, result: unknown

        beforeAll(async () => {
          cachedResult = await cached.getAll()

          jest.clearAllMocks()

          result = await cached.getAll()
        })

        it('should not get config from source', () => {
          expect(source.getAll).not.toHaveBeenCalled()
        })

        it('should return cached config', () => {
          expect(result).toBe(cachedResult)
        })
      })

      describe('It should cache individual config values', () => {
        const cached = new configStore.ConfigProvider(source)
        let result: unknown

        beforeAll(async () => {
          await cached.getAll()

          result = await cached.get(key)
        })

        it('should not get value from source', () => {
          expect(source.get).not.toHaveBeenCalled()
        })

        it('should return cached value', () => {
          expect(result).toBe(config[key])
        })
      })
    })

    describe('setAll', () => {
      const source = mockConfigStore.createConfigProvider()
      const cached = new configStore.ConfigProvider(source)
      const key = 'set-all-key'
      const value = 'set-all-value'
      const config = { [key]: value }
      let result: unknown

      beforeAll(async () => {
        source.setAll.mockResolvedValue(config)

        result = await cached.setAll(config)
      })

      it('should return set config', () => {
        expect(result).toBe(config)
      })

      it('should store config', () => {
        expect(source.setAll).toHaveBeenCalledTimes(1)
        expect(getFirstArguments(source.setAll)).toMatchSnapshot()
      })

      it('should cache config', async () => {
        const cachedConfig = await cached.getAll()

        expect(cachedConfig).toBe(config)
        expect(source.getAll).not.toHaveBeenCalled()
      })

      describe('It should cache individual config values', () => {
        let cachedResult: unknown

        beforeAll(async () => {
          cachedResult = await cached.get(key)
        })

        it('should not get value from source', () => {
          expect(source.get).not.toHaveBeenCalled()
        })

        it('should return cached value', () => {
          expect(cachedResult).toBe(config[key])
        })
      })
    })

    describe('clear', () => {
      const source = mockConfigStore.createConfigProvider()
      const cached = new configStore.ConfigProvider(source)
      const key = 'clear-key'
      const value = 'clear-value'
      const config = { [key]: value }

      beforeAll(async () => {
        source.getAll.mockResolvedValue(config)

        await cached.getAll()

        jest.clearAllMocks()

        await cached.clear()
      })

      it('should clear source', () => {
        expect(source.clear).toHaveBeenCalledTimes(1)
      })

      it('should clear cache', async () => {
        await cached.getAll()

        expect(source.getAll).toHaveBeenCalledTimes(1)
      })
    })
  })
})

describe('ConfigSource', () => {
  describe('Methods', () => {
    describe('getProvider', () => {
      const provider = mockConfigStore.createConfigProvider()

      describe('Provider is not cached', () => {
        const source = mockConfigStore.createConfigSource()
        const cached = new configStore.ConfigSource(source)
        let result: configStore.ConfigProvider

        beforeAll(async () => {
          source.getProvider.mockResolvedValue(provider)

          result = await cached.getProvider('uncached-provider')
        })

        it('should get provider from source', () => {
          expect(source.getProvider).toHaveBeenCalledTimes(1)
        })

        it('should return provider', () => {
          expect(result).toBeInstanceOf(configStore.ConfigProvider)
        })
      })

      describe('Provider is cached', () => {
        const source = mockConfigStore.createConfigSource()
        const cache = new configStore.ConfigSource(source)
        let cachedResult: configStore.ConfigProvider,
          result: configStore.ConfigProvider

        beforeAll(async () => {
          source.getProvider.mockResolvedValue(provider)

          cachedResult = await cache.getProvider('cached-provider')

          jest.clearAllMocks()

          result = await cache.getProvider('cached-provider')
        })

        it('should not get provider from source', () => {
          expect(source.getProvider).not.toHaveBeenCalled()
        })

        it('should return cached provider', async () => {
          expect(result).toBe(cachedResult)
          expect(cachedResult).toBeInstanceOf(configStore.ConfigProvider)
        })
      })
    })
  })
})