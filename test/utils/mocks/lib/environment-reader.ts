import * as root from 'app-root-path'

import {
  EnvironmentReader as EnvironmentReaderImplementation,
} from '~/lib/environment-reader'

type Overrides = {
  [key in keyof EnvironmentReaderImplementation]:
    EnvironmentReaderImplementation[key]
}
type SetAble<
  Type extends EnvironmentReaderImplementation,
  Properties extends keyof Type = keyof Type,
> = {
  [Key in Properties as `set${Capitalize<Key & string>}`]:
    (arg: Type[Key])=> Type
}
type ResetAble<
  Type extends EnvironmentReaderImplementation,
  Properties extends keyof Type = keyof Type,
> = {
  [Key in Properties as `reset${Capitalize<Key & string>}`]: ()=> Type
}

export type TestEnvironmentReader = EnvironmentReaderImplementation
  & SetAble<EnvironmentReaderImplementation, 'platform'>
  & ResetAble<EnvironmentReaderImplementation, 'platform'>

class EnvironmentReader
  extends EnvironmentReaderImplementation
  implements TestEnvironmentReader
{
  constructor(private overrides: Writable<Partial<Overrides>> = {}) {
    super()
  }

  get platform() {
    return this.overrides.platform ?? super.platform
  }

  get memoize() {
    return this.overrides.memoize ?? super.memoize
  }

  get runPath() {
    return this.overrides.runPath ?? super.runPath
  }

  get runDirectory() {
    return this.overrides.runDirectory ?? super.runDirectory
  }

  get scriptDirectory() {
    return this.overrides.scriptDirectory ?? `${root.path}/src`
  }

  setPlatform(platform: NodeJS.Platform) {
    this.overrides.platform = platform

    return this
  }

  resetPlatform() {
    this.overrides.platform = undefined

    return this
  }
}

export function create(
  overrides: Partial<Overrides> = {},
): TestEnvironmentReader {
  return new EnvironmentReader(overrides)
}
