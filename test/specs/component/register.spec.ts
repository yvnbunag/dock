import * as fallback from '~/config/fallback'

import {
  createTestInstances,
  caseFactory,
  FileSwitcher,
} from '#/specs/component/__lib__'
import { projects } from '#/specs/component/__lib__/test'
import { mapContextSwitch } from '#/utils/environment'

const { $configSource, dock, recorder } = createTestInstances()
const contexts = mapContextSwitch({
  ROOT: projects.root.location.ABSOLUTE,
  NO_CONFIG: projects.no_config.location.ABSOLUTE,
  GENERAL: projects.general.location.ABSOLUTE,
})

describe('Component - register [path]', () => {
  describe('Command', () => {
    describe('Path is not provided', () => {
      describe('Current directory', () => {
        describe('Valid config exists', () => {
          contexts.GENERAL.scope()

          afterAll($configSource.Class.wipe)

          it('should register successfully', async () => {
            recorder.clearRecording()

            await dock.run('register')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should be included in registered projects', async () => {
            recorder.clearRecording()

            await dock.run('projects --location-only')

            expect(recorder.recording).toMatchSnapshot()
          })
        })

        it('should fail registration if config does not exist', async () => {
          contexts.NO_CONFIG.switch()
          recorder.clearRecording()

          await dock.run('register')

          expect(recorder.recording).toMatchSnapshot()

          contexts.NO_CONFIG.reset()
        })
      })
    })

    describe('Path is relative', () => {
      contexts.ROOT.scope()

      describe('Path is valid', () => {
        describe('Valid config exists', () => {
          afterAll($configSource.Class.wipe)

          it('should register successfully', async () => {
            recorder.clearRecording()

            await dock.run(`register ${projects.general.location.RELATIVE}`)

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should be included in registered projects', async () => {
            recorder.clearRecording()

            await dock.run('projects --location-only')

            expect(recorder.recording).toMatchSnapshot()
          })
        })

        it('should fail registration if config does not exist', async () => {
          recorder.clearRecording()

          await dock.run(`register ${projects.no_config.location.RELATIVE}`)

          expect(recorder.recording).toMatchSnapshot()
        })
      })

      it('should fail registration if path is invalid', async () => {
        recorder.clearRecording()

        await dock.run(`register ${projects.non_existent.location.RELATIVE}`)

        expect(recorder.recording).toMatchSnapshot()
      })
    })

    describe('Path is absolute', () => {
      contexts.ROOT.scope()

      describe('Path is valid', () => {
        describe('Valid config exists', () => {
          afterAll($configSource.Class.wipe)

          it('should register successfully', async () => {
            recorder.clearRecording()

            await dock.run(`register ${projects.general.location.ABSOLUTE}`)

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should be included in registered projects', async () => {
            recorder.clearRecording()

            await dock.run('projects --location-only')

            expect(recorder.recording).toMatchSnapshot()
          })
        })

        it('should fail registration if config does not exist', async () => {
          recorder.clearRecording()

          await dock.run(`register ${projects.no_config.location.ABSOLUTE}`)

          expect(recorder.recording).toMatchSnapshot()
        })
      })

      it('should fail registration if path is invalid', async () => {
        recorder.clearRecording()

        await dock.run(`register ${projects.non_existent.location.ABSOLUTE}`)

        expect(recorder.recording).toMatchSnapshot()
      })
    })

    describe('Project is already registered', () => {
      contexts.ROOT.scope()

      beforeAll(async () => {
        await dock.run(`register ${projects.general.location.ABSOLUTE}`)
      })
      afterAll($configSource.Class.wipe)

      it('should fail registration for projects with same name', async () => {
        recorder.clearRecording()

        await dock.run(`register ${projects.general_copy.location.ABSOLUTE}`)

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should fail registration for projects in the same location', async () => {
        recorder.clearRecording()

        await dock.run(`register ${projects.general.location.ABSOLUTE}`)

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should fail registration for projects in the same location with an updated name', async () => {
        const project = projects['register/name/switched']
        const location = project.location.ABSOLUTE
        const { name } = fallback.project.CONFIG
        const cases = {
          INITIAL: 'dock.config.initial.yml',
          UPDATED: 'dock.config.updated.yml',
        }
        const fileSwitcher = new FileSwitcher(location, name, cases)

        await fileSwitcher.switch('INITIAL')
        await dock.run(`register ${location}`)
        recorder.clearRecording()
        await fileSwitcher.switch('UPDATED')

        await dock.run(`register ${location}`)

        expect(recorder.recording).toMatchSnapshot()

        await dock.run(`unregister ${project.name}`)
        await fileSwitcher.cleanup()
      })
    })

    describe('Registering multiple projects', () => {
      contexts.ROOT.scope()

      afterAll($configSource.Class.wipe)

      test('projects should register successfully', async () => {
        recorder.clearRecording()

        await dock.run(`register ${projects.general.location.ABSOLUTE}`)
        await dock.run(`register ${projects.alternative.location.ABSOLUTE}`)

        expect(recorder.recording).toMatchSnapshot()
      })

      test('projects should be included in registered projects', async () => {
        recorder.clearRecording()

        await dock.run('projects --location-only')

        expect(recorder.recording).toMatchSnapshot()
      })
    })
  })

  describe('Completion', () => {
    it('should complete with system completion if ran with path', async () => {
      recorder.clearRecording()

      await dock.complete('register ./path/to/project')

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should complete with system completion for all arguments', async () => {
      recorder.clearRecording()

      await dock.complete('register ')
      await dock.complete('register ./path/to/project ')

      expect(recorder.recording).toMatchSnapshot()
    })

    describe(
      'Options are provided before command arguments',
      caseFactory.completion.options.createProvidedBeforeArguments(
        'register',
        () => ({ dock, recorder }),
        ['./path/to/project', './path/to/another/project'],
      ),
    )

    describe(
      'Default options',
      caseFactory.completion.options.createDefault(
        'register',
        () => ({ dock, recorder }),
        ['./path/to/project', './path/to/another/project'],
      ),
    )
  })

  describe(
    'Version',
    caseFactory.version.createDefault('register', () => ({ dock })),
  )

  describe(
    'Help',
    caseFactory.help.createDefault('register', () => ({ dock })),
  )
})
