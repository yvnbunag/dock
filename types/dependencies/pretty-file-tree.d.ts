declare module 'pretty-file-tree' {
  function createPrettyFileTree(files: Array<string>): string

  export = createPrettyFileTree
}
