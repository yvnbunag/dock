import { access, readFile, stat } from 'fs/promises'

import chalk from 'chalk'

import { NOT_FOUND } from '~/config/exit-codes'
import { ValidationErrorSet } from '~/errors/validation-error'
import { toBulletedSentence } from '~/lib/print-formatter/list'

import type { Provisions } from '@/command'
import type { Updated } from '@/config/project'

export function exists(fileName: string): Promise<boolean> {
  return access(fileName)
    .then(() => true)
    .catch(() => false)
}

export function read(fileName: string): Promise<string> {
  return readFile(fileName, 'utf8')
}

export async function getUpdateDate(fileName: string): Promise<Updated> {
  const { mtime } = await stat(fileName)

  return mtime.toISOString().
    replace(/T/, ' '). // replace T with a space
    replace(/\..+/, '') // delete the dot and everything after
}

export function handleNotFound(
  fileName: string,
  provisions: Pick<Provisions, 'emitter'>,
): SideEffect<never> {
  return provisions.emitter
    .emitError(`Configuration file ${chalk.underline(fileName)} not found`)
    .exit(NOT_FOUND)
}

export function handleValidationError(
  errorSet: ValidationErrorSet,
  provisions: Provisions,
): SideEffect<never> {
  const formattedErrors = errorSet.errors.map(
    (error) => error.formatMessageIdentifier(chalk.underline),
  )
  const errorsList = toBulletedSentence(formattedErrors)

  return provisions.emitter
    .emitError(`Config schema validation failed with errors:\n${errorsList}`)
    .exit(errorSet.code)
}
