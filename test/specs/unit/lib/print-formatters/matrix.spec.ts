import {
  toStringMatrix,
  toTable,
  toIndentedStringMatrix,
} from '~/lib/print-formatter/matrix'
import { primitive } from '#/utils/values'

const variantEntries = Object
  .entries(primitive)
  .map(([key, value]) => [key.toLowerCase(), value])
const variantMatrix = [
  ['Type', 'Value'],
  ...variantEntries,
]

describe('toStringMatrix', () => {
  it('should transform matrix of varying types to matrix of strings', () => {
    const stringMatrix = toStringMatrix(variantMatrix)
    const nonStringValues = stringMatrix
      .flat()
      .filter((value) => typeof value !== 'string')

    expect(stringMatrix).toMatchSnapshot()
    expect(nonStringValues.length).toBe(0)
  })
})

describe('toTable', () => {
  it('should format to table a matrix with a single column', () => {
    const matrix = [
      ['A'],
      ['A0'],
      ['A1'],
    ]

    expect(toTable(matrix)).toMatchSnapshot()
  })

  it('should format to table a matrix with multiple columns', () => {
    const matrix = [
      ['A', 'B', 'C'],
      ['A1', 'B2', 'C3'],
      ['A1', 'B2', 'C3'],
    ]

    expect(toTable(matrix)).toMatchSnapshot()
  })

  it('should format to table a matrix with varying data types', () => {
    expect(toTable(variantMatrix)).toMatchSnapshot()
  })
})

describe('toIndentedStringMatrix', () => {
  const singleRowMatrix = [['A', 'B', 'C']]

  describe('It should format to indented string matrix', () => {
    test('a matrix with varying data types', () => {
      expect(toIndentedStringMatrix(variantMatrix)).toMatchSnapshot()
    })

    test('a matrix configured with default indentation', () => {
      const indentedStringMatix = toIndentedStringMatrix(singleRowMatrix)

      expect(indentedStringMatix).toMatchSnapshot()
    })

    test('a matrix configured with initial indentation', () => {
      const indentedStringMatix = toIndentedStringMatrix(
        singleRowMatrix,
        { initial: 5 },
      )

      expect(indentedStringMatix).toMatchSnapshot()
    })

    test('a matrix configured with left indentation per column', () => {
      const indentedStringMatix = toIndentedStringMatrix(
        singleRowMatrix,
        { left: 5 },
      )

      expect(indentedStringMatix).toMatchSnapshot()
    })

    test('a matrix configured with right indentation per column', () => {
      const indentedStringMatix = toIndentedStringMatrix(
        singleRowMatrix,
        { right: 5 },
      )

      expect(indentedStringMatix).toMatchSnapshot()
    })

    test('a matrix configured with all indentation options', () => {
      const indentedStringMatix = toIndentedStringMatrix(
        singleRowMatrix,
        { initial: 3, left: 3, right: 3 },
      )

      expect(indentedStringMatix).toMatchSnapshot()
    })
  })
})
