import { capitalize } from 'lodash'

import { exitCodes } from '~/config'
import { toSentence } from '~/lib/print-formatter/list'

import type { Middleware } from '@/middleware'

export function requirePlatforms(
  platforms: Array<NodeJS.Platform>,
  feature = 'Command',
): Middleware<'optionMediator' | 'environmentReader' | 'emitter'> {
  return async (commandArguments, { provisions }) => {
    const { platform } = provisions.environmentReader

    if (platforms.includes(platform)) return

    const message = {
      unsupportedPlatform:
        `${capitalize(feature)} unsupported for platform '${platform}'`,
      supportedPlatforms: `Supported platforms are ${toSentence(platforms)}`,
    }

    return provisions.emitter
      .emitError(message.unsupportedPlatform)
      .emitError(message.supportedPlatforms)
      .exit(exitCodes.UNSUPPORTED)
  }
}
