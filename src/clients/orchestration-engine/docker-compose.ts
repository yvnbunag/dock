import {
  OrchestrationEngine as AbstractBaseOrchestrationEngine,
} from '~/clients/orchestration-engine/abstract-base'
import { Shell } from '@/config/project'

import type { ContainerNames, ContainerName } from '@/config/project'
import type {
  OrchestrationEngineInterface,
  ContainerCommandOptions,
  ContainerCommands,
} from '@/clients/orchestration-engine'

export class OrchestrationEngine extends AbstractBaseOrchestrationEngine {
  private readonly _configFile = 'docker-compose.yml'

  build(
    containers: ContainerNames = [],
  ): Promise<OrchestrationEngineInterface> | never {
    return this.execute(['build', '--pull', ...containers])
  }

  start(
    containers: ContainerNames = [],
  ): Promise<OrchestrationEngineInterface> | never {
    return this.execute(['up', '--detach', ...containers])
  }

  stop(
    containers: ContainerNames = [],
  ): Promise<OrchestrationEngineInterface> | never {
    return this.execute(['stop', ...containers])
  }

  remove(
    containers: ContainerNames = [],
  ): Promise<OrchestrationEngineInterface> | never {
    return this.execute(['rm', '--force', '--stop', ...containers])
  }

  down(
    containers: ContainerNames = [],
  ): Promise<OrchestrationEngineInterface> | never {
    if (containers.length) {
      return this.stop(containers).then(() => this.remove(containers))
    }

    return this.execute(['down'])
  }

  run(
    container: ContainerName,
    commands: ContainerCommands,
    {
      shell = Shell.BASH,
      entryPoint,
    }: ContainerCommandOptions = {},
  ): Promise<OrchestrationEngineInterface> | never {
    const navigateToEntryPoint = entryPoint ? `cd ${entryPoint}`: ''
    const formattedCommands = commands
      .map((command) => OrchestrationEngine.escapeQuotes(command, '"'))
    const containerCommand = OrchestrationEngine.chainCommands([
      navigateToEntryPoint,
      ...formattedCommands,
    ])

    return this.execute([
      'exec',
      container,
      `${shell} -c "${containerCommand}"`,
    ])
  }

  open(
    container: ContainerName,
    {
      shell = Shell.BASH,
      entryPoint,
    }: Pick<ContainerCommandOptions, 'shell' | 'entryPoint'> = {},
  ): Promise<OrchestrationEngineInterface> | never {
    if (entryPoint) return this.run(container, [shell], { shell, entryPoint })

    return this.execute(['exec', container, shell])
  }

  private async execute(
    args: Array<string>,
  ): Promise<OrchestrationEngineInterface> | never {
    const command = OrchestrationEngine.joinArguments([
      'docker-compose',
      `--file ${this.configPath}/${this._configFile}`,
      ...args,
    ])

    await this.executeCommand([command])

    return this
  }
}
