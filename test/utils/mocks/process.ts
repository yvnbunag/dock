export function mockHrtime(secondsStep = 1, nanoSecondsStep = 0): SideEffect {
  const hrtimeSpy = jest.spyOn(process, 'hrtime')

  beforeAll(() => {
    hrtimeSpy.mockImplementation(
      ([seconds, nanoseconds] = [0, 0]) => {
        return [seconds + secondsStep, nanoseconds + nanoSecondsStep]
      },
    )
  })

  afterAll(() => {
    hrtimeSpy.mockRestore()
  })
}
