import { dirname, relative } from 'path'

export interface Location {
  ABSOLUTE: string,
  RELATIVE: string,
}
export interface Project {
  name: string,
  location: Location,
}
export type Projects = Record<string, Project>

function normalizeDirectory(directory: string): string {
  return directory.toLowerCase().replace('_', '-')
}

function createLocation(
  directory = '',
  parent: Pick<Location, 'ABSOLUTE'> = { ABSOLUTE: dirname(__filename) },
): Location {
  const ABSOLUTE = `${parent.ABSOLUTE}${directory}/`

  return {
    ABSOLUTE,
    get RELATIVE() { return `./${relative(process.cwd(), ABSOLUTE) || '.'}/` },
  }
}

export const projects = new Proxy(
  {
    root: {
      name: 'root',
      location: createLocation(),
    },
  },
  {
    get(target: Projects, directory: string): Projects[string] {
      const normalizedDirectory = normalizeDirectory(directory)

      if (!Reflect.has(target, normalizedDirectory)) {
        const project = {
          name: normalizedDirectory,
          location: createLocation(normalizedDirectory, target.root.location),
        }

        if (Object.entries(target).length < 50) {
          Reflect.set(target, normalizedDirectory, project)
        }

        return project
      }

      return Reflect.get(target, normalizedDirectory)
    },
  },
)
