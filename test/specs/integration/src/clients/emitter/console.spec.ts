import { Emitter } from '~/clients/emitter/console'

import { getFirstArguments } from '#/utils/mock-helpers'

function createConsoleMock() {
  return {
    log: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
  }
}

describe('Console emitter', () => {
  describe('Methods', () => {
    describe('emitMessage', () => {
      const consoleMock = createConsoleMock()
      const emitter = new Emitter(consoleMock as unknown as Console)

      beforeAll(() => emitter.emitMessage('Emitted message'))

      it('should emit message', () => {
        const [firstArgument] = getFirstArguments(consoleMock.log)

        expect(consoleMock.log).toHaveBeenCalledTimes(1)
        expect(firstArgument).toMatchSnapshot()
      })
    })

    describe('emitWarning', () => {
      const consoleMock = createConsoleMock()
      const emitter = new Emitter(consoleMock as unknown as Console)

      beforeAll(() => emitter.emitWarning('Emitted warning'))

      it('should emit warning', () => {
        const [firstArgument] = getFirstArguments(consoleMock.warn)

        expect(consoleMock.warn).toHaveBeenCalledTimes(1)
        expect(firstArgument).toMatchSnapshot()
      })
    })

    describe('emitError', () => {
      const consoleMock = createConsoleMock()
      const emitter = new Emitter(consoleMock as unknown as Console)

      beforeAll(() => emitter.emitError('Emitted Error'))

      it('should emit error', () => {
        const [firstArgument] = getFirstArguments(consoleMock.error)

        expect(consoleMock.error).toHaveBeenCalledTimes(1)
        expect(firstArgument).toMatchSnapshot()
      })
    })
  })
})
