import { up } from '~/handlers'
import { programHelpers } from '~/lib'
import { not, hasEntryIn } from '~/lib/predicate-factory'
import { toBulletedSentence } from '~/lib/print-formatter/list'
import { extractCompletionMetadata } from '~/lib/completion'

import type { Provisions, Options } from '@/command'
import type { Handler, CompletionHandler } from '@/handler'
import type { ServiceNames } from '@/config/project'
import type { InstallMap } from '~/lib/program-helpers/projects/services'

type Arguments = [ServiceNames]

export const handle: Handler<Arguments> = async function (
  [serviceNames],
  options,
) {
  const { provisions } = options
  const undefinedServices = await programHelpers.projects.services
    .extractUndefined(serviceNames, provisions)

  if (undefinedServices.length) {
    return programHelpers.projects.services
      .handleNotDefined(undefinedServices, provisions)
  }

  const installMap = await programHelpers.projects.services
    .getInstallMap(provisions, serviceNames)
  const servicesWithInstall = Object.keys(installMap)
  const servicesWithNoInstall
    = serviceNames.filter(not(hasEntryIn(servicesWithInstall)))

  if (servicesWithInstall.length) {
    await handleInstallServices(installMap, options)
  }

  if (servicesWithNoInstall.length) {
    warnSkippedServices(servicesWithNoInstall, provisions)
  }
}

async function handleInstallServices(
  installMap: InstallMap,
  options: Options,
): Promise<SideEffect> {
  const upServices = Object.keys(installMap)
  const { provisions } = options
  const { emitter } = provisions
  const { configPath, rawConfig } = await provisions.programMediator
    .getUsedProjectProvider()
    .then((provider) => provider.getAll())
  const orchestrationEngine = provisions.orchestrationEngineFactory
    .create(configPath, rawConfig.engine)

  await up.handle([upServices], options)

  for (const [service, installAggregate] of Object.entries(installMap)) {
    const { container, install, entryPoint } = installAggregate
    const runOptions = { shell: container.shell, entryPoint }

    emitter.emitMessage(`-- Starting installation for ${service} --`)
    await orchestrationEngine.run(container.name, install, runOptions)
    emitter.emitMessage(`-- Finished installation for ${service} --`)
  }
}

function warnSkippedServices(
  services: ServiceNames,
  provisions: Provisions,
): SideEffect {
  const notification = [
    'The following services are skipped as they have no install scripts:',
    toBulletedSentence(services),
  ].join('\n')

  provisions.emitter.emitWarning(notification)
}

export const complete: CompletionHandler<Arguments> = async function (
  commandArguments,
  { provisions },
) {
  const metadata = extractCompletionMetadata(provisions)

  if (metadata.isOptionCompletion()) {
    return programHelpers.completion.completeOptions(metadata, provisions)
  }

  const services = await programHelpers.projects.services.getKeys(provisions)

  return programHelpers.completion
    .variadicComplete(metadata, provisions, services)
}
