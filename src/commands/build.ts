import * as commander from 'commander'

import { build } from '~/handlers'
import { createActionHandler, decorateWithDefaults } from '~/commands'
import { requireUse, requireUsedConfigFile, autoReload } from '~/middlewares'
import { logDuration } from '~/lib/decorators'

import type { Create } from '@/command'

export const name = 'build'

const nameAndArgs = `${name} [reference...]`
const description = 'Build container/s or service container/s'
const argumentDescriptions = {
  reference:
    '(Optional) Container or service reference/s. Defaults to all containers',
}

export const create: Create = (provisions) => {
  const handler = createActionHandler(
    logDuration(build.handle, provisions.emitter, 'Build'),
    build.complete,
    provisions,
    [requireUse, requireUsedConfigFile, autoReload],
  )
  const command = new commander.Command()
    .command(nameAndArgs)
    .description(description, argumentDescriptions)
    .action(handler)
  const options = Object.values(build.options).map(
    ({ flag, description }) => new commander.Option(flag, description),
  )

  options.forEach((option) => command.addOption(option))

  return decorateWithDefaults(command, provisions)
}
