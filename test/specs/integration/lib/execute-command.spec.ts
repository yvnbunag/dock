import { EventEmitter } from 'events'

import { executeCommand } from '~/lib'

import { TemporaryWriteStreamMediator } from '#/utils/temporary-write-stream-mediator'

import type { ExecuteCommand } from '~/lib/execute-command'


async function trackAndCleanup(
  result: ReturnType<ExecuteCommand>,
  streamMediator: TemporaryWriteStreamMediator,
  mockProcess?: EventEmitter,
) {
  const [, child] = await result.catch((data) => data)
  await streamMediator.close()

  /**
   * Child process unusually needs to be killed manually after the commands has
   *  finished running to prevent it from blocking the next command to be
   *  executed / the Jest process from exiting
   */
  child.kill()
  if (mockProcess) mockProcess.emit('exit')
}

it('should stream command output', async () => {
  const streamMediator = await TemporaryWriteStreamMediator.create()
  const mockProcess = new EventEmitter()
  const { stream } = streamMediator
  const result = executeCommand(
    [
      'echo Initial command',
      'echo Middle command',
      'echo Last command',
    ],
    { stdio: ['ignore', stream, stream], process: mockProcess },
  )
  await trackAndCleanup(result, streamMediator, mockProcess)

  expect(streamMediator.data).toMatchSnapshot()
})

describe('It should abort command if parent process is terminated with', () => {
  const cases = [
    ['generic signal', 'SIGTERM'],
    ['keyboard interruption', 'SIGINT'],
    ['keyboard quit', 'SIGQUIT'],
  ]

  test.each(cases)('%s', async (description, signal) => {
    const streamMediator = await TemporaryWriteStreamMediator.create()
    const mockProcess = new EventEmitter()
    const { stream } = streamMediator
    const result = executeCommand(
      ['echo Initial command', 'sleep 5', 'echo Last command'],
      { stdio: ['ignore', stream, stream], process: mockProcess },
    )

    setTimeout(() => mockProcess.emit(signal), 500)
    await trackAndCleanup(result, streamMediator)

    expect(streamMediator.data).toMatchSnapshot()
  })
})
