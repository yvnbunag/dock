
import { table, getBorderCharacters } from 'table'
import { toString } from '~/lib/print-formatter/word'

export function toStringMatrix(
  matrix: Array<Array<Primitive>>,
): Array<Array<string>> {
  return matrix.map((list) => list.map(toString))
}

export function toTable(matrix: Array<Array<Primitive>>): string {
  const matrixTable = table(
    toStringMatrix(matrix),
    { border: getBorderCharacters('ramac') },
  )

  return matrixTable.replace(/\n$/, '')
}

export function toIndentedStringMatrix(
  matrix: Array<Array<Primitive>>,
  { initial = 0, left = 0, right = 1 } = {},
): string {
  return table(
    toStringMatrix(matrix),
    {
      border: getBorderCharacters('void'),
      columns: { 0: { paddingLeft: initial } },
      columnDefault: { paddingLeft: left, paddingRight: right },
      drawHorizontalLine: () => false,
    },
  )
}
