/**
 * Message emitter
 */
export interface Emitter {
  emitMessage(message: string): SideEffect<Emitter>,

  emitWarning(warning: string): SideEffect<Emitter>,

  emitError(error: string): SideEffect<Emitter>,

  exit(code: number): SideEffect<never>,
}
