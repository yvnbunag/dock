import { spawn } from 'child_process'

import AbortController from 'node-abort-controller'

import { CommandError, AbortError } from '~/errors'

import type { ChildProcess, StdioOptions } from 'child_process'

const exitSignals = [
  'SIGINT',
  'SIGTERM',
  'SIGQUIT',
  'exit',
  'uncaughtException',
  'unhandledRejection',
]

interface Dependencies {
  stdio?: StdioOptions,
  process?: NodeJS.EventEmitter,
}

type ExecuteCommandResult = [Nullable<CommandError>, ChildProcess]

export interface ExecuteCommand {
  (
    commands: Array<string>,
    dependencies?: Dependencies
  ): Promise<ExecuteCommandResult> | never,
  abortController?: AbortController,
}

export const executeCommand: ExecuteCommand = function (
  commands,
  {
    stdio = 'inherit',
    process = global.process,
  }: Dependencies = {},
) {
  return new Promise<ExecuteCommandResult>((resolve) => {
    const joinedCommands = commands.join(' && ')
    const abortController: AbortController = (() => {
      if (
        !executeCommand.abortController
        || executeCommand.abortController.signal.aborted
      ) {
        executeCommand.abortController = new AbortController()

        exitSignals.forEach(
          (signal) => process.once(signal, () => abortController.abort()),
        )
      }

      return executeCommand.abortController
    })()
    const child = spawn(joinedCommands, [], {
      shell: true,
      stdio,
      signal: abortController.signal,
    })
    function handleExit() { resolve([null, child])}
    function handleError(error: CommandError) { resolve([error, child]) }

    child.once('exit', handleExit)
    child.once('disconnect', handleExit)
    child.once('close', handleExit)
    child.once('error', handleError)
    abortController.signal.addEventListener('abort', () => {
      handleError(new AbortError(`Command "${joinedCommands}" aborted`))
    })
  })
}
