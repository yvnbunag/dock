import type { config } from '~/config'

export function create(): typeof config {
  const actual = jest.requireActual('~/config/config')

  Object.defineProperty(actual, 'version', {
    get() { return 'TEST' },
  })

  return actual
}
