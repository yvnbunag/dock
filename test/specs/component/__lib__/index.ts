export * as test from '#/specs/component/__lib__/test'
export * as caseFactory from '#/specs/component/__lib__/case-factory'
export { FileSwitcher } from '#/specs/component/__lib__/file-switcher'
export { createTestInstances } from '#/specs/component/__lib__/create-test-instances'
