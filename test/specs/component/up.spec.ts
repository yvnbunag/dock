import { createTestInstances, caseFactory } from '#/specs/component/__lib__'
import { projects } from '#/specs/component/__lib__/test'
import { Engine } from '@/config/project'

import type { Project } from '#/specs/component/__lib__/test/projects'
import type { Dock } from '#/utils/dock'
import type { Recorder } from '#/utils/recorder'

describe('Component - up [reference...]', () => {
  describe('Engine', () => {
    const projectsMap: Record<
      Engine,
      Parameters<typeof createEngineSuite>[1]
    > = {
      [Engine.DOCKER_COMPOSE]: {
        WITH_REFERENCES: projects['up/engine/docker-compose/with-references'],
        NO_REFERENCE: projects['up/engine/docker-compose/no-reference'],
      },
    }

    describe(
      ...createEngineSuite(
        Engine.DOCKER_COMPOSE,
        projectsMap[Engine.DOCKER_COMPOSE],
      ),
    )
  })

  describe('Completion', () => {
    describe('Project is used', () => {
      describe('Project has references', () => {
        let dock: Dock
        let recorder: Recorder

        beforeAll(async () => {
          const instances = await createTestInstances(
            'completion-with-references',
            projects['up/with-references'],
          )

          dock = instances.dock
          recorder = instances.recorder
        })
        beforeEach(() => { recorder.clearRecording() })

        it('should complete if ran with no arguments', async () => {
          await dock.complete('up ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete if ran with partial reference', async () => {
          await dock.complete('up container-with-')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should filtered complete if ran with complete reference', async () => {
          await dock.complete('up container-with-service-1 ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should filtered complete if ran with multiple references', async () => {
          await dock.complete('up container-with-service-1 service-1 ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete if ran with undefined reference', async () => {
          await dock.complete('up undefined-1 ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete with system completion if ran with exhausted references', async () => {
          const references = [
            'container-no-service-1',
            'container-with-service-1',
            'container-with-service-2',
            'container-same-with-service',
            'service-1',
            'service-2',
          ]

          await dock.complete(`up ${references.join(' ')} `)

          expect(recorder.recording).toMatchSnapshot()
        })

        describe(
          'Options are provided before command arguments',
          caseFactory.completion.options.createProvidedBeforeArguments(
            'up',
            () => ({ dock, recorder }),
            ['service-1', 'service-2'],
          ),
        )

        describe(
          'Default options',
          caseFactory.completion.options.createDefault(
            'up',
            () => ({ dock, recorder }),
            ['service-1', 'service-2'],
          ),
        )
      })

      describe('Project has no references', () => {
        let dock: Dock
        let recorder: Recorder

        beforeAll(async () => {
          const instances = await createTestInstances(
            'completion-no-reference',
            projects['up/no-reference'],
          )

          dock = instances.dock
          recorder = instances.recorder
        })
        beforeEach(() => { recorder.clearRecording() })

        it('should complete with system completion if ran with no arguments', async () => {
          await dock.complete('up ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete with system completion if ran with arguments', async () => {
          await dock.complete('up ./path ')

          expect(recorder.recording).toMatchSnapshot()
        })

        describe(
          'Options are provided before command arguments',
          caseFactory.completion.options.createProvidedBeforeArguments(
            'up',
            () => ({ dock, recorder }),
            ['./path', './another-path'],
          ),
        )

        describe(
          'Default options',
          caseFactory.completion.options.createDefault(
            'up',
            () => ({ dock, recorder }),
            ['./path', './another-path'],
          ),
        )
      })
    })

    describe('Project is not used', () => {
      const namespace = 'completion-no-project-used'
      const { dock, recorder } = createTestInstances(namespace)

      beforeEach(() => { recorder.clearRecording() })

      it('should complete with system completion if ran with no arguments', async () => {
        await dock.complete('up ')

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should complete with system completion if ran with arguments', async () => {
        await dock.complete('up ./path ')

        expect(recorder.recording).toMatchSnapshot()
      })

      describe(
        'Options are provided before command arguments',
        caseFactory.completion.options.createProvidedBeforeArguments(
          'up',
          () => ({ dock, recorder }),
          ['./path', './another-path'],
        ),
      )

      describe(
        'Default options',
        caseFactory.completion.options.createDefault(
          'up',
          () => ({ dock, recorder }),
          ['./path', './another-path'],
        ),
      )
    })
  })

  describe(
    'Auto reload',
    caseFactory.autoReload.createIntercepted(
      {
        GENERAL: { project: projects['up/with-references'] },
        VALID_CHANGE: { project: projects['up/updated-valid'] },
        INVALID_CHANGE: { project: projects['up/updated-invalid'] },
        REMOVED: { project: projects['up/removed'] },
      },
      {
        runValid: ({ dock }) => dock.run('up'),
        runInvalid: ({ dock }) => dock.run('up undefined'),
      },
    ),
  )

  describe(
    'Version',
    caseFactory.version.createDefault('up', createTestInstances),
  )

  describe(
    'Help',
    caseFactory.help.createDefault('up', createTestInstances),
  )
})

function createEngineSuite(
  engine: Engine,
  projectsMap: {
    WITH_REFERENCES: Project,
    NO_REFERENCE: Project,
  },
): [Engine, Method] {
  function createNameSpace(name: string) { return `${engine}-${name}`}

  function suite() {
    describe('Command', () => {
      describe('Project is used', () => {
        describe('Project has references', () => {
          let dock: Dock
          let recorder: Recorder

          beforeAll(async () => {
            const namespace = createNameSpace('with-references')
            const instances = await createTestInstances(
              namespace,
              projectsMap.WITH_REFERENCES,
            )

            dock = instances.dock
            recorder = instances.recorder
          })
          beforeEach(() => recorder.clearRecording())

          it('should bring up all containers if ran with no references', async () => {
            await dock.run('up')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should bring up container if ran with a container reference', async () => {
            await dock.run('up container-no-service-1')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should bring up containers if ran with multiple container references', async () => {
            await dock.run('up container-no-service-1 container-with-service-1')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should bring up service container if ran with a service reference', async () => {
            await dock.run('up service-1')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should bring up service containers if ran with multiple service references', async () => {
            await dock.run('up service-1 service-2')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should bring up containers if ran with all type of references', async () => {
            await dock.run('up container-with-service-1 service-2')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should bring up containers if ran with intersecting container and service container references', async () => {
            await dock.run('up container-same-with-service')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should bring up containers if ran with non unique references', async () => {
            const references = [
              'container-with-service-1',
              'container-with-service-1',
              'service-2',
              'service-2',
            ]

            await dock.run(`up ${references.join(' ')}`)

            expect(recorder.recording).toMatchSnapshot()
          })

          describe('It should emit error if container or service reference is not defined', () => {
            test('in only argument', async () => {
              await dock.run('up undefined-1')

              expect(recorder.recording).toMatchSnapshot()
            })

            test('in one of multiple arguments', async () => {
              await dock.run('up undefined-1 container-with-service-1')

              expect(recorder.recording).toMatchSnapshot()
            })

            test('in many of multiple arguments', async () => {
              const references = [
                'undefined-1',
                'undefined-2',
                'container-with-service-1',
                'service-1',
              ]
              await dock.run(`up ${references.join(' ')}`)

              expect(recorder.recording).toMatchSnapshot()
            })

            test('in all arguments', async () => {
              await dock.run('up undefined-1 undefined-2 undefined-3')

              expect(recorder.recording).toMatchSnapshot()
            })
          })
        })

        describe('Project has no reference', () => {
          let dock: Dock
          let recorder: Recorder

          beforeAll(async () => {
            const namespace = createNameSpace('no-reference')
            const instances = await createTestInstances(
              namespace,
              projectsMap.NO_REFERENCE,
            )

            dock = instances.dock
            recorder = instances.recorder
          })
          beforeEach(() => recorder.clearRecording())

          it('should bring up all containers if ran with no references', async () => {
            await dock.run('up')

            expect(recorder.recording).toMatchSnapshot()
          })

          describe('It should emit error if container or service reference is provided', () => {
            test('for one argument', async () => {
              await dock.run('up service-1')

              expect(recorder.recording).toMatchSnapshot()
            })

            test('for multiple arguments', async () => {
              await dock.run('up service-1 service-2 container-no-service-1')

              expect(recorder.recording).toMatchSnapshot()
            })
          })
        })
      })

      describe('Project is not used', () => {
        const namespace = createNameSpace('no-project-used')
        const { dock, recorder } = createTestInstances(namespace)
        const location = projectsMap.WITH_REFERENCES.location.ABSOLUTE

        beforeAll(async () => await dock.run(`register ${location}`))
        beforeEach(() => recorder.clearRecording())

        it('should emit error if ran with no arguments', async () => {
          await dock.run('up')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should emit error if ran with arguments', async () => {
          await dock.run('up service-1 service-2')

          expect(recorder.recording).toMatchSnapshot()
        })
      })
    })
  }

  return [engine, suite]
}
