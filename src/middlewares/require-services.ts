import { NOT_FOUND } from '~/config/exit-codes'

import type { Middleware } from '@/middleware'

type RequireServices = Middleware<'emitter' | 'programMediator'>

export const requireServices: RequireServices = async function (
  commandArguments,
  { provisions },
) {
  const { values } = await provisions.programMediator
    .getUsedProjectProvider()
    .then((provider) => provider.get('aggregates'))
    .then(({ services }) => services)

  if (values.length) return

  return provisions.emitter
    .emitError('There are no services defined in the project config')
    .exit(NOT_FOUND)
}
