import { camelCase } from 'lodash'

import { CHECK_MARK } from '~/config/symbols'
import { completion } from '~/lib/program-helpers'
import { toTable } from '~/lib/print-formatter/matrix'
import { completeFilename, extractCompletionMetadata } from '~/lib/completion'
import { extract } from '~/lib/transform-factory'

import type { Provisions, Option } from '@/command'
import type { Handler, CompletionHandler } from '@/handler'
import type { Program, Project } from '@/config'

type MatrixHeaders<Headers extends string = string> = Array<Headers>
type MatrixValues = Array<Array<string>>
type Matrix = [MatrixHeaders, ...MatrixValues]
enum KeyColumn {
  NAME = 'Name',
}
enum SettingColumn {
  ACTIVE = 'Active',
}
enum MetadataColumn {
  LOCATION = 'Location',
  UPDATED = 'Updated',
  VERSION = 'Version',
}
type ColumnOption = {
  display: Capitalize<string>,
  flag: `--${string}-${string}`,
  ref: string,
  description: string,
}

const explicitIncludeColumns = [
  ...Object.values(SettingColumn),
  MetadataColumn.LOCATION,
]
const explicitIncludeOptions: Array<ColumnOption>
  = explicitIncludeColumns.map((display) => {
    const flag = `--${display.toLowerCase()}-only` as const
    const ref = camelCase(flag.slice(2))
    const description
        = `Explicitly include ${display.toLowerCase()} column and exclude others`

    return { display, flag, ref, description }
  })
const includeColumns = [
  [MetadataColumn.UPDATED, 'Include config update date column'],
  [MetadataColumn.VERSION, 'Include config internal storage version column'],
]
const includeOptions: Array<ColumnOption>
  = includeColumns.map(([display, description]) => {
    const flag = `--include-${display.toLowerCase()}` as const
    const ref = camelCase(flag.slice(2))

    return { display, flag, ref, description }
  })

export const columnOptions: Array<ColumnOption> = [
  ...explicitIncludeOptions,
  ...includeOptions,
]

export const handle: Handler = async function (
  commandArguments,
  { provisions, option },
) {
  const config = await provisions.programMediator.getConfig()

  if (!Object.entries(config.projects.metadata).length) {
    return handleNoneRegistered(provisions)
  }

  return handleProjects(option, config, provisions)
}

function handleNoneRegistered(provisions: Provisions): SideEffect {
  provisions.emitter.emitMessage('No projects registered')
}

function handleProjects(
  option: Option,
  config: Program.Config,
  provisions: Provisions,
): SideEffect {
  const projects = Object.keys(config.projects.metadata)
  const [
    keyHeaders,
    keyValues,
  ] = extractKeysMatrix(projects)
  const [
    settingHeaders,
    settingValues,
  ] = extractSettingsMatrix(projects, config)
  const [
    metadataHeaders,
    metadataValues,
  ] = extractMetadataMatrix(projects, config)
  const headers = [
    ...keyHeaders,
    ...settingHeaders,
    ...metadataHeaders,
  ]
  const data = projects.map((name, index) => {
    return [
      ...keyValues.map((values) => values[index]),
      ...settingValues.map((values) => values[index]),
      ...metadataValues.map((values) => values[index]),
    ]
  })
  const rawMatrix: Matrix = [headers, ...data]
  const matrix = extractIncludedColumns(keyHeaders, rawMatrix, option)
  const message = toTable(matrix)

  provisions.emitter.emitMessage(message)
}

function extractKeysMatrix(projects: Array<Project.Name>) {
  const headers: MatrixHeaders<KeyColumn> = []
  const values: MatrixValues = []

  //#region Name
  {
    headers.push(KeyColumn.NAME)
    values.push(projects)
  }
  //#endregion

  return [headers, values] as const
}

function extractSettingsMatrix(
  projects: Array<Project.Name>,
  config: Program.Config,
) {
  const headers: MatrixHeaders<SettingColumn> = []
  const values: MatrixValues = []

  //#region Active
  {
    const activeValues = projects.map(
      (name) => config.settings.active === name ? CHECK_MARK : '',
    )

    headers.push(SettingColumn.ACTIVE)
    values.push(activeValues)
  }
  //#endregion

  return [headers, values] as const
}

function extractMetadataMatrix(
  projects: Array<Project.Name>,
  config: Program.Config,
) {
  const headers: MatrixHeaders<MetadataColumn> = []
  const values: MatrixValues = []
  const { metadata } = config.projects

  //#region Location
  {
    const locationValues = projects.map((name) => metadata[name].configPath)

    headers.push(MetadataColumn.LOCATION)
    values.push(locationValues)
  }
  //#endregion

  //#region Updated
  {
    const updatedValues = projects.map((name) => metadata[name].updated)

    headers.push(MetadataColumn.UPDATED)
    values.push(updatedValues)
  }
  //#endregion

  //#region Version
  {
    const versionValues = projects.map((name) => metadata[name].version)

    headers.push(MetadataColumn.VERSION)
    values.push(versionValues)
  }
  //#endregion

  return [headers, values] as const
}

function extractIncludedColumns(
  keyHeaders: Array<KeyColumn>,
  matrix: Matrix,
  option: Option,
): Matrix {
  const includedExplicitIncludeOptions = (() => {
    const provided = explicitIncludeOptions.filter(({ ref }) => ref in option)

    if (provided.length) return provided

    return explicitIncludeOptions
  })()
  const includedExplicitIncludeHeaders = includedExplicitIncludeOptions
    .map(extract('display'))
  const includedIncludeOptions = includeOptions
    .filter(({ ref }) => ref in option)
  const includedIncludeHeaders = includedIncludeOptions
    .map(extract('display'))
  const includedHeaders = [
    ...includedExplicitIncludeHeaders,
    ...includedIncludeHeaders,
  ]
  const includedIndexes = matrix[0].reduce(
    (indexes, header, index) => {
      if (includedHeaders.includes(header)) indexes.push(index)

      return indexes
    },
    Object.keys(keyHeaders).map(Number),
  )

  return matrix.map(
    (row) => row.filter((column, index) => includedIndexes.includes(index)),
  ) as Matrix
}

export const complete: CompletionHandler = async function (
  commandArguments,
  { provisions },
) {
  const metadata = extractCompletionMetadata(provisions)

  if (metadata.isOptionCompletion()) {
    return completion.completeOptions(
      metadata,
      provisions,
      columnOptions.map(extract('flag')),
    )
  }

  return completeFilename(provisions.emitter)
}
