import * as commander from 'commander'

import { run } from '~/handlers'
import { createActionHandler, decorateWithDefaults } from '~/commands'
import {
  requireArguments,
  requireUse,
  requireUsedConfigFile,
  autoReload,
} from '~/middlewares'

import type { Create } from '@/command'

export const name = 'run'

const nameAndArgs = `${name} [reference] [command...]`
const description = 'Run command in container or service container'
const argumentDescriptions = {
  reference: 'Container or service reference',
  command: 'Commands to run',
}

export const create: Create = (provisions) => {
  const handler = createActionHandler(
    run.handle,
    run.complete,
    provisions,
    [
      requireArguments(nameAndArgs),
      requireUse,
      requireUsedConfigFile,
      autoReload,
    ],
  )
  const command = new commander.Command()
    .command(nameAndArgs)
    .description(description, argumentDescriptions)
    .action(handler)
    .allowUnknownOption()

  return decorateWithDefaults(command, provisions)
}
