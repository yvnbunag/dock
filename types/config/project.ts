export type Name = string

export type ConfigVersion = string

export type ConfigPath = string

export type Updated = string

export enum Engine {
  DOCKER_COMPOSE = 'docker-compose',
}

export type Description = string

export enum Shell {
  SH = 'sh',
  BASH = 'bash',
}

export type ContainerName = string

export type ContainerNames = Array<ContainerName>

export type Container = {
  shell?: Shell,
  description?: Description,
}

export type ServiceName = string

export type ServiceNames = Array<ServiceName>

export type EntryPoint = string

export type ShellCommand = string

export type Reference = ContainerName | ServiceName

export type References = Array<Reference>

export type Service = {
  container: ContainerName,
  'entry-point'?: EntryPoint,
  install?: Array<ShellCommand>,
  description?: Description,
}

export type ModeName = string

export type ModeNames = Array<ModeName>

export type Mode = {
  containers?: ContainerNames,
  services?: ServiceNames,
  description?: Description,
}

export type RawConfig = {
  name: Name,
  engine?: Engine,
  description?: Description,
  containers?: Record<ContainerName, Container>,
  services?: Record<ServiceName, Service>,
  modes?: Record<ModeName, Mode>,
}

export interface ContainerAggregates {
  values: ContainerNames,
}

export interface ServiceAggregates {
  values: ServiceNames,
}

export interface ModeAggregates {
  values: Array<ModeName>,
}

export interface ReferenceAggregates {
  values: References,
}

export interface Aggregates {
  containers: ContainerAggregates,
  services: ServiceAggregates,
  modes: ModeAggregates,
  references: ReferenceAggregates,
}

export type Config = {
  version: ConfigVersion,
  configPath: ConfigPath,
  updated: Updated,
  rawConfig: RawConfig,
  aggregates: Aggregates,
}
