import type * as commander from 'commander'

import type { OptionMediator } from '~/lib/option-mediator'
import type { EnvironmentReader } from '~/lib/environment-reader'
import type { ProgramMediator } from '~/lib/program-mediator'
import type { Emitter } from '@/clients'
import type { NodeArguments } from '@/main'
import type { OrchestrationEngineFactory } from '@/clients/orchestration-engine'

export type Name = string

export type CommandArguments = [...Array<string | Array<string>>]

export interface Provisions {
  readonly emitter: Emitter,
  readonly environmentReader: EnvironmentReader,
  readonly optionMediator: OptionMediator,
  readonly programMediator: ProgramMediator,
  readonly orchestrationEngineFactory: OrchestrationEngineFactory,
  readonly nodeArguments: NodeArguments,
}

export type Option = commander.OptionValues

export type Command = commander.Command

export interface Options<
  ProvisionKeys extends keyof Provisions = keyof Provisions,
> {
  provisions: Pick<Provisions, ProvisionKeys>,
  option: Option,
  command: Command,
}

export type ActionArguments<
  ProvidedArguments extends CommandArguments = CommandArguments,
> = [
  ...ProvidedArguments,
  Option,
  Command,
]

export interface Create {
  (provisions: Provisions): Command,
}

/**
 * Command module definition
 */
export interface Factory {
  readonly name: Name,
  readonly create: Create,
}
