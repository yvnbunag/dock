import { programHelpers } from '~/lib'
import { extractCompletionMetadata } from '~/lib/completion'

import type { Provisions } from '@/command'
import type { Handler, CompletionHandler } from '@/handler'
import type { References } from '@/config/project'

type Arguments = [References]

export const handle: Handler<Arguments> = async function (
  [references],
  { provisions },
) {
  const undefinedReferences = await programHelpers.projects.references
    .extractUndefined(references, provisions)

  if (undefinedReferences.length) {
    return programHelpers.projects.references
      .handleNotDefined(undefinedReferences, provisions)
  }

  return handleDown(references, provisions)
}

async function handleDown(
  references: References,
  provisions: Provisions,
): AsyncSideEffect | never {
  const [
    { configPath, rawConfig },
    containers,
  ] = await Promise.all([
    provisions.programMediator.getUsedProjectProvider()
      .then((provider) => provider.getAll()),
    programHelpers.projects.references
      .extractContainers(references, provisions),
  ])

  await provisions.orchestrationEngineFactory
    .create(configPath, rawConfig.engine)
    .down(containers)
}

export const complete: CompletionHandler<Arguments> = async function (
  commandArguments,
  { provisions },
) {
  const metadata = extractCompletionMetadata(provisions)

  if (metadata.isOptionCompletion()) {
    return programHelpers.completion.completeOptions(metadata, provisions)
  }

  const references = await programHelpers.projects.references
    .getKeys(provisions)

  return programHelpers.completion
    .variadicComplete(metadata, provisions, references)
}
