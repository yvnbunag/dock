import type { Provisions, Option, Command, Options } from '@/command'

export function createOptions<
  ProvisionKeys extends keyof Provisions = keyof Provisions,
>({
  provisions = Symbol('Provisions') as unknown as Provisions,
  option = Symbol('Option') as unknown as Option,
  command = Symbol('Command') as unknown as Command,
}: Partial<Options<ProvisionKeys>>): Options<ProvisionKeys> {
  return {
    provisions,
    option,
    command,
  }
}
