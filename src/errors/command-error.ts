import { DockError } from '~/errors/dock-error'
import { exitCodes } from '~/config'

export class CommandError extends DockError {
  name = 'CommandError'
  constructor(message?: string) {
    super(exitCodes.INVALID, message)
  }
}
