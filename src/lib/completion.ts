import { globalFlags } from '~/config'
import { SYSTEM_COMPLETE } from '~/config/exit-codes'
import { QUOTES } from '~/config/symbols'
import { DockExit } from '~/errors'
import { environmentReader } from '~/lib/environment-reader'
import { trim, pushStringWithGrouping } from '~/lib/transform-factory'
import { not } from '~/lib/predicate-factory'

import type { Provisions } from '@/command'
import type { Emitter } from '@/clients'
import type { RawNodeArguments } from '@/main'

enum Type {
  NEW='n',
  CONTINUATION='c',
  NEGATIVE='x',
}

type CompletionProvisions = Pick<Provisions, 'optionMediator' | 'nodeArguments'>

type CompletionArguments = RawNodeArguments | true | undefined

export enum SystemCompletion {
  DEFAULT = 1,
}

export class CompletionMetadata {
  private _normalizedArguments: Nullable<string> = null
  private _level = 0
  private _lastCharacter: Nullable<string> = null
  private _type: Nullable<Type> = null
  private _position: Nullable<number> = null
  private _arguments: Nullable<Array<string>> = null
  private _commandArguments: Nullable<Array<string>> = null
  private _optionArguments: Nullable<Array<string>> = null
  private _lastArgument: Nullable<string> = null
  private _lastCommandArgument: Nullable<string> = null
  private _lastOptionArgument: Nullable<string> = null

  constructor(rawArguments?: RawNodeArguments, level = 0) {
    if (typeof rawArguments !== 'string') return

    this.normalizedArguments = rawArguments
      .replace(/\s+/g, ' ') // Combine spaces
    this.level = level
  }

  get normalizedArguments(): Nullable<string> {
    return this._normalizedArguments
  }

  private set normalizedArguments(value) {
    this._normalizedArguments = value
  }

  get level(): number {
    return this._level
  }

  private set level(value) {
    this._level = value
  }

  get lastCharacter(): string {
    if (!this._lastCharacter) {
      const normalizedArguments = this.normalizedArguments || ''

      this._lastCharacter = normalizedArguments[normalizedArguments.length - 1]
    }

    return this._lastCharacter
  }

  get type(): Type {
    if (!this._type) {
      this._type = (() => {
        if (this.normalizedArguments === null) return Type.NEGATIVE

        return !this.normalizedArguments || this.lastCharacter === ' '
          ? Type.NEW
          : Type.CONTINUATION
      })()
    }

    return this._type
  }

  get position(): number {
    if (!this._position) this._position = this.commandArguments.length

    return this._position
  }

  get arguments(): Array<string> {
    if (!this._arguments) {
      this._arguments = (this.normalizedArguments || '')
        .split(' ')
        .reduce(pushStringWithGrouping(QUOTES), [])
        .map(trim(QUOTES))
        .slice(this.level)
    }

    return this._arguments
  }

  get commandArguments(): Array<string> {
    if (!this._commandArguments) {
      this._commandArguments = this.arguments.filter(
        not(CompletionMetadata.isOption),
      )
    }

    return this._commandArguments
  }

  get optionArguments(): Array<string> {
    if (!this._optionArguments) {
      this._optionArguments = this.arguments.filter(CompletionMetadata.isOption)
    }

    return this._optionArguments
  }

  get lastArgument(): string {
    if (!this._lastArgument) {
      this._lastArgument = this.arguments[this.arguments.length - 1]
    }

    return this._lastArgument
  }

  get lastCommandArgument(): string {
    if (!this._lastCommandArgument) {
      const value = this.commandArguments[this.commandArguments.length - 1]

      this._lastCommandArgument = value
    }

    return this._lastCommandArgument
  }

  get lastOptionArgument(): string {
    if (!this._lastOptionArgument) {
      const value = this.optionArguments[this.optionArguments.length - 1]

      this._lastOptionArgument = value
    }

    return this._lastOptionArgument
  }

  isCompletion(): boolean {
    return this.type !== Type.NEGATIVE
  }

  isNew(): boolean {
    return this.type === Type.NEW
  }

  isContinuation(): boolean {
    return this.type === Type.CONTINUATION
  }

  isCommandCompletion(): boolean {
    return !CompletionMetadata.isOption(this.lastArgument)
  }

  isOptionCompletion(): boolean {
    return CompletionMetadata.isOption(this.lastArgument)
  }

  static isOption(argument: string): boolean {
    return Boolean(argument) && argument[0] === '-'
  }

  static extract: Memoized<
    (rawArguments: RawNodeArguments, level?: number)=> CompletionMetadata
  > = function (rawArguments, level = 1) {
    const key = `${rawArguments}:${level}`
    const memoizedMetadata = CompletionMetadata.extract?.memoized?.[key]

    if (memoizedMetadata) return memoizedMetadata

    const completionMetadata = new CompletionMetadata(rawArguments, level)

    if (environmentReader.memoize) {
      if (!CompletionMetadata.extract.memoized) {
        CompletionMetadata.extract.memoized = {}
      }

      CompletionMetadata.extract.memoized[key] = completionMetadata
    }

    return completionMetadata
  }
}

/**
 * Extract completion metadata from provisions and command level
 */
export function extractCompletionMetadata(
  provisions: CompletionProvisions,
  level = 1,
): CompletionMetadata {
  const completionArguments = provisions.optionMediator.get(
    globalFlags.COMPLETION.name,
  ) as CompletionArguments
  const { nodeArguments } = provisions

  /**
   * Commander sets option value to `true` (option was passed) if next argument
   *  starts with "--" (indicates end of options).
   *
   * To work around the given case, extract argument next to completion flag to
   *  replace commander provided value.
   *
   * See commander documentation:
   *  https://www.npmjs.com/package/commander#:~:text=You%20can%20use%20--%20to%20indicate%20the%20end%20of%20the%20options
   */
  const coercedArguments = completionArguments === true
    ? nodeArguments[nodeArguments.indexOf('--completion') + 1]
    : completionArguments as RawNodeArguments

  return CompletionMetadata.extract(coercedArguments, level)
}

/**
 * Determine if command is completion
 */
export function isCompletion(provisions: CompletionProvisions): boolean {
  return extractCompletionMetadata(provisions).isCompletion()
}

/**
 * Determine if command is completion from node arguments
 */
export function isStrictCompletion(provisions: CompletionProvisions): boolean {
  return provisions.nodeArguments.includes(globalFlags.COMPLETION.flag, 2)
}

/**
 * Decorate async completion method to complete with system completion on error
 */
export function completeWithErrorFallback<MethodType extends Method>(
  complete: MethodType,
  emitter: Emitter,
  fallback: string | SystemCompletion = SystemCompletion.DEFAULT,
): Promise<ReturnType<MethodType> | SideEffect> {
  return Promise
    .resolve(complete())
    .catch((error) => {
      if (error instanceof DockExit) throw error

      return emitCompletion(fallback, emitter)
    })
}

/**
 * Emit filename completion
 */
export function completeFilename(emitter: Emitter): SideEffect {
  emitCompletion(SystemCompletion.DEFAULT, emitter)
}

/**
 * Emit completion based on type
 *
 * @param completion - Completion to be emitted. Values may be comma delimited
 *  string or a system completion strategy
 */
export function emitCompletion(
  completion: string | SystemCompletion,
  emitter: Emitter,
): SideEffect | never {
  if (completion === SystemCompletion.DEFAULT || !completion) {
    return emitter.exit(SYSTEM_COMPLETE)
  }

  emitter.emitMessage(completion)
}
