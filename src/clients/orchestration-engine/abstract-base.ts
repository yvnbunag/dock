import escapeQuotes from 'escape-quotes'

import type { ExecuteCommand } from '~/lib/execute-command'
import type {
  ConfigPath,
  ContainerNames,
  ContainerName,
} from '@/config/project'
import type {
  OrchestrationEngineInterface,
  ContainerCommandOptions,
  ContainerCommands,
} from '@/clients/orchestration-engine'

export abstract class OrchestrationEngine
implements OrchestrationEngineInterface
{
  constructor(
    protected readonly configPath: ConfigPath,
    protected readonly executeCommand: ExecuteCommand,
  ) {}

  abstract build(
    containers?: ContainerNames
  ): Promise<OrchestrationEngineInterface> | never
  abstract start(
    containers?: ContainerNames
  ): Promise<OrchestrationEngineInterface> | never
  abstract stop(
    containers?: ContainerNames
  ): Promise<OrchestrationEngineInterface> | never
  abstract remove(
    containers?: ContainerNames
  ): Promise<OrchestrationEngineInterface> | never
  abstract down(
    containers?: ContainerNames
  ): Promise<OrchestrationEngineInterface> | never
  abstract run(
    container: ContainerName,
    commands: ContainerCommands,
    options?: ContainerCommandOptions,
  ): Promise<OrchestrationEngineInterface> | never
  abstract open(
    container: ContainerName,
    options?: Pick<ContainerCommandOptions, 'shell' | 'entryPoint'>,
  ): Promise<OrchestrationEngineInterface> | never

  static chainCommands(commands: Array<string>): string {
    return commands.filter(Boolean).join(' && ')
  }

  static joinArguments(args: Array<string>): string {
    return args.filter(Boolean).join(' ')
  }

  static escapeQuotes = escapeQuotes
}
