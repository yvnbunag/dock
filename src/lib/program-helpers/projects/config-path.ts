import { isAbsolute } from 'path'

import { exitCodes } from '~/config'

import type { ConfigPath } from '@/config/project'
import type { Provisions } from '@/command'

export function handleAlreadyRegistered(
  configPath: ConfigPath,
  provisions: Provisions,
): SideEffect<never> {
  return provisions.emitter
    .emitError(`Project in path '${configPath}' already registered`)
    .emitError(`Run 'dock projects' to list registered projects`)
    .exit(exitCodes.CONFLICT)
}

export function normalize(
  configPath: ConfigPath = '',
  runPath = process.cwd(),
): ConfigPath {
  return isAbsolute(configPath)
    ? normalizeAbsolute(configPath)
    : determineAbsolute(configPath, runPath)
}

export function normalizeAbsolute(configPath: ConfigPath): ConfigPath {
  return configPath.slice(-1) === '/'
    ? configPath.slice(0, -1)
    : configPath
}

export function determineAbsolute(
  configPath: ConfigPath,
  runPath = process.cwd(),
): ConfigPath {
  return normalizeAbsolute(
    `${runPath}/${normalizeRelative(configPath)}`,
  )
}

export function normalizeRelative(configPath: ConfigPath): ConfigPath {
  const shiftAbles = ['.', '/']

  return shiftAbles.reduce(
    (normalizedPath, shiftable) => {
      return normalizedPath[0] === shiftable
        ? normalizedPath.slice(1)
        : normalizedPath
    },
    configPath,
  )
}
