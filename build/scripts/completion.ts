import CopyPlugin from 'copy-webpack-plugin'

const includeCompletionScriptsPlugin = new CopyPlugin({
  patterns: [
    {
      from: 'src/scripts/completion',
      to: 'scripts/completion',
    },
  ],
})

export const plugins = [
  includeCompletionScriptsPlugin,
]
