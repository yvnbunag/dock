import { exec } from 'child_process'

import { Dock } from '#/utils/dock'

enum SpecificArgs {
  EMPTY = 1,
  SINGLE_SPACE,
  MULTIPLE_SPACES,
}

const specificArgValues: Record<SpecificArgs, string> = {
  [SpecificArgs.EMPTY]: '',
  [SpecificArgs.SINGLE_SPACE]: ' ',
  [SpecificArgs.MULTIPLE_SPACES]: '     ',
}

class PublicizedDock extends Dock {
  publicizedCreateTestProcessArgv(
    args: string | SpecificArgs,
  ): ReturnType<Dock['createTestProcessArgv']> {
    const computedArgs = args in SpecificArgs
      ? specificArgValues[args as SpecificArgs]
      : args as string

    return this.createTestProcessArgv(computedArgs)
  }
}

function getActualProcessArgv(
  args: string | SpecificArgs,
): Promise<Array<string>> {
  const script = `${__dirname}/print-process-argv.js`
  const computedArgs = args in SpecificArgs
    ? specificArgValues[args as SpecificArgs]
    : ` ${args}`

  return new Promise((resolve, reject) => {
    exec(`node ${script}${computedArgs}`, (error, stdout, stderr) => {
      if (error) return reject(error)
      if (stderr) return reject(stderr)

      resolve(JSON.parse(stdout))
    })
  })
}

interface ArgvCommandArguments {
  actual: Array<string>,
  test: Array<string>,
}

async function getCommandArguments(
  args: string | SpecificArgs,
  dock: PublicizedDock,
): Promise<ArgvCommandArguments> {
  const actual = (await getActualProcessArgv(args)).slice(2)
  const test = [...dock.publicizedCreateTestProcessArgv(args)]

  return { actual, test }
}

describe('Methods', () => {
  describe('Private', () => {
    describe('createTestProcessArgv', () => {
      const dock = new PublicizedDock()

      describe('It should mock process.argv for case', () => {
        type Cases = Array<[string, string | SpecificArgs]>

        const noArgumentCases: Cases = [
          [
            'No argument given',
            SpecificArgs.EMPTY,
          ],
          [
            'Single space',
            SpecificArgs.SINGLE_SPACE,
          ],
          [
            'Multiple spaces',
            SpecificArgs.MULTIPLE_SPACES,
          ],
        ]
        const commandCases: Cases = [
          [
            'Command',
            'register',
          ],
          [
            'Space after command',
            'register ',
          ],
          [
            'Command with argument',
            'register ./path/to/project',
          ],
          [
            'Command with single quoted argument',
            `use 'service 1'`,
          ],
          [
            'Command with double quoted argument',
            'use "service 1"',
          ],
          [
            'Command with multiple arguments',
            'use service-1 service-2 service-3',
          ],
          [
            'Command with multiple varying quoted arguments',
            `use 'service 1' "service 2"`,
          ],
          [
            'Multiple varying arguments',
            `use service-1 'service 2' "service 3"`,
          ],
        ]
        const optionCases: Cases = [
          [
            'Single partial option',
            '-',
          ],
          [
            'Single option',
            '--help',
          ],
          [
            'Space after option',
            '-h ',
          ],
          [
            'Option with option argument',
            '--level info',
          ],
          [
            'Option with single quoted option argument',
            `--completion 'completion ena'`,
          ],
          [
            'Option with double quoted option argument',
            '--run "yarn dev"',
          ],
          [
            'Multiple options',
            '--help -v',
          ],
          [
            'Multiple options with option argument',
            '--level info --logger console',
          ],
          [
            'Multiple options with varying quoted options argument',
            `--run "yarn dev" --level 'info'`,
          ],
          [
            'Multiple varying options',
            `-v --version --level 'info' --run "yarn dev"`,
          ],
        ]
        const commandAndOptionCases: Cases = [
          [
            'Command and option',
            'use -v',
          ],
          [
            'Command with argument and option',
            'up nginx --help',
          ],
          [
            'Command with multiple arguments and options',
            'mode front-end back-end -v --help',
          ],
          [
            'Command with multiple arguments and options with option argument',
            'mode front-end back-end --level info --logger console',
          ],
          [
            'Command with multiple varying arguments and options with option argument',
            `mode front-end 'back end' "database 1" --level info --logger console`,
          ],
          [
            'Command with multiple arguments and multiple varying options',
            `mode front-end back-end -v --level 'info' --run "yarn dev"`,
          ],
          [
            'Command with multiple varying arguments and multiple varying options',
            `mode front-end 'back end' "database 1" -v --level 'info' --run "yarn dev"`,
          ],
          [
            'Multiple intersecting and varying command and options',
            `mode front-end -v 'back end' --level 'info' "database 1" --run "yarn dev"`,
          ],
        ]
        const quoteCases: Cases = [
          [
            'Argument has single quote within double quotes',
            `run "docker exec -it 'yarn dev'"`,
          ],
          [
            'Argument has double quote within single quotes',
            `run 'docker exec -it "yarn dev"'`,
          ],
          [
            'Argument has mixed single and double quote intersection',
            `run "docker exec -it 'yarn dev'" --on-fail 'log "Command failed"'`,
          ],
        ]
        const spacingCases: Cases = [
          [
            'Argument has 2 spaces to next argument',
            'completion  enable',
          ],
          [
            'Arguments has more than 2 spaces to next argument',
            'completion     enable',
          ],
          [
            'Arguments has varying spaces between each other',
            'completion     enable --level  info',
          ],
          [
            'Arguments within quotes has spaces at the start and end',
            '--completion " --help completion "  --help completion ',
          ],
        ]
        const cases: Cases = [
          ...noArgumentCases,
          ...commandCases,
          ...optionCases,
          ...commandAndOptionCases,
          ...quoteCases,
          ...spacingCases,
        ]

        test.each(cases)('%s', async (description, args) => {
          const { actual, test } = await getCommandArguments(args, dock)

          expect(test).toStrictEqual(actual)
          expect(test).toMatchSnapshot()
        })
      })
    })
  })
})
