import { DockError } from '~/errors'
import { INCORRECT_USAGE } from '~/config/exit-codes'

export class NoUsedProjectError extends DockError {
  name = 'NoUsedProjectError'
  constructor(message = 'No project is currently in use') {
    super(INCORRECT_USAGE, message)
  }
}
