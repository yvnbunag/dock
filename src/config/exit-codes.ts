/**
 * @file When extending, ensure that documentation in
 * `documentation/exit-codes.md` is kept up to date
 */

export const SUCCESS = 0
export const SYSTEM_COMPLETE = 0
export const FAILED = 100
export const PROCESS_FAILED = 101
export const UNKNOWN = 102
export const UNSUPPORTED = 103
export const INVALID = 104
export const CONFLICT = 105
export const NOT_FOUND = 106
export const INCORRECT_USAGE = 107
export const INCOMPLETE = 108
