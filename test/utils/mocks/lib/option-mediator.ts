import { OptionMediator } from '~/lib'

class MockedOptionMediator
  extends OptionMediator
  implements jest.Mocked<OptionMediator>
{
  add = jest.fn()
  has = jest.fn()
  get = jest.fn()
}

export function create(): jest.Mocked<OptionMediator> {
  return new MockedOptionMediator()
}
