import { toString, singleQuote } from '~/lib/print-formatter/word'

import { primitive } from '#/utils/values'
const variantEntries = Object
  .entries(primitive)
  .map(([key, value]) => [key.toLowerCase(), value])

describe('toString', () => {
  describe('It should format to string arguments of type', () => {
    test.each(variantEntries)('%s', (type, value) => {
      expect(toString(value)).toMatchSnapshot()
    })
  })
})

describe('singleQuote', () => {
  describe('It should quote arguments of type', () => {
    test.each(variantEntries)('%s', (type, value) => {
      expect(singleQuote(value)).toMatchSnapshot()
    })
  })
})