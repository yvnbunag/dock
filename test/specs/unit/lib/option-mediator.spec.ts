import { OptionMediator } from '~/lib'

import type * as commander from 'commander'

function createCommandWithOptions(options = {}): commander.Command {
  return { opts: () => options } as commander.Command
}

describe('Instance methods', () => {
  describe('add', () => {
    const optionMediator = new OptionMediator()
    const key = 'add-key'

    beforeAll(() => {
      optionMediator.add(key, createCommandWithOptions())
    })

    it('should be added', () => {
      expect(optionMediator.has(key)).toBe(true)
    })
  })

  describe('has', () => {
    const key = 'has-key'

    it('should return false if source is not added', () => {
      const optionMediator = new OptionMediator()

      expect(optionMediator.has(key)).toBe(false)
    })

    it('should return true if source is added', () => {
      const optionMediator = new OptionMediator()

      optionMediator.add(key, createCommandWithOptions())

      expect(optionMediator.has(key)).toBe(true)
    })
  })

  describe('get', () => {
    const key = 'get-key'

    describe('Source is added', () => {
      const optionMediator = new OptionMediator()
      const value = 'get-value'

      beforeAll(() => {
        optionMediator.add(key, createCommandWithOptions({ [key]: value }))
      })

      it('should return value from source', () => {
        expect(optionMediator.get(key)).toBe(value)
      })
    })

    describe('Source is not added', () => {
      const optionMediator = new OptionMediator()

      it('should return undefined', () => {
        expect(optionMediator.get(key)).toBeUndefined()
      })
    })
  })
})