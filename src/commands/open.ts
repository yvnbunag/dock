import * as commander from 'commander'

import { open } from '~/handlers'
import { createActionHandler, decorateWithDefaults } from '~/commands'
import {
  requireArguments,
  requireUse,
  requireUsedConfigFile,
  autoReload,
} from '~/middlewares'

import type { Create } from '@/command'

export const name = 'open'

const nameAndArgs = `${name} [reference]`
const description = 'Open interactive shell of container or service container'
const argumentDescriptions = { reference: `Container or service reference` }

export const create: Create = (provisions) => {
  const handler = createActionHandler(
    open.handle,
    open.complete,
    provisions,
    [
      requireArguments(nameAndArgs),
      requireUse,
      requireUsedConfigFile,
      autoReload,
    ],
  )
  const command = new commander.Command()
    .command(nameAndArgs)
    .description(description, argumentDescriptions)
    .action(handler)

  return decorateWithDefaults(command, provisions)
}
