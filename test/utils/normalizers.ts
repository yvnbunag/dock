import * as root from 'app-root-path'

import { replaceString, replaceStringInstance } from '#/utils/general-helpers'

export function createRootPathNormalizer(): ReturnType<typeof replaceString> {
  return replaceString(root.path, '<dock-directory>')
}

export function createUpdatedColumnNormalizer():
  ReturnType<typeof replaceStringInstance>
{
  return replaceStringInstance(
    /\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}/g,
    (instance) => {
      const width = 19
      const replacement = `<updated-${instance}>`
      const offset = ' '.repeat((width - replacement.length))

      return `${replacement}${offset}`
    },
  )
}
