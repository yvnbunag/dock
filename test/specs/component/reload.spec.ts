import { fallback, config } from '~/config'
import {
  createTestInstances,
  caseFactory,
  FileSwitcher,
} from '#/specs/component/__lib__'
import { projects } from '#/specs/component/__lib__/test'
import { createUpdatedColumnNormalizer } from '#/utils/normalizers'
import { delay } from '#/utils/general-helpers'
import { mockHrtime } from '#/utils/mocks/process'

import type { Dock } from '#/utils/dock'
import type { Recorder } from '#/utils/recorder'
import type { Instances } from '#/specs/component/__lib__/create-test-instances'

mockHrtime()

function addReloadTransformers(instances: Instances): Instances {
  instances.recorder.addTransformer(createUpdatedColumnNormalizer())

  return instances
}

describe('Component - reload', () => {
  describe('Command', () => {
    const initialSnapshots = { PRE_RELOAD: '', RELOAD: '', POST_RELOAD: '' }
    const reloadConfirmationCommand
      = 'projects --active-only --include-updated --include-version'

    describe('Project is used', () => {
      const cases = {
        INITIAL: 'dock.config.initial.yml',
        UPDATED: 'dock.config.updated.yml',
      }
      const { name: configName } = fallback.project.CONFIG

      describe('Config has no changes', () => {
        describe('Internal storage is not changed', () => {
          let dock: Dock
          let recorder: Recorder
          const snapshots = { ...initialSnapshots }

          beforeAll(async () => {
            const instances = await createTestInstances(
              'no-changes-no-internal',
              projects['reload/general'],
            ).then(addReloadTransformers)

            dock = instances.dock
            recorder = instances.recorder

            recorder.clearRecording()

            await dock.run(reloadConfirmationCommand)
            snapshots.PRE_RELOAD = recorder.clearRecording()

            await dock.run('reload')
            snapshots.RELOAD = recorder.clearRecording()

            await dock.run(reloadConfirmationCommand)
            snapshots.POST_RELOAD = recorder.clearRecording()
          })

          it('should reload successfully', () => {
            expect(snapshots.RELOAD).toMatchSnapshot()
          })

          it('should have no changes', () => {
            const { PRE_RELOAD, POST_RELOAD } = snapshots
            const comparison = [PRE_RELOAD, POST_RELOAD].join('\n')

            expect(PRE_RELOAD).toBe(POST_RELOAD)
            expect(comparison).toMatchSnapshot()
          })
        })

        describe('Internal storage is changed', () => {
          let dock: Dock
          let recorder: Recorder
          let versionSpy: jest.SpyInstance
          const snapshots = { ...initialSnapshots }

          beforeAll(async () => {
            const instances = await createTestInstances(
              'no-changes-with-internal',
              projects['reload/general'],
            ).then(addReloadTransformers)

            dock = instances.dock
            recorder = instances.recorder
            versionSpy = jest.spyOn(config, 'version', 'get')

            recorder.clearRecording()

            await dock.run(reloadConfirmationCommand)
            snapshots.PRE_RELOAD = recorder.clearRecording()

            versionSpy.mockReturnValue('TEST-UPDATE')
            await dock.run('reload')
            snapshots.RELOAD = recorder.clearRecording()

            await dock.run(reloadConfirmationCommand)
            snapshots.POST_RELOAD = recorder.clearRecording()
          })
          afterAll(() => versionSpy.mockRestore())

          it('should reload successfully', () => {
            expect(snapshots.RELOAD).toMatchSnapshot()
          })

          it('should store changes', () => {
            const { PRE_RELOAD, POST_RELOAD } = snapshots
            const comparison = [PRE_RELOAD, POST_RELOAD].join('\n')

            expect(PRE_RELOAD).not.toBe(POST_RELOAD)
            expect(comparison).toMatchSnapshot()
          })
        })
      })

      describe('Project name was updated', () => {
        let dock: Dock
        let recorder: Recorder
        let fileSwitcher: FileSwitcher<keyof typeof cases>
        const snapshots = { ...initialSnapshots }

        beforeAll(async () => {
          const project = projects['reload/updated-name']
          const location = project.location.ABSOLUTE
          fileSwitcher = await new FileSwitcher(location, configName, cases)
            .switch('INITIAL')
          const instances = await createTestInstances('updated-name', project)
            .then(addReloadTransformers)

          dock = instances.dock
          recorder = instances.recorder

          recorder.clearRecording()

          await dock.run(reloadConfirmationCommand)
          snapshots.PRE_RELOAD = recorder.clearRecording()

          await delay(1.1)
          await fileSwitcher.switch('UPDATED')
          await dock.run('reload')
          snapshots.RELOAD = recorder.clearRecording()

          await dock.run(reloadConfirmationCommand)
          snapshots.POST_RELOAD = recorder.clearRecording()
        })
        afterAll(async () => await fileSwitcher.cleanup())

        it('should reload successfully', () => {
          expect(snapshots.RELOAD).toMatchSnapshot()
        })

        it('should store changes', () => {
          const { PRE_RELOAD, POST_RELOAD } = snapshots
          const comparison = [PRE_RELOAD, POST_RELOAD].join('\n')

          expect(PRE_RELOAD).not.toBe(POST_RELOAD)
          expect(comparison).toMatchSnapshot()
        })
      })

      describe('Project configuration was updated', () => {
        let dock: Dock
        let recorder: Recorder
        let fileSwitcher: FileSwitcher<keyof typeof cases>
        const snapshots = { ...initialSnapshots }

        beforeAll(async () => {
          const project = projects['reload/updated-config']
          const location = project.location.ABSOLUTE
          fileSwitcher = await new FileSwitcher(location, configName, cases)
            .switch('INITIAL')
          const instances = await createTestInstances('updated-config', project)
            .then(addReloadTransformers)

          dock = instances.dock
          recorder = instances.recorder

          recorder.clearRecording()

          await dock.run(reloadConfirmationCommand)
          snapshots.PRE_RELOAD = recorder.clearRecording()

          await delay(1.1)
          await fileSwitcher.switch('UPDATED')
          await dock.run('reload')
          snapshots.RELOAD = recorder.clearRecording()

          await dock.run(reloadConfirmationCommand)
          snapshots.POST_RELOAD = recorder.clearRecording()
        })
        afterAll(async () => await fileSwitcher.cleanup())

        it('should reload successfully', () => {
          expect(snapshots.RELOAD).toMatchSnapshot()
        })

        it('should store changes', () => {
          const { PRE_RELOAD, POST_RELOAD } = snapshots
          const comparison = [PRE_RELOAD, POST_RELOAD].join('\n')

          expect(PRE_RELOAD).not.toBe(POST_RELOAD)
          expect(comparison).toMatchSnapshot()
        })
      })

      describe('Project name was updated to another existing name', () => {
        let dock: Dock
        let recorder: Recorder
        let fileSwitcher: FileSwitcher<keyof typeof cases>
        const snapshots = { ...initialSnapshots }

        beforeAll(async () => {
          const project = projects['reload/updated-name-existing']
          const location = project.location.ABSOLUTE
          fileSwitcher = await new FileSwitcher(location, configName, cases)
            .switch('INITIAL')
          const namespace = 'updated-name-existing'
          const instances = await createTestInstances(namespace, project)
            .then(addReloadTransformers)
          const generalLocation = projects['reload/general'].location.ABSOLUTE

          dock = instances.dock
          recorder = instances.recorder

          await dock.run(`register ${generalLocation}`)
          recorder.clearRecording()

          await dock.run(reloadConfirmationCommand)
          snapshots.PRE_RELOAD = recorder.clearRecording()

          await delay(1.1)
          await fileSwitcher.switch('UPDATED')
          await dock.run('reload')
          snapshots.RELOAD = recorder.clearRecording()

          await dock.run(reloadConfirmationCommand)
          snapshots.POST_RELOAD = recorder.clearRecording()
        })
        afterAll(async () => {
          await fileSwitcher.cleanup()
        })

        it('should fail reload', () => {
          expect(snapshots.RELOAD).toMatchSnapshot()
        })

        it('should not store changes', () => {
          const { PRE_RELOAD, POST_RELOAD } = snapshots
          const comparison = [PRE_RELOAD, POST_RELOAD].join('\n')

          expect(PRE_RELOAD).toBe(POST_RELOAD)
          expect(comparison).toMatchSnapshot()
        })
      })

      describe('Project configuration was updated to an invalid config', () => {
        let dock: Dock
        let recorder: Recorder
        let fileSwitcher: FileSwitcher<keyof typeof cases>
        const snapshots = { ...initialSnapshots }

        beforeAll(async () => {
          const project = projects['reload/updated-to-invalid-config']
          const location = project.location.ABSOLUTE
          fileSwitcher = await new FileSwitcher(location, configName, cases)
            .switch('INITIAL')
          const namespace = 'updated-to-invalid-config'
          const instances = await createTestInstances(namespace, project)
            .then(addReloadTransformers)

          dock = instances.dock
          recorder = instances.recorder

          recorder.clearRecording()

          await dock.run(reloadConfirmationCommand)
          snapshots.PRE_RELOAD = recorder.clearRecording()

          await delay(1.1)
          await fileSwitcher.switch('UPDATED')
          await dock.run('reload')
          snapshots.RELOAD = recorder.clearRecording()

          await dock.run(reloadConfirmationCommand)
          snapshots.POST_RELOAD = recorder.clearRecording()
        })
        afterAll(async () => await fileSwitcher.cleanup())

        it('should fail reload', () => {
          expect(snapshots.RELOAD).toMatchSnapshot()
        })

        it('should not store changes', () => {
          const { PRE_RELOAD, POST_RELOAD } = snapshots
          const comparison = [PRE_RELOAD, POST_RELOAD].join('\n')

          expect(PRE_RELOAD).toBe(POST_RELOAD)
          expect(comparison).toMatchSnapshot()
        })
      })

      describe('Project config file was removed', () => {
        let dock: Dock
        let recorder: Recorder
        let fileSwitcher: FileSwitcher<keyof typeof cases>
        const snapshots = { ...initialSnapshots }

        beforeAll(async () => {
          const project = projects['reload/removed-file']
          const location = project.location.ABSOLUTE
          fileSwitcher = await new FileSwitcher(location, configName, cases)
            .switch('INITIAL')
          const instances = await createTestInstances('removed-file', project)
            .then(addReloadTransformers)

          dock = instances.dock
          recorder = instances.recorder

          recorder.clearRecording()

          await dock.run(reloadConfirmationCommand)
          snapshots.PRE_RELOAD = recorder.clearRecording()

          await fileSwitcher.cleanup()
          await dock.run('reload')
          snapshots.RELOAD = recorder.clearRecording()

          await dock.run(reloadConfirmationCommand)
          snapshots.POST_RELOAD = recorder.clearRecording()
        })

        it('should fail reload', () => {
          expect(snapshots.RELOAD).toMatchSnapshot()
        })

        it('should not store changes', () => {
          const { PRE_RELOAD, POST_RELOAD } = snapshots
          const comparison = [PRE_RELOAD, POST_RELOAD].join('\n')

          expect(PRE_RELOAD).toBe(POST_RELOAD)
          expect(comparison).toMatchSnapshot()
        })
      })
    })

    describe('No project is used', () => {
      const namespace = 'no-project-used'
      const instances = addReloadTransformers(createTestInstances(namespace))
      const { dock, recorder } = instances
      const snapshots = { ...initialSnapshots }

      beforeAll(async () => {
        const location = projects['reload/general'].location.ABSOLUTE

        await dock.run(`register ${location}`)
        recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.PRE_RELOAD = recorder.clearRecording()

        await dock.run('reload')
        snapshots.RELOAD = recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.POST_RELOAD = recorder.clearRecording()
      })

      it('should fail reload', () => {
        expect(snapshots.RELOAD).toMatchSnapshot()
      })

      it('should not store changes', () => {
        const { PRE_RELOAD, POST_RELOAD } = snapshots
        const comparison = [PRE_RELOAD, POST_RELOAD].join('\n')

        expect(PRE_RELOAD).toBe(POST_RELOAD)
        expect(comparison).toMatchSnapshot()
      })
    })
  })

  describe('Completion', () => {
    describe('Project is used', () => {
      let dock: Dock
      let recorder: Recorder

      beforeAll(async () => {
        const instances = await createTestInstances(
          'completion-project-used',
          projects['reload/general'],
        )

        dock = instances.dock
        recorder = instances.recorder
      })
      beforeEach(() => { recorder.clearRecording() })

      it('should complete with system completion by default', async () => {
        await dock.complete('reload ')

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should complete with system completion if ran with arguments', async () => {
        await dock.complete('reload ./path ./another-path')

        expect(recorder.recording).toMatchSnapshot()
      })

      describe(
        'Default options',
        caseFactory.completion.options.createDefault(
          'reload',
          () => ({ dock, recorder }),
          ['./path', './another-path'],
        ),
      )
    })

    describe('Project is not used', () => {
      const namespace = 'completion-no-project-used'
      const { dock, recorder } = createTestInstances(namespace)

      beforeAll(async () => {
        const location = projects['reload/general'].location.ABSOLUTE

        await dock.run(`register ${location}`)
      })
      beforeEach(() => recorder.clearRecording())

      it('should complete with system completion by default', async () => {
        await dock.complete('reload ')

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should complete with system completion if ran with arguments', async () => {
        await dock.complete('reload ./path ./another-path')

        expect(recorder.recording).toMatchSnapshot()
      })

      describe(
        'Default options',
        caseFactory.completion.options.createDefault(
          'reload',
          () => ({ dock, recorder }),
          ['./path', './another-path'],
        ),
      )
    })
  })

  describe(
    'Version',
    caseFactory.version.createDefault('reload', createTestInstances),
  )

  describe(
    'Help',
    caseFactory.help.createDefault('reload', createTestInstances),
  )
})
