/**
 * Primitive keys
 *
 */
type PrimitiveKey = string | number

/**
 * Primitive types
 */
type Primitive = string
  | number
  | boolean
  | symbol
  | { [key in PrimitiveKey]: Primitive }
  | Array<Primitive>
  | undefined
  | null

/**
 * Custom function memoization signature
 */
interface Memoized<Signature extends Method> {
  (...args: Parameters<Signature>): ReturnType<Signature>,
  memoized?: Record<string, ReturnType<Signature>>,
}

/**
 * Custom single call function memoization signature
 */
interface MemoizedOnce<Signature extends Method> extends Memoized<Signature> {
  memoized?: ReturnType<Signature>,
}
