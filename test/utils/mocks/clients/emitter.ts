import { Emitter as AbstractBaseEmitter } from '~/clients/emitter/abstract-base'

import type { Observable, ObserverHandler } from '#/utils/observable'
import type { Recorder } from '#/utils/recorder'

type EmitterEmittedData = [string, Array<unknown>]
type EmitterObserverHandler = ObserverHandler<EmitterEmittedData>

export type Overrides = Partial<AbstractBaseEmitter>
export type ObservableEmitter = Observable<
  AbstractBaseEmitter,
  EmitterEmittedData
>

class Emitter extends AbstractBaseEmitter implements ObservableEmitter {
  private readonly _handlers: Array<EmitterObserverHandler> = []

  constructor(readonly overrides: Overrides = {}) { super() }

  emitMessage: AbstractBaseEmitter['emitMessage'] = (...args) => {
    const override = this.overrides['emitMessage']

    this.emit('message', args)

    if (override) return override.apply(this, args)

    return this
  }

  emitWarning: AbstractBaseEmitter['emitWarning'] = (...args) => {
    const override = this.overrides['emitWarning']

    this.emit('warning', args)

    if (override) return override.apply(this, args)

    return this
  }

  emitError: AbstractBaseEmitter['emitError'] = (...args) => {
    const override = this.overrides['emitError']

    this.emit('error', args)

    if (override) return override.apply(this, args)

    return this
  }

  exit: AbstractBaseEmitter['exit'] = (...args) => {
    const override = this.overrides['exit']

    this.emit('exit', args)

    if (override) return override.apply(this, args)

    return super.exit(...args)
  }

  subscribe(handler: EmitterObserverHandler): SideEffect<Emitter> {
    this._handlers.push(handler)

    return this
  }

  unsubscribe(handler: EmitterObserverHandler): SideEffect<Emitter> {
    const index = this._handlers.indexOf(handler)

    if (index > -1) this._handlers.splice(index, 1)

    return this
  }

  emit(tag: string, args: Array<unknown>): SideEffect<Emitter> {
    this._handlers.forEach((handle) => handle(tag, args))

    return this
  }
}

export function create(overrides: Overrides = {}): ObservableEmitter {
  return new Emitter(overrides)
}

export function createRecorderHandler(
  recorder: Recorder,
): EmitterObserverHandler {
  return (tag, args) => recorder.record(`emitted-${tag}`, args)
}
