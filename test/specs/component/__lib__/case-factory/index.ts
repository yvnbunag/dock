import type { Dock } from '#/utils/dock'
import type { Recorder } from '#/utils/recorder'

export * as completion from '#/specs/component/__lib__/case-factory/completion'
export * as version from '#/specs/component/__lib__/case-factory/version'
export * as help from '#/specs/component/__lib__/case-factory/help'
export * as autoReload from '#/specs/component/__lib__/case-factory/auto-reload'

export interface TestInstances {
  dock: Dock,
  recorder: Recorder,
}

export type GetTestInstances<
  Keys extends keyof TestInstances = keyof TestInstances,
> = ()=> Promise<Pick<TestInstances, Keys>> | Pick<TestInstances, Keys>
