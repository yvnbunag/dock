import { fallback, config } from '~/config'

import { createTestInstances, FileSwitcher } from '#/specs/component/__lib__'
import { createUpdatedColumnNormalizer } from '#/utils/normalizers'
import { delay } from '#/utils/general-helpers'

import type { Project } from '#/specs/component/__lib__/test/projects'
import type { Dock } from '#/utils/dock'
import type { Recorder } from '#/utils/recorder'
import type { Instances } from '#/specs/component/__lib__/create-test-instances'

interface FileCases {
  INITIAL: string,
  UPDATED: string,
}

interface CaseProjects {
  GENERAL: {
    project: Project,
  },
  VALID_CHANGE: {
    project: Project,
    fileCases?: FileCases,
  },
  INVALID_CHANGE: {
    project: Project,
    fileCases?: FileCases,
  },
  REMOVED: {
    project: Project,
    fileCases?: Pick<FileCases, 'INITIAL'>,
  },
}

interface CaseCommands {
  runValid: (instances: Instances, project: Project)=> AsyncSideEffect,
  runInvalid: (instances: Instances, project: Project)=> AsyncSideEffect,
}

function addReloadTransformers(instances: Instances): Instances {
  instances.recorder.addTransformer(createUpdatedColumnNormalizer())

  return instances
}

const initialSnapshots = { PRE_COMMAND: '', COMMAND: '', POST_COMMAND: '' }
const reloadConfirmationCommand
  = 'projects --active-only --include-updated --include-version'
const defaultFileCases: FileCases = {
  INITIAL: 'dock.config.initial.yml',
  UPDATED: 'dock.config.updated.yml',
}
const { name: configName } = fallback.project.CONFIG

export function createIntercepted(
  { GENERAL, VALID_CHANGE, INVALID_CHANGE, REMOVED }: CaseProjects,
  caseCommands: CaseCommands,
): Method {
  return () => {
    describe('Config has no changes and internal storage not changed', () => {
      const { project } = GENERAL
      const snapshots = { ...initialSnapshots }
      let dock: Dock
      let recorder: Recorder

      beforeAll(async () => {
        const namespace = 'pre-auto-reload-no-changes-no-internal'
        const instances = await createTestInstances(namespace, project)
          .then(addReloadTransformers)

        dock = instances.dock
        recorder = instances.recorder

        recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.PRE_COMMAND = recorder.clearRecording()

        await caseCommands.runValid(instances, project)
        snapshots.COMMAND = recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.POST_COMMAND = recorder.clearRecording()
      })

      it('should run command successfully', () => {
        expect(snapshots.COMMAND).toMatchSnapshot()
      })

      it('should not reload config', () => {
        const { PRE_COMMAND, POST_COMMAND } = snapshots
        const comparison = [PRE_COMMAND, POST_COMMAND].join('\n')

        expect(PRE_COMMAND).toBe(POST_COMMAND)
        expect(comparison).toMatchSnapshot()
      })
    })

    describe('Internal storage has changed', () => {
      const { project } = GENERAL
      const snapshots = { ...initialSnapshots }
      let dock: Dock
      let recorder: Recorder
      let versionSpy: jest.SpyInstance

      beforeAll(async () => {
        const namespace = 'pre-auto-reload-no-changes-with-internal'
        const instances = await createTestInstances(namespace, project)
          .then(addReloadTransformers)

        dock = instances.dock
        recorder = instances.recorder
        versionSpy = jest.spyOn(config, 'version', 'get')

        recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.PRE_COMMAND = recorder.clearRecording()

        versionSpy.mockReturnValue('TEST-UPDATE')
        await caseCommands.runValid(instances, project)
        snapshots.COMMAND = recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.POST_COMMAND = recorder.clearRecording()
      })
      afterAll(() => versionSpy.mockRestore())

      it('should run command successfully', () => {
        expect(snapshots.COMMAND).toMatchSnapshot()
      })

      it('should reload config', () => {
        const { PRE_COMMAND, POST_COMMAND } = snapshots
        const comparison = [PRE_COMMAND, POST_COMMAND].join('\n')

        expect(PRE_COMMAND).not.toBe(POST_COMMAND)
        expect(comparison).toMatchSnapshot()
      })
    })

    describe('Config has valid changes', () => {
      const { project, fileCases = defaultFileCases } = VALID_CHANGE
      const snapshots = { ...initialSnapshots }
      let dock: Dock
      let recorder: Recorder
      let fileSwitcher: FileSwitcher<keyof typeof fileCases>

      beforeAll(async () => {
        const location = project.location.ABSOLUTE
        fileSwitcher = await new FileSwitcher(location, configName, fileCases)
          .switch('INITIAL')
        const namespace = 'pre-auto-reload-updated-config'
        const instances = await createTestInstances(namespace, project)
          .then(addReloadTransformers)

        dock = instances.dock
        recorder = instances.recorder

        recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.PRE_COMMAND = recorder.clearRecording()

        await delay(1.1)
        await fileSwitcher.switch('UPDATED')
        await caseCommands.runValid(instances, project)
        snapshots.COMMAND = recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.POST_COMMAND = recorder.clearRecording()
      })
      afterAll(async () => await fileSwitcher.cleanup())

      it('should run command successfully', () => {
        expect(snapshots.COMMAND).toMatchSnapshot()
      })

      it('should reload config', () => {
        const { PRE_COMMAND, POST_COMMAND } = snapshots
        const comparison = [PRE_COMMAND, POST_COMMAND].join('\n')

        expect(PRE_COMMAND).not.toBe(POST_COMMAND)
        expect(comparison).toMatchSnapshot()
      })
    })

    describe('Config auto reloaded with invalid command', () => {
      const { project } = GENERAL
      const snapshots = { ...initialSnapshots }
      let dock: Dock
      let recorder: Recorder
      let versionSpy: jest.SpyInstance

      beforeAll(async () => {
        const namespace
          = 'pre-auto-reload-no-changes-with-internal-invalid-command'
        const instances = await createTestInstances(namespace, project)
          .then(addReloadTransformers)

        dock = instances.dock
        recorder = instances.recorder
        versionSpy = jest.spyOn(config, 'version', 'get')

        recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.PRE_COMMAND = recorder.clearRecording()

        versionSpy.mockReturnValue('TEST-UPDATE')
        await caseCommands.runInvalid(instances, project)
        snapshots.COMMAND = recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.POST_COMMAND = recorder.clearRecording()
      })
      afterAll(() => versionSpy.mockRestore())

      it('should fail command', () => {
        expect(snapshots.COMMAND).toMatchSnapshot()
      })

      it('should reload config', () => {
        const { PRE_COMMAND, POST_COMMAND } = snapshots
        const comparison = [PRE_COMMAND, POST_COMMAND].join('\n')

        expect(PRE_COMMAND).not.toBe(POST_COMMAND)
        expect(comparison).toMatchSnapshot()
      })
    })

    describe('Config has invalid changes', () => {
      const { project, fileCases = defaultFileCases } = INVALID_CHANGE
      const snapshots = { ...initialSnapshots }
      let dock: Dock
      let recorder: Recorder
      let fileSwitcher: FileSwitcher<keyof typeof fileCases>

      beforeAll(async () => {
        const location = project.location.ABSOLUTE
        fileSwitcher = await new FileSwitcher(location, configName, fileCases)
          .switch('INITIAL')
        const namespace = 'pre-auto-reload-updated-to-invalid-config'
        const instances = await createTestInstances(namespace, project)
          .then(addReloadTransformers)

        dock = instances.dock
        recorder = instances.recorder

        recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.PRE_COMMAND = recorder.clearRecording()

        await delay(1.1)
        await fileSwitcher.switch('UPDATED')
        await caseCommands.runValid(instances, project)
        snapshots.COMMAND = recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.POST_COMMAND = recorder.clearRecording()
      })
      afterAll(async () => await fileSwitcher.cleanup())

      it('should fail command', () => {
        expect(snapshots.COMMAND).toMatchSnapshot()
      })

      it('should not reload config', () => {
        const { PRE_COMMAND, POST_COMMAND } = snapshots
        const comparison = [PRE_COMMAND, POST_COMMAND].join('\n')

        expect(PRE_COMMAND).toBe(POST_COMMAND)
        expect(comparison).toMatchSnapshot()
      })
    })

    describe('Config was removed', () => {
      const { project, fileCases = defaultFileCases } = REMOVED
      const snapshots = { ...initialSnapshots }
      let dock: Dock
      let recorder: Recorder
      let fileSwitcher: FileSwitcher<keyof typeof fileCases>

      beforeAll(async () => {
        const location = project.location.ABSOLUTE
        fileSwitcher = await new FileSwitcher(location, configName, fileCases)
          .switch('INITIAL')
        const namespace = 'pre-auto-reload-removed-file'
        const instances = await createTestInstances(namespace, project)
          .then(addReloadTransformers)

        dock = instances.dock
        recorder = instances.recorder

        recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.PRE_COMMAND = recorder.clearRecording()

        await fileSwitcher.cleanup()
        await caseCommands.runValid(instances, project)
        snapshots.COMMAND = recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.POST_COMMAND = recorder.clearRecording()
      })

      it('should fail command', () => {
        expect(snapshots.COMMAND).toMatchSnapshot()
      })

      it('should not reload config', () => {
        const { PRE_COMMAND, POST_COMMAND } = snapshots
        const comparison = [PRE_COMMAND, POST_COMMAND].join('\n')

        expect(PRE_COMMAND).toBe(POST_COMMAND)
        expect(comparison).toMatchSnapshot()
      })
    })
  }
}

export function createPostProcessed(
  { GENERAL, VALID_CHANGE, INVALID_CHANGE, REMOVED }: CaseProjects,
  caseCommands: CaseCommands,
): Method {
  return () => {
    describe('Config has no changes and internal storage not changed', () => {
      const { project } = GENERAL
      const snapshots = { ...initialSnapshots }
      let dock: Dock
      let recorder: Recorder

      beforeAll(async () => {
        const namespace = 'post-auto-reload-no-changes-no-internal'
        const instances = await createTestInstances(namespace, project)
          .then(addReloadTransformers)

        dock = instances.dock
        recorder = instances.recorder

        recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.PRE_COMMAND = recorder.clearRecording()

        await caseCommands.runValid(instances, project)
        snapshots.COMMAND = recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.POST_COMMAND = recorder.clearRecording()
      })

      it('should run command successfully', () => {
        expect(snapshots.COMMAND).toMatchSnapshot()
      })

      it('should not reload config', () => {
        const { PRE_COMMAND, POST_COMMAND } = snapshots
        const comparison = [PRE_COMMAND, POST_COMMAND].join('\n')

        expect(PRE_COMMAND).toBe(POST_COMMAND)
        expect(comparison).toMatchSnapshot()
      })
    })

    describe('Internal storage has changed', () => {
      const { project } = GENERAL
      const snapshots = { ...initialSnapshots }
      let dock: Dock
      let recorder: Recorder
      let versionSpy: jest.SpyInstance

      beforeAll(async () => {
        const namespace = 'post-auto-reload-no-changes-with-internal'
        const instances = await createTestInstances(namespace, project)
          .then(addReloadTransformers)

        dock = instances.dock
        recorder = instances.recorder
        versionSpy = jest.spyOn(config, 'version', 'get')

        recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.PRE_COMMAND = recorder.clearRecording()

        versionSpy.mockReturnValue('TEST-UPDATE')
        await caseCommands.runValid(instances, project)
        snapshots.COMMAND = recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.POST_COMMAND = recorder.clearRecording()
      })
      afterAll(() => versionSpy.mockRestore())

      it('should run command successfully', () => {
        expect(snapshots.COMMAND).toMatchSnapshot()
      })

      it('should reload config', () => {
        const { PRE_COMMAND, POST_COMMAND } = snapshots
        const comparison = [PRE_COMMAND, POST_COMMAND].join('\n')

        expect(PRE_COMMAND).not.toBe(POST_COMMAND)
        expect(comparison).toMatchSnapshot()
      })
    })

    describe('Config has valid changes', () => {
      const { project, fileCases = defaultFileCases } = VALID_CHANGE
      const snapshots = { ...initialSnapshots }
      let dock: Dock
      let recorder: Recorder
      let fileSwitcher: FileSwitcher<keyof typeof fileCases>

      beforeAll(async () => {
        const location = project.location.ABSOLUTE
        fileSwitcher = await new FileSwitcher(location, configName, fileCases)
          .switch('INITIAL')
        const namespace = 'post-auto-reload-updated-config'
        const instances = await createTestInstances(namespace, project)
          .then(addReloadTransformers)

        dock = instances.dock
        recorder = instances.recorder

        recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.PRE_COMMAND = recorder.clearRecording()

        await delay(1.1)
        await fileSwitcher.switch('UPDATED')
        await caseCommands.runValid(instances, project)
        snapshots.COMMAND = recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.POST_COMMAND = recorder.clearRecording()
      })
      afterAll(async () => await fileSwitcher.cleanup())

      it('should run command successfully', () => {
        expect(snapshots.COMMAND).toMatchSnapshot()
      })

      it('should reload config', () => {
        const { PRE_COMMAND, POST_COMMAND } = snapshots
        const comparison = [PRE_COMMAND, POST_COMMAND].join('\n')

        expect(PRE_COMMAND).not.toBe(POST_COMMAND)
        expect(comparison).toMatchSnapshot()
      })
    })

    describe('Config auto reloaded with invalid command', () => {
      const { project } = GENERAL
      const snapshots = { ...initialSnapshots }
      let dock: Dock
      let recorder: Recorder
      let versionSpy: jest.SpyInstance

      beforeAll(async () => {
        const namespace
          = 'post-auto-reload-no-changes-with-internal-invalid-command'
        const instances = await createTestInstances(namespace, project)
          .then(addReloadTransformers)

        dock = instances.dock
        recorder = instances.recorder
        versionSpy = jest.spyOn(config, 'version', 'get')

        recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.PRE_COMMAND = recorder.clearRecording()

        versionSpy.mockReturnValue('TEST-UPDATE')
        await caseCommands.runInvalid(instances, project)
        snapshots.COMMAND = recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.POST_COMMAND = recorder.clearRecording()
      })
      afterAll(() => versionSpy.mockRestore())

      it('should fail command', () => {
        expect(snapshots.COMMAND).toMatchSnapshot()
      })

      it('should not reload config', () => {
        const { PRE_COMMAND, POST_COMMAND } = snapshots
        const comparison = [PRE_COMMAND, POST_COMMAND].join('\n')

        expect(PRE_COMMAND).toBe(POST_COMMAND)
        expect(comparison).toMatchSnapshot()
      })
    })

    describe('Config has invalid changes', () => {
      const { project, fileCases = defaultFileCases } = INVALID_CHANGE
      const snapshots = { ...initialSnapshots }
      let dock: Dock
      let recorder: Recorder
      let fileSwitcher: FileSwitcher<keyof typeof fileCases>

      beforeAll(async () => {
        const location = project.location.ABSOLUTE
        fileSwitcher = await new FileSwitcher(location, configName, fileCases)
          .switch('INITIAL')
        const namespace = 'post-auto-reload-updated-to-invalid-config'
        const instances = await createTestInstances(namespace, project)
          .then(addReloadTransformers)

        dock = instances.dock
        recorder = instances.recorder

        recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.PRE_COMMAND = recorder.clearRecording()

        await delay(1.1)
        await fileSwitcher.switch('UPDATED')
        await caseCommands.runValid(instances, project)
        snapshots.COMMAND = recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.POST_COMMAND = recorder.clearRecording()
      })
      afterAll(async () => await fileSwitcher.cleanup())

      it('should successfully handle command but fail post processing', () => {
        expect(snapshots.COMMAND).toMatchSnapshot()
      })

      it('should not reload config', () => {
        const { PRE_COMMAND, POST_COMMAND } = snapshots
        const comparison = [PRE_COMMAND, POST_COMMAND].join('\n')

        expect(PRE_COMMAND).toBe(POST_COMMAND)
        expect(comparison).toMatchSnapshot()
      })
    })

    describe('Config was removed', () => {
      const { project, fileCases = defaultFileCases } = REMOVED
      const snapshots = { ...initialSnapshots }
      let dock: Dock
      let recorder: Recorder
      let fileSwitcher: FileSwitcher<keyof typeof fileCases>

      beforeAll(async () => {
        const location = project.location.ABSOLUTE
        fileSwitcher = await new FileSwitcher(location, configName, fileCases)
          .switch('INITIAL')
        const namespace = 'post-auto-reload-removed-file'
        const instances = await createTestInstances(namespace, project)
          .then(addReloadTransformers)

        dock = instances.dock
        recorder = instances.recorder

        recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.PRE_COMMAND = recorder.clearRecording()

        await fileSwitcher.cleanup()
        await caseCommands.runValid(instances, project)
        snapshots.COMMAND = recorder.clearRecording()

        await dock.run(reloadConfirmationCommand)
        snapshots.POST_COMMAND = recorder.clearRecording()
      })

      it('should successfully handle command but fail post processing', () => {
        expect(snapshots.COMMAND).toMatchSnapshot()
      })

      it('should not reload config', () => {
        const { PRE_COMMAND, POST_COMMAND } = snapshots
        const comparison = [PRE_COMMAND, POST_COMMAND].join('\n')

        expect(PRE_COMMAND).toBe(POST_COMMAND)
        expect(comparison).toMatchSnapshot()
      })
    })
  }
}
