import fs from 'fs'

import webpack from 'webpack'
import Ajv from 'ajv/dist/jtd'
import standaloneCode from 'ajv/dist/standalone'

import {
  standaloneCodeLocation,
  defaultOptions,
  rootPath,
} from '~/validation/config'
import { schema } from '~/validation/config/schema'

import type { Compiler } from 'webpack'

const buildStandaloneValidationCodePlugin = (() => {
  function buildStandaloneValidationCode(): SideEffect {
    const ajv = new Ajv({
      ...defaultOptions,
      code: { source: true },
    })
    const validate = ajv.compile(schema)
    const moduleCode = standaloneCode(ajv, validate)

    fs.writeFileSync(standaloneCodeLocation, moduleCode)
  }

  function deleteStandaloneValidationCode(): SideEffect {
    if (fs.existsSync(standaloneCodeLocation)) {
      fs.unlinkSync(standaloneCodeLocation)
    }
  }

  const pluginName = `${rootPath}/build`

  return {
    apply: (compiler: Compiler): void => {
      compiler.hooks.beforeCompile.tap(
        pluginName,
        buildStandaloneValidationCode,
      )
      compiler.hooks.done.tap(
        pluginName,
        deleteStandaloneValidationCode,
      )
    },
  }
})()

const replaceValidationImportPlugin = new webpack.NormalModuleReplacementPlugin(
  new RegExp(`${rootPath}/runtime`),
  `${rootPath}/standalone`,
)

export const plugins = [
  buildStandaloneValidationCodePlugin,
  replaceValidationImportPlugin,
]
