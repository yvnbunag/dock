import type * as commander from 'commander'

export class OptionMediator{
  private _optionSource: Record<string, ()=> Primitive> = {}

  add(name: string, source: commander.Command): void {
    this._optionSource[name] = () => source.opts()[name]
  }

  has(name: string): boolean {
    return Boolean(this._optionSource[name])
  }

  get(name: string): Primitive {
    if (name in this._optionSource) return this._optionSource[name]()
  }
}
