export function time(): ()=> Seconds {
  const start = process.hrtime()

  return () => {
    const [seconds, nanoseconds] = process.hrtime(start)
    const combined = seconds + (nanoseconds / Math.pow(10,9))

    return Math.round( combined * 1e2 ) / 1e2
  }
}
