import { Emitter as AbstractBaseEmitter } from '~/clients/emitter/abstract-base'

import { create } from '#/utils/mocks/clients/emitter'
import { getFirstArguments } from '#/utils/mock-helpers'
import { NOOP, suppress } from '#/utils/general-helpers'

import type { Overrides } from '#/utils/mocks/clients/emitter'

describe('Methods', () => {
  describe('emitMessage', () => {
    it('should emit message to observers', () => {
      const emitter = create()
      const observer = jest.fn()

      emitter.subscribe(observer)
      emitter.emitMessage('emit-message')

      expect(observer).toHaveBeenCalledTimes(1)
      expect(getFirstArguments(observer)).toMatchSnapshot()
    })

    it('should be override-able', () => {
      const emitMessage = jest.fn()

      create({ emitMessage }).emitMessage('emit-message')

      const [firstArgument] = getFirstArguments(emitMessage)

      expect(emitMessage).toHaveBeenCalledTimes(1)
      expect(firstArgument).toMatchInlineSnapshot(`"emit-message"`)
    })
  })

  describe('emitWarning', () => {
    it('should emit warning to observers', () => {
      const emitter = create()
      const observer = jest.fn()

      emitter.subscribe(observer)
      emitter.emitWarning('emit-warning')

      expect(observer).toHaveBeenCalledTimes(1)
      expect(getFirstArguments(observer)).toMatchSnapshot()
    })

    it('should be override-able', () => {
      const emitWarning = jest.fn()

      create({ emitWarning }).emitWarning('emit-warning')

      const [firstArgument] = getFirstArguments(emitWarning)

      expect(emitWarning).toHaveBeenCalledTimes(1)
      expect(firstArgument).toMatchInlineSnapshot(`"emit-warning"`)
    })
  })

  describe('emitError', () => {
    it('should emit error to observers', () => {
      const emitter = create()
      const observer = jest.fn()

      emitter.subscribe(observer)
      emitter.emitError('emit-error')

      expect(observer).toHaveBeenCalledTimes(1)
      expect(getFirstArguments(observer)).toMatchSnapshot()
    })

    it('should be override-able', () => {
      const emitError = jest.fn()

      create({ emitError }).emitError('emit-error')

      const [firstArgument] = getFirstArguments(emitError)

      expect(emitError).toHaveBeenCalledTimes(1)
      expect(firstArgument).toMatchInlineSnapshot(`"emit-error"`)
    })
  })

  describe('exit', () => {
    it('should invoke base exit if not overridden', () => {
      const baseExitSpy = jest
        .spyOn(AbstractBaseEmitter.prototype, 'exit')
        .mockImplementationOnce(<AbstractBaseEmitter['exit']>NOOP)

      create().exit(1)

      expect(baseExitSpy).toHaveBeenCalledTimes(1)
      expect(getFirstArguments(baseExitSpy)).toMatchSnapshot()

      baseExitSpy.mockRestore()
    })

    it('should emit exit to observers', () => {
      const emitter = create()
      const observer = jest.fn()

      emitter.subscribe(observer)
      suppress(() => emitter.exit(1))

      expect(observer).toHaveBeenCalledTimes(1)
      expect(getFirstArguments(observer)).toMatchSnapshot()
    })

    it('should be override-able', () => {
      const exit = jest.fn()
      const overrides = { exit } as unknown as Pick<Overrides, 'exit'>

      create(overrides).exit(1)

      expect(exit).toHaveBeenCalledTimes(1)
      expect(getFirstArguments(exit)[0]).toMatchInlineSnapshot(`1`)
    })
  })

  describe('subscribe', () => {
    it('should observe if subscribed', () => {
      const emitter = create()
      const observer = jest.fn()

      emitter.emitMessage('message-before-subscribe')
      emitter.subscribe(observer)
      emitter.emitMessage('message-after-subscribe')

      expect(observer.mock.calls).toMatchSnapshot()
    })
  })

  describe('unsubscribe', () => {
    it('should stop observing if unsubscribed', () => {
      const emitter = create()
      const observer = jest.fn()

      emitter.emitMessage('message-before-subscribe')
      emitter.subscribe(observer)
      emitter.emitMessage('message-after-subscribe')
      emitter.emitMessage('message-before-unsubscribe')
      emitter.unsubscribe(observer)
      emitter.emitMessage('message-after-unsubscribe')

      expect(observer.mock.calls).toMatchSnapshot()
    })
  })
})
