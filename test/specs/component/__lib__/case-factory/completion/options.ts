import { defaultOptions } from '~/config'

import type { GetTestInstances, TestInstances } from '#/specs/component/__lib__/case-factory'


export function createProvidedBeforeArguments(
  subCommand: string,
  getTestInstances: GetTestInstances<'dock' | 'recorder'>,
  commandArguments: [string,...Array<string>],
): Method {
  return () => {
    const sortedCommandArguments = [...commandArguments].sort()
    const [firstArg, ...succeedingArgs] = sortedCommandArguments
    let dock: TestInstances['dock']
    let recorder: TestInstances['recorder']

    beforeAll(async () => {
      const instances = await getTestInstances()

      dock = instances.dock
      recorder = instances.recorder
    })
    beforeEach(() => recorder.clearRecording())

    it('should complete for single option', async () => {
      await dock.complete(`${subCommand} -h `)

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should complete for multiple options', async () => {
      await dock.complete(`${subCommand} -h -v `)

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should complete for partial command argument', async () => {
      await dock.complete(`${subCommand} --help ${firstArg.slice(0, 1)}`)

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should complete for succeeding arguments', async () => {
      await dock.complete(`${subCommand} --help ${firstArg} `)

      expect(recorder.recording).toMatchSnapshot()
    })

    if (succeedingArgs.length) {
      it('should complete for options in between multiple command arguments', async () => {
        const [secondArg] = succeedingArgs

        await dock.complete(
          `${subCommand} --help ${firstArg} -h ${secondArg} --version `,
        )

        expect(recorder.recording).toMatchSnapshot()
      })
    }
  }
}

export function createDefault(
  subCommand: string,
  getTestInstances: GetTestInstances<'dock' | 'recorder'>,
  commandArguments: Array<string> = [],
): Method {
  return () => {
    const sortedCommandArguments = [...commandArguments].sort()
    const [firstArg, ...succeedingArgs] = sortedCommandArguments
    let dock: TestInstances['dock']
    let recorder: TestInstances['recorder']

    beforeAll(async () => {
      const instances = await getTestInstances()

      dock = instances.dock
      recorder = instances.recorder
    })
    beforeEach(() => recorder.clearRecording())

    it('should complete if ran with partial option', async () => {
      await dock.complete(`${subCommand} -`)
      await dock.complete(`${subCommand} --`)

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should complete if ran with multiple options', async () => {
      await dock.complete(`${subCommand} -h -`)
      await dock.complete(`${subCommand} -h --help --`)

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should complete if ran with unknown option', async () => {
      await dock.complete(`${subCommand} --unknown-option --ve`)

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should complete if ran with all options', async () => {
      await dock.complete(`${subCommand} ${defaultOptions.ALL.join(' ')}`)

      expect(recorder.recording).toMatchSnapshot()
    })

    if (!firstArg) return

    it('should complete if ran with command argument', async () => {
      await dock.complete(`${subCommand} ${firstArg} --ve`)

      expect(recorder.recording).toMatchSnapshot()
    })

    if (!succeedingArgs.length) return

    it('should complete if ran with multiple command arguments', async () => {
      await dock.complete(
        `${subCommand} --help ${sortedCommandArguments.join(' ')} --ve`,
      )

      expect(recorder.recording).toMatchSnapshot()
    })
  }
}

export function createNonDefault(
  subCommand: string,
  getTestInstances: GetTestInstances<'dock' | 'recorder'>,
  optionArguments: [string,...Array<string>],
): Method {
  return () => {
    const sortedOptionArguments = [...optionArguments].sort()
    const [firstArg, ...succeedingArgs] = sortedOptionArguments
    let dock: TestInstances['dock']
    let recorder: TestInstances['recorder']

    beforeAll(async () => {
      const instances = await getTestInstances()

      dock = instances.dock
      recorder = instances.recorder
    })
    beforeEach(() => recorder.clearRecording())

    it('should complete if ran with partial option', async () => {
      await dock.complete(`${subCommand} ${firstArg.slice(0, -1)}`)

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should complete if ran with full option', async () => {
      await dock.complete(`${subCommand} ${firstArg}`)

      expect(recorder.recording).toMatchSnapshot()
    })

    if (succeedingArgs.length) {
      it('should complete succeeding options', async () => {
        const [secondArg] = succeedingArgs

        await dock.complete(
          `${subCommand} ${firstArg} ${secondArg.slice(0, -1)}`,
        )

        expect(recorder.recording).toMatchSnapshot()
      })
    }

    it('should complete if ran with all options', async () => {
      await dock.complete(`${subCommand} ${sortedOptionArguments.join(' ')}`)

      expect(recorder.recording).toMatchSnapshot()
    })
  }
}

export function createAll(
  subCommand: string,
  getTestInstances: GetTestInstances<'dock' | 'recorder'>,
  optionArguments: [string,...Array<string>],
): Method {
  return () => {
    const sortedOptionArguments = [...optionArguments].sort()
    let dock: TestInstances['dock']
    let recorder: TestInstances['recorder']

    beforeAll(async () => {
      const instances = await getTestInstances()

      dock = instances.dock
      recorder = instances.recorder
    })

    it('should complete if ran with all options', async () => {
      const options = [
        ...sortedOptionArguments,
        ...defaultOptions.ALL,
      ].join(' ')

      await dock.complete(`${subCommand} ${options}`)

      expect(recorder.recording).toMatchSnapshot()
    })
  }
}
