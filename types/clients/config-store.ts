export type Config = { [key in string]: unknown | Config }

export type Key<ConfigImplementation extends Config>
  = keyof ConfigImplementation & string

/**
 * Config store abstraction manager
 */
export interface ConfigProvider<ConfigImplementation extends Config = Config> {
  get<KeyImplementation extends Key<ConfigImplementation>>(
    key: KeyImplementation
  ): Promise<ConfigImplementation[KeyImplementation]>,

  set<KeyImplementation extends Key<ConfigImplementation>>(
    key: KeyImplementation,
    value: ConfigImplementation[KeyImplementation]
  ): Promise<ConfigImplementation[KeyImplementation]>,

  getAll(): Promise<ConfigImplementation>,

  setAll(values: ConfigImplementation): Promise<ConfigImplementation>,

  clear(): AsyncSideEffect,
}

/**
 * ConfigProvider factory for grouping in a namespace
 */
export interface ConfigSource {
  getProvider<ConfigImplementation extends Config = Config>(
    name: string,
    defaultConfig?: ConfigImplementation | Partial<ConfigImplementation>
  ): Promise<ConfigProvider<ConfigImplementation>>,
}
