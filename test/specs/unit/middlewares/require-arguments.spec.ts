import { requireArguments } from '~/middlewares'

import { Recorder } from '#/utils/recorder'
import { optionMediator as optionMediatorFactory } from '#/utils/mocks/lib'
import { emitter as emitterFactory } from '#/utils/mocks/clients'
import { NOOP } from '#/utils/general-helpers'
import { createOptions } from '#/utils/command'

const recorder = new Recorder()
const emitter = emitterFactory
  .create({ exit: <emitterFactory.Overrides['exit']>NOOP })
  .subscribe(emitterFactory.createRecorderHandler(recorder))
const optionMediator = optionMediatorFactory.create()
const provisions = { emitter, optionMediator }
const options = createOptions({ provisions })

describe(`'until' argument is not provided`, () => {
  const intercept = requireArguments('command [firstArgument] [secondArgument]')

  beforeEach(() => {
    jest.clearAllMocks()
    recorder.clearRecording()
  })

  describe('All arguments should be required when intercepting command', () => {
    it('should exit with error if no arguments are provided', () => {
      intercept([], options)

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should exit with error if some arguments are provided', () => {
      intercept(['firstArgument'], options)

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should continue if all arguments are provided', () => {
      intercept(['firstArgument', 'secondArgument'], options)

      expect(recorder.recording).toBeFalsy()
    })
  })
})

describe(`'until' argument is less than arguments count`, () => {
  const intercept = requireArguments(
    'command [firstArgument] [secondArgument]',
    1,
  )

  beforeEach(() => {
    jest.clearAllMocks()
    recorder.clearRecording()
  })

  describe('All arguments until provided count should be required when intercepting command', () => {
    it('should exit with error if no arguments are provided', () => {
      intercept([], options)

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should continue if all required arguments are provided', () => {
      intercept(['firstArgument'], options)

      expect(recorder.recording).toBeFalsy()
    })

    it('should continue if all arguments are provided', () => {
      intercept(['firstArgument', 'secondArgument'], options)

      expect(recorder.recording).toBeFalsy()
    })
  })
})

describe(`'until' argument is less than 1`, () => {
  describe('All arguments should be optional', () => {
    describe.each([0, -1])('until %d', (until) => {
      const intercept = requireArguments(
        'command [firstArgument] [secondArgument]',
        until,
      )

      beforeEach(() => {
        jest.clearAllMocks()
        recorder.clearRecording()
      })

      it('should continue if no arguments are provided', () => {
        intercept([], options)

        expect(recorder.recording).toBeFalsy()
      })

      it('should continue if all arguments are provided', () => {
        intercept(['firstArgument', 'secondArgument'], options)

        expect(recorder.recording).toBeFalsy()
      })
    })
  })
})

describe(`'until' argument is greater than arguments count`, () => {
  const intercept = requireArguments(
    'command [firstArgument] [secondArgument]',
    99,
  )

  beforeEach(() => {
    jest.clearAllMocks()
    recorder.clearRecording()
  })

  describe('All defined arguments should be required when intercepting command', () => {
    it('should exit with error if no arguments are provided', () => {
      intercept([], options)

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should exit with error if some arguments are provided', () => {
      intercept(['firstArgument'], options)

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should continue if all arguments are provided', () => {
      intercept(['firstArgument', 'secondArgument'], options)

      expect(recorder.recording).toBeFalsy()
    })
  })
})
