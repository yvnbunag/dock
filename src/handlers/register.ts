import { load } from 'js-yaml'

import { fallback } from '~/config'
import { validate } from '~/validation/config/runtime'
import { programHelpers } from '~/lib'
import { completeFilename, extractCompletionMetadata } from '~/lib/completion'

import type { Provisions } from '@/command'
import type { Handler, CompletionHandler } from '@/handler'
import type { Project } from '@/config'

type Arguments = [string]

export const handle: Handler<Arguments> = async function (
  [path],
  { provisions },
) {
  const configPath = programHelpers.projects.configPath
    .normalize(path, provisions.environmentReader.runPath)
  const fileName = `${configPath}/${fallback.project.CONFIG.name}`
  const configExists = await programHelpers.projects.config.exists(fileName)

  if (!configExists) {
    return programHelpers.projects.config.handleNotFound(fileName, provisions)
  }

  const [file, updated] = await Promise.all([
    programHelpers.projects.config.read(fileName),
    programHelpers.projects.config.getUpdateDate(fileName),
  ])
  const config = load(file) as Project.RawConfig
  const error = validate(config)

  if (error) {
    return programHelpers.projects.config
      .handleValidationError(error, provisions)
  }

  const projects = await provisions.programMediator.getProjects()

  if (projects.names.includes(config.name)) {
    return programHelpers.projects.name
      .handleAlreadyRegistered(config.name, provisions)
  }

  if (projects.configPaths.includes(configPath)) {
    return programHelpers.projects.configPath
      .handleAlreadyRegistered(configPath, provisions)
  }

  await handleRegister(config, configPath, updated, provisions)
}

async function handleRegister(
  rawConfig: Project.RawConfig,
  configPath: Project.ConfigPath,
  updated: Project.Updated,
  provisions: Provisions,
): AsyncSideEffect {
  const { rawConfig: { name } } = await provisions.programMediator
    .loadProject(rawConfig, configPath, updated)

  provisions.emitter.emitMessage(
    `Project '${name}' at ${configPath} registered`,
  )
}

export const complete: CompletionHandler<Arguments> = async function (
  commandArguments,
  { provisions },
) {
  const metadata = extractCompletionMetadata(provisions)

  if (metadata.isOptionCompletion()) {
    return programHelpers.completion.completeOptions(metadata, provisions)
  }

  return completeFilename(provisions.emitter)
}
