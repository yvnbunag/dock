import { reload } from '~/handlers'
import { fallback } from '~/config'
import { version } from '~/config/config'
import { config } from '~/lib/program-helpers/projects'

import type { Middleware } from '@/middleware'

export const autoReload: Middleware = async function (
  commandArguments,
  options,
) {
  const currentConfig = await options.provisions.programMediator
    .getUsedProjectProvider()
    .then((provider) => provider.getAll())

  if (currentConfig.version !== version) {
    return reload.handle([], options)
  }

  const fileName = `${currentConfig.configPath}/${fallback.project.CONFIG.name}`
  const updated = await config.getUpdateDate(fileName)

  if (currentConfig.updated !== updated) {
    return reload.handle([], options)
  }
}
