import { Recorder } from '#/utils/recorder'
import { primitive } from '#/utils/values'

describe('Recording', () => {
  it('should return blank string if no record is stored', () => {
    expect(new Recorder().recording).toBe('')
  })
})

describe('Methods', () => {
  describe('record', () => {
    const recorder = new Recorder()

    beforeAll(() => {
      recorder.record('empty-record')
      recorder.record('single-record', ['single-entry'])
      recorder.record('multiple-records', Object.values(primitive))
    })

    it('should record', () => {
      expect(recorder.recording).toMatchSnapshot()
    })
  })

  describe('clearRecording', () => {
    const recorder = new Recorder()

    beforeAll(() => {
      recorder.record('before-clear')
      recorder.clearRecording()
      recorder.record('after-clear')
    })

    it('should clear recording', () => {
      expect(recorder.recording).toMatchSnapshot()
    })
  })

  describe('addTransformer', () => {
    const recorder = new Recorder()
    const record = { property: 'value' }
    const recording = { NO_TRANSFORMER: '', WITH_TRANSFORMER: '' }
    const stringify = (record: unknown) => JSON.stringify(record, null, 2)

    beforeAll(() => {
      recorder.record('add-transformer', [record])
      recording.NO_TRANSFORMER = recorder.recording
      recorder.addTransformer(stringify)
      recording.WITH_TRANSFORMER = recorder.recording
    })

    it('should transform recording', () => {
      const { NO_TRANSFORMER, WITH_TRANSFORMER } = recording

      expect(WITH_TRANSFORMER).not.toBe(NO_TRANSFORMER)
      expect(WITH_TRANSFORMER).toMatchSnapshot()
    })
  })

  describe('removeTransformer', () => {
    const recorder = new Recorder()
    const record = { property: 'value' }
    const recording = { WITH_TRANSFORMER: '', REMOVED_TRANSFORMER: '' }
    const stringify = (record: unknown) => JSON.stringify(record, null, 2)

    beforeAll(() => {
      recorder.record('remove-transformer', [record])
      recorder.addTransformer(stringify)
      recording.WITH_TRANSFORMER = recorder.recording
      recorder.removeTransformer(stringify)
      recording.REMOVED_TRANSFORMER = recorder.recording
    })

    it('should remove transformer', () => {
      const { REMOVED_TRANSFORMER, WITH_TRANSFORMER } = recording

      expect(REMOVED_TRANSFORMER).not.toBe(WITH_TRANSFORMER)
      expect(REMOVED_TRANSFORMER).toMatchSnapshot()
    })
  })
})