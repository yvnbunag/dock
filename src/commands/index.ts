import * as commander from 'commander'

import * as completion from '~/commands/completion'
import * as register from '~/commands/register'
import * as projects from '~/commands/projects'
import * as use from '~/commands/use'
import * as build from '~/commands/build'
import * as install from '~/commands/install'
import * as up from '~/commands/up'
import * as down from '~/commands/down'
import * as mode from '~/commands/mode'
import * as open from '~/commands/open'
import * as run from '~/commands/run'
import * as reload from '~/commands/reload'
import * as unregister from '~/commands/unregister'
import { mount, root } from '~/handlers'
import { requireUse, requireUsedConfigFile, autoReload } from '~/middlewares'
import { globalFlags, defaultOptions } from '~/config'
import {
  isCompletion,
  completeWithErrorFallback,
  isStrictCompletion,
} from '~/lib/completion'

import { version } from '+/package.json'

import type { GlobalFlag } from '~/config/global-flags'
import type * as commandType from '@/command'
import type { Provisions } from '@/command'
import type { Handler, CompletionHandler } from '@/handler'
import type { Middleware } from '@/middleware'

const args = '[service-command...]'
const usage = [
  '',
  '- dock [options] [command]',
  `- dock [options] ${args}`,
].join('\n')
const description = [
  'Given that the first argument is not a command and current directory is a service reference:',
  '- Open interactive shell of service container if no arguments are provided',
  '- Run command in service container if arguments are provided',
].join('\n')
const argumentDescriptions = {
  'service-command': '(Optional) Commands to run in service container',
}

export const create: commandType.Create = (provisions) => {
  const subCommands = [
    completion,
    register,
    projects,
    use,
    build,
    install,
    up,
    down,
    mode,
    open,
    run,
    reload,
    unregister,
  ]
  const { handle, complete } = root.create(
    mount.handle,
    mount.complete,
    subCommands,
  )
  const handler = createActionHandler(
    handle,
    complete,
    provisions,
    [requireUse, requireUsedConfigFile, autoReload],
  )
  const command = new commander.Command()
    .arguments(args)
    .usage(usage)
    .description(description, argumentDescriptions)
    .action(handler)
    .allowUnknownOption()

  registerSubCommands(subCommands, command, provisions)
  registerGlobalFlags(Object.values(globalFlags), command, provisions)

  return decorateWithDefaults(command, provisions)
}

/**
 * Decorate command with default configurations
 */
export function decorateWithDefaults(
  command: commander.Command,
  provisions: Provisions,
): commander.Command {
  if (isStrictCompletion(provisions)) {
    command.allowUnknownOption()
    command.helpOption(false)

    return command
  }

  const versionOptions = defaultOptions.VERSION.join(', ')
  const helpOptions = defaultOptions.HELP.join(', ')

  command.version(version, versionOptions, 'Output the package version')
  command.helpOption(helpOptions, 'Display help for command')

  return command
}

/**
 * Create command action handler
 */
export function createActionHandler<
  ProvidedArguments extends commandType.CommandArguments,
>(
  handle: Handler<ProvidedArguments>,
  complete: CompletionHandler<ProvidedArguments>,
  provisions: commandType.Provisions,
  interceptors: Array<Middleware> = [],
  postProcessors: Array<Middleware> = [],
): ()=> AsyncSideEffect {
  return async (...actionArguments: commandType.ActionArguments) => {
    const commandArguments = actionArguments.slice(0, -2) as ProvidedArguments
    const [
      option,
      command,
    ] = actionArguments.slice(-2) as [commandType.Option, commandType.Command]
    const options: commandType.Options = {
      provisions,
      option,
      command,
    }

    if (isCompletion(provisions) || isStrictCompletion(provisions)) {
      return await completeWithErrorFallback(
        () => complete(commandArguments, options),
        provisions.emitter,
      )
    }

    for (const intercept of interceptors) {
      await intercept(commandArguments, options)
    }

    await handle(commandArguments, options)

    for (const postProcess of postProcessors) {
      await postProcess(commandArguments, options)
    }
  }
}

/**
 * Create sub command via its factory and add to command
 */
export function registerSubCommands(
  subCommands: Array<commandType.Factory>,
  command: commander.Command,
  provisions: commandType.Provisions,
): commander.Command {
  subCommands.forEach(
    (subCommand) => command.addCommand(subCommand.create(provisions)),
  )

  return command
}

/**
 * Add global flag options to command and option mediator
 */
export function registerGlobalFlags(
  flags: Array<GlobalFlag>,
  command: commander.Command,
  provisions: commandType.Provisions,
): commander.Command {
  flags.forEach(({ name, option }) => {
    command.addOption(option)
    provisions.optionMediator.add(name, command)
  })

  return command
}
