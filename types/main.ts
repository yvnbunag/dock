import type { EnvironmentReader } from '~/lib/environment-reader'

import type { Emitter } from '@/clients'
import type { ConfigSource } from '@/clients/config-store'
import type { ExecuteCommand } from '~/lib/execute-command'

/**
 * Program dependencies for automated test injection usage
 */
export interface Dependencies {
  readonly $emitter?: Emitter,
  readonly $environmentReader?: EnvironmentReader,
  readonly $configSource?: ConfigSource,
  readonly $executeCommand?: ExecuteCommand,
  readonly $createErrorHandler?: (emitter: Emitter)=> (error: Error)=> void,
}

export type RawNodeArguments = string

export type NodeArguments = Readonly<Array<string>>
