import {
  OrchestrationEngine as DockerCompose,
} from '~/clients/orchestration-engine/docker-compose'
import { Engine } from '@/config/project'

import type { ExecuteCommand } from '~/lib/execute-command'
import type {
  OrchestrationEngineFactory as OrchestrationEngineFactoryInterface,
  OrchestrationEngineConstructor,
  OrchestrationEngineInterface,
} from '@/clients/orchestration-engine'
import type { ConfigPath } from '@/config/project'

export class OrchestrationEngineFactory
implements OrchestrationEngineFactoryInterface
{
  private _cache: Partial<Record<Engine, OrchestrationEngineInterface>> = {}

  constructor(private readonly executeCommand: ExecuteCommand) {}

  create(
    configPath: ConfigPath,
    engine: Engine = Engine.DOCKER_COMPOSE,
  ): OrchestrationEngineInterface {
    if (!(engine in this._cache)) {
      this._cache[engine] = new OrchestrationEngineFactory.ENGINES[engine](
        configPath,
        this.executeCommand,
      )
    }

    return this._cache[engine] as OrchestrationEngineInterface
  }

  static ENGINES: Record<Engine, OrchestrationEngineConstructor> = {
    [Engine.DOCKER_COMPOSE]: DockerCompose,
  }
}

export {
  DockerCompose,
}
