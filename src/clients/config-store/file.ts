import ConfigStore from 'configstore'
import rimraf from 'rimraf'

import type {
  ConfigProvider as ConfigProviderInterface,
  ConfigSource as ConfigSourceInterface,
  Config as ConfigInterface,
  Key as KeyInterface,
} from '@/clients/config-store'

export class ConfigProvider<
  Config extends ConfigInterface = ConfigInterface,
> implements ConfigProviderInterface<Config> {
  constructor(private readonly configStore: ConfigStore) {}

  async get<Key extends KeyInterface<Config>>(key: Key): Promise<Config[Key]> {
    return this.configStore.get(key)
  }

  async set<Key extends KeyInterface<Config>>(
    key: Key,
    value: Config[Key],
  ): Promise<Config[Key]> {
    this.configStore.set(key, value)

    return value
  }

  async getAll(): Promise<Config> {
    return { ...this.configStore.all }
  }

  async setAll(values: Config): Promise<Config> {
    this.configStore.all = values

    return { ...values }
  }

  clear(): AsyncSideEffect {
    return new Promise((resolve, reject) => {
      try {
        this.configStore.clear()

        rimraf(this.configStore.path, () => resolve())
      } catch (err) {
        reject(err)
      }
    })
  }
}

export class ConfigSource implements ConfigSourceInterface {
  constructor(private readonly namespace: string) {}

  async getProvider<Config extends ConfigInterface = ConfigInterface>(
    name: string,
    defaultConfig: Config | Partial<Config> = {},
  ): Promise<ConfigProvider<Config>> {
    const configStore = new ConfigStore(
      `${this.namespace}/${name}`,
      defaultConfig,
    )

    return new ConfigProvider(configStore)
  }
}
