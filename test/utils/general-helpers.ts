import { replace } from 'lodash'

export function NOOP<Method extends (...args: Array<unknown>)=> unknown>(
  ...args: Parameters<Method> // eslint-disable-line @typescript-eslint/no-unused-vars
  // @ts-expect-error noop
): ReturnType<Method> {
  // eslint-disable-line @typescript-eslint/no-empty-function
}

export function suppress<ExpectedError extends Error>(
  operation: ()=> unknown,
): Nullable<ExpectedError> {
  try { operation() }
  catch (error) { return error }

  return null
}

export function replaceString(
  matcher: string,
  replacement: string,
): <Type extends unknown>(value: Type)=> Type {
  return <Type extends unknown>(value: Type): Type => {
    if (typeof value !== 'string') return value

    return replace(value, new RegExp(matcher, 'g'), replacement) as Type
  }
}

export function replaceStringInstance(
  pattern: RegExp,
  replaceInstance: (instance: number)=> string,
): <Type extends unknown>(value: Type)=> Type {
  let count = 0
  const instanceMap: Record<string, number> = {}

  return <Type extends unknown>(value: Type): Type => {
    if (typeof value !== 'string') return value

    const matches = [...(value.match(pattern) || [])]
    const replacedInstance = matches.reduce(
      (currentValue, currentMatch) => {
        if (!(currentMatch in instanceMap)) instanceMap[currentMatch] = ++count

        return replace(
          currentValue,
          new RegExp(currentMatch, 'g'),
          replaceInstance(instanceMap[currentMatch]),
        )
      },
      value,
    )

    return replacedInstance as Type
  }
}

export function delay(seconds: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, seconds * 1000))
}
