import { platform as getPlatform } from 'os'
import { basename, resolve } from 'path'

import { Memoize } from '@/process/env'

type EnvironmentProcess = Pick<NodeJS.Process, 'env' | 'cwd' | 'mainModule'>

export class EnvironmentReader {
  private readonly _process: EnvironmentProcess

  constructor({
    env = global.process.env,
    cwd = global.process.cwd,
  }: Partial<EnvironmentProcess> = {}) {
    this._process = { env, cwd }
  }

  get platform(): NodeJS.Platform { return getPlatform() }

  get memoize(): boolean {
    return !('memoize' in this._process.env)
      || this._process.env.memoize === Memoize.TRUE
  }

  get runPath(): string {
    return this._process.cwd()
  }

  get runDirectory(): string {
    return basename(resolve(this.runPath))
  }

  get scriptDirectory(): string {
    return require?.main?.path
      || this._process?.mainModule?.path
      || __dirname // Fallback for bundled script
      || this._process.cwd()
  }
}

export const environmentReader = new EnvironmentReader()
