import type { JTDSchemaType } from 'ajv/dist/jtd'

import { Project } from '@/config'

const containerNameSchema: JTDSchemaType<Project.ContainerName> = {
  type: 'string',
}

const containerSchema: JTDSchemaType<Project.Container> = {
  optionalProperties: {
    shell: { enum: Object.values(Project.Shell) },
    description: { type: 'string' },
  },
}

const serviceNameSchema: JTDSchemaType<Project.ServiceName> = {
  type: 'string',
}

const serviceSchema: JTDSchemaType<Project.Service> = {
  properties: { container: containerNameSchema },
  optionalProperties: {
    'entry-point': { type: 'string' },
    install: { elements: { type: 'string' } },
    description: { type: 'string' },
  },
}

const modeSchema: JTDSchemaType<Project.Mode> = {
  optionalProperties: {
    containers: { elements: containerNameSchema },
    services: { elements: serviceNameSchema },
    description: { type: 'string' },
  },
}

export const schema: JTDSchemaType<Project.RawConfig> = {
  properties: {
    name: { type: 'string' },
  },
  optionalProperties: {
    engine: { enum: Object.values(Project.Engine) },
    containers: { values: containerSchema },
    services: { values: serviceSchema },
    modes: { values: modeSchema },
    description: { type: 'string' },
  },
}
