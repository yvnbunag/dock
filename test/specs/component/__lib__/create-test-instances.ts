import { ConfigSource } from '~/clients/config-store/memory'

import { Dock } from '#/utils/dock'
import { Recorder } from '#/utils/recorder'
import { emitter } from '#/utils/mocks/clients'
import { createRootPathNormalizer } from '#/utils/normalizers'
import { executeCommand, environmentReader } from '#/utils/mocks/lib'

import type { Project } from '#/specs/component/__lib__/test/projects'
import type { ObservableEmitter } from '#/utils/mocks/clients/emitter'
import type { ObservableExecuteCommand } from '#/utils/mocks/lib/execute-command'
import type { TestEnvironmentReader } from '#/utils/mocks/lib/environment-reader'

export type Instances = {
  $configSource: ConfigSource,
  $environmentReader: TestEnvironmentReader,
  $emitter: ObservableEmitter,
  $executeCommand: ObservableExecuteCommand,
  dock: Dock,
  recorder: Recorder,
}

export function createTestInstances(
  namespace?: string
): Instances;
export function createTestInstances(
  namespace?: string,
  preRegisteredProject?: Project
): Promise<Instances>;
export function createTestInstances(
  namespace = 'dock',
  preRegisteredProject?: Project,
): Instances | Promise<Instances> {
  const $configSource = new ConfigSource(namespace)
  const $environmentReader = environmentReader.create()
  const recorder = new Recorder().addTransformer(createRootPathNormalizer())
  const $emitter = emitter.create()
    .subscribe(emitter.createRecorderHandler(recorder))
  const $executeCommand = executeCommand.create()
    .subscribe(executeCommand.createRecorderHandler(recorder))
  const dock = new Dock({
    $emitter,
    $executeCommand,
    $configSource,
    $environmentReader,
  })
  const instances = {
    $configSource,
    $environmentReader,
    recorder,
    $emitter,
    $executeCommand,
    dock,
  }

  if (!preRegisteredProject) return instances

  return new Promise<Instances>((resolve) => {
    dock.run(`register ${preRegisteredProject.location.ABSOLUTE}`)
      .then(() => dock.run(`use ${preRegisteredProject.name}`))
      .then(() => resolve(instances))
  })
}
