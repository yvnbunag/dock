import { upperFirst } from 'lodash'

import { time } from '~/lib'

import { Emitter } from '@/clients'

export function logDuration<Invoke extends Async<Method>>(
  invoke: Invoke,
  emitter: Emitter,
  operation = '',
): (...args: Parameters<Invoke>)=> Promise<Awaited<ReturnType<Invoke>>> {
  return async (...args) => {
    const endTime = time()
    const result = await invoke(...args)
    const duration = upperFirst(`${operation} done in ${endTime()}s`.trim())

    emitter.emitMessage(duration)

    return result
  }
}
