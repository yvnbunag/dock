# Configuration Reference

## Schema

### name

Name and reference to the project

**Type:** string

**Required**

```yml
name: library-system
```

---

### engine

Docker orchestration engine of the project

**Type:** string

**Default:** docker-compose

**Values:**
- docker-compose

**Optional**

```yml
name: library-system

engine: docker-compose
```

---

### description

Project description

**Type:** string

**Optional**

```yml
name: library-system

description: Library System for Software Development with Quality Assurance
```

---

### containers

Container definitions. Key maps into a service name of a docker-compose
configuration

**Type:** Record&lt;string, Object&gt;

**Optional**

```yml
name: library-system

containers: {}
```

---

### containers[container].shell

Shell to use when performing actions in container

**Type:** string

**Default:** bash

**Values:**
- bash
- sh

**Optional**

```yml
name: library-system

containers:
  node16:
    shell: sh
```

---

### containers[container].description

Container description

**Type:** string

**Optional**

```yml
name: library-system

containers:
  node16:
    description: Container for the front and back end services
```

---

### services

Service definitions. Each service may share a container

**Type:** Record&lt;string, Object&gt;

**Optional**

```yml
name: library-system

services: {}
```

---

### services[service].container

Container reference

**Type:** string

**Required**

```yml
name: library-system

containers:
  node16: {}

services:
  library-system:
    container: node16
```

---

### services[service].entry-point

Entry point path of service when performing actions in its container

**Type:** string

**Optional**

```yml
name: library-system

containers:
  node16: {}

services:
  library-system:
    container: node16
    entry-point: ./applications/library-system
```

> When configuring with a relative path, root path would be the container's
> working directory

---

### services[service].install

Service install commands / scripts

**Type:** Array&lt;string&gt;

**Optional**

```yml
name: library-system

containers:
  node16: {}

services:
  library-system:
    container: node16
    install:
      - yarn
      - cp /shared/applications/library-system/.env .env
```

---

### services[service].description

Service description

**Type:** string

**Optional**

```yml
name: library-system

containers:
  node16: {}

services:
  library-system:
    container: node16
    description: Front end application
```

---

### modes

Mode definitions. May reference both or one of services and containers

**Type:** Record&lt;string, Object&gt;

**Optional**

```yml
name: library-system

modes: {}
```

---

### modes[mode].containers

Container references

**Type:** Array&lt;string&gt;

**Optional**

```yml
name: library-system

containers:
  mysql57: {}
  mysql80: {}
  node16: {}

modes:
  database:
    containers:
      - mysql57
      - mysql80
```

---

### modes[mode].services

Service references

**Type:** Array&lt;string&gt;

**Optional**

```yml
name: library-system

containers:
  mysql57: {}
  mysql80: {}
  node16: {}

services:
  library-system:
    container: node16
  library-system-api:
    container: node16

modes:
  app:
    services:
      - library-system
      - library-system-api
```

---

### modes[mode].description

Mode description

**Type:** string

**Optional**

<br/>

```yml
name: library-system

containers:
  mysql57: {}
  mysql80: {}
  node16: {}

services:
  library-system:
    container: node16
  library-system-api:
    container: node16

modes:
  app:
    services:
      - library-system
      - library-system-api
    description: Front and back end services
```

---

## Example

From [Library System Stack](https://gitlab.com/ianbunag/library-system-stack)

```yml
name: library-system

containers:
  nginx:
    shell: sh
  mysql: {}
  node16:
    shell: sh

services:
  library-system:
    container: node16
    entry-point: ./applications/library-system
    install:
      - yarn
      - cp /shared/applications/library-system/.env .env
  library-system-api:
    container: node16
    entry-point: ./microservices/library-system-api
    install:
      - yarn
      - cp /shared/microservices/library-system-api/.env .env
      - yarn database:sync
      - yarn database:seed

modes:
  front-end:
    containers:
      - nginx
    services:
      - library-system
  back-end:
    containers:
      - nginx
      - mysql
    services:
      - library-system-api
  full-stack:
    containers:
      - nginx
      - mysql
    services:
      - library-system
      - library-system-api
```
