import type {
  GetTestInstances,
  TestInstances,
} from '#/specs/component/__lib__/case-factory'

export function createDefault(
  subCommand: string,
  getTestInstances: GetTestInstances<'dock'>,
): Method {
  return () => {
    let dock: TestInstances['dock']

    beforeAll(async () => {
      dock = (await getTestInstances()).dock
    })

    it('should show help text', async () => {
      await expect(dock.getHelp(subCommand)).resolves.toMatchSnapshot()
    })
  }
}
