import { NoUsedProjectError } from '~/lib/program-mediator/errors'
import { nameAndArgs as useNameAndArgs } from '~/commands/use'

import type { Middleware } from '@/middleware'

type RequireUse = Middleware<'emitter' | 'programMediator'>

export const requireUse: RequireUse = async function (
  commandArguments,
  { provisions },
) {
  try {
    await provisions.programMediator.getUsedProjectProvider()
  } catch (error) {
    if (!(error instanceof NoUsedProjectError)) throw error

    return provisions.emitter
      .emitError(error.message)
      .emitError(`Run dock ${useNameAndArgs} to use project`)
      .exit(error.code)
  }
}
