declare module 'escape-quotes' {
  function escapeQuotes(
    source: string,
    chars?: string,
    escapeChar?: string
  ): string

  export = escapeQuotes
}