import { getFirstArguments, getAllArguments } from '#/utils/mock-helpers'

describe('getFirstArguments', () => {
  it('should return undefined if mock function was not invoked', () => {
    expect(getFirstArguments(jest.fn())).toBeUndefined()
  })

  describe('Mock function was invoked', () => {
    const firstArguments = [Symbol('first-1'), Symbol('first-2')]
    const lastArguments = [Symbol('last-1'), Symbol('last-2')]
    let mock: jest.Mock,
      result: ReturnType<typeof getFirstArguments>

    beforeAll(() => {
      mock = jest.fn().mockReturnValue(true)

      mock(...firstArguments) && mock(...lastArguments)

      result = getFirstArguments(mock)
    })

    it('should return first arguments', () => {
      expect(result).toMatchSnapshot()
    })

    it('should not mutate mock calls', () => {
      expect(mock.mock.calls).toMatchSnapshot()
    })
  })
})

describe('getAllArguments', () => {
  it('should return empty array if mock function was not invoked', () => {
    expect(getAllArguments(jest.fn())).toStrictEqual([])
  })

  describe('Mock function was invoked', () => {
    const firstArguments = [Symbol('first-1'), Symbol('first-2')]
    const middleArguments = [Symbol('middle-1'), Symbol('middle-2')]
    const lastArguments = [Symbol('last-1'), Symbol('last-2')]
    let mock: jest.Mock,
      result: ReturnType<typeof getAllArguments>

    beforeAll(() => {
      mock = jest.fn().mockReturnValue(true)

      mock(...firstArguments)
      mock(...middleArguments)
      mock(...lastArguments)

      result = getAllArguments(mock)
    })

    it('should return all arguments', () => {
      expect(result).toMatchSnapshot()
    })

    it('should not mutate mock calls', () => {
      expect(mock.mock.calls).toMatchSnapshot()
    })
  })
})