import { TemporaryWriteStreamMediator } from '#/utils/temporary-write-stream-mediator'

import { Memoize } from '@/process/env'

/**
 * Make terminal width consistent across environments
 * - To force commander help text outputs to have consistent width
 */
process.stdout.columns = undefined as unknown as number
process.stderr.columns = undefined as unknown as number

/**
 * Disable memoization during tests
 */
process.env.memoize = Memoize.FALSE

/**
 * Intercept table dependency to normalize columns with value equal to root path
 *  for cross environment automated tests
 */
jest.mock(
  'table',
  jest.requireActual('#/utils/decorated-dependencies/table').create,
)

/**
 * Mock chalk to produce snapshot readable results
 */
jest.mock(
  'chalk',
  jest.requireActual('#/utils/decorated-dependencies/chalk').create,
)

/**
 * Remap exit code keys into their values
 */
jest.mock(
  '~/config/exit-codes',
  jest.requireActual('#/utils/mocks/config/exit-codes').create,
)

/**
 * Allow config to be spied and overridden
 */
jest.mock(
  '~/config/config',
  jest.requireActual('#/utils/mocks/config/config').create,
)

/**
 * Clean up first since non default exits (e.g. SIGINT signal) does not wait for
 *  clean up script to finish
 */
TemporaryWriteStreamMediator.cleanupDefaultDirectory()
