import { Emitter as AbstractBaseEmitter } from '~/clients/emitter/abstract-base'
import { DockExit } from '~/errors'
import { suppress } from '#/utils/general-helpers'

class AbstractBaseEmitterImplementer extends AbstractBaseEmitter {
  /* eslint-disable @typescript-eslint/no-empty-function */
  emitMessage(): AbstractBaseEmitterImplementer {
    return this
  }
  emitWarning(): AbstractBaseEmitter {
    return this
  }
  emitError(): AbstractBaseEmitterImplementer {
    return this
  }
  /* eslint-enable @typescript-eslint/no-empty-function */
}

describe('Abstract base emitter', () => {
  describe('Concrete implementations', () => {
    describe('exit', () => {
      describe('It should throw dock exit with provided exit code', () => {
        test.each([1, 10, 100])('exit code %d', async (code) => {
          const implementer = new AbstractBaseEmitterImplementer()
          const error = suppress<DockExit>(() => implementer.exit(code))

          expect(error).toBeInstanceOf(DockExit)
          expect(error?.code).toBe(code)
        })
      })
    })
  })

  describe('Static methods', () => {
    describe('formatMessage', () => {
      it('should format message', () => {
        expect(AbstractBaseEmitter.formatMessage('message')).toMatchSnapshot()
      })
    })

    describe('formatWarning', () => {
      it('should format warning', () => {
        expect(AbstractBaseEmitter.formatWarning('warning')).toMatchSnapshot()
      })
    })

    describe('formatError', () => {
      it('should format error', () => {
        expect(AbstractBaseEmitter.formatError('error')).toMatchSnapshot()
      })
    })
  })
})
