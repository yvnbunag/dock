import { config } from '~/lib/program-helpers/projects'
import { fallback } from '~/config'

import type { Middleware } from '@/middleware'

type RequireUsedConfigFile = Middleware<'emitter' | 'programMediator'>

export const requireUsedConfigFile: RequireUsedConfigFile = async function (
  commandArguments,
  { provisions },
) {
  const configPath = await provisions.programMediator
    .getUsedProjectProvider()
    .then((provider) => provider.get('configPath'))
  const fileName = `${configPath}/${fallback.project.CONFIG.name}`
  const configExists = await config.exists(fileName)

  if (configExists) return

  return config.handleNotFound(fileName, provisions)
}
