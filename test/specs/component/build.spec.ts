import { options } from '~/handlers/build'

import { createTestInstances, caseFactory } from '#/specs/component/__lib__'
import { projects } from '#/specs/component/__lib__/test'
import { mockHrtime } from '#/utils/mocks/process'
import { Engine } from '@/config/project'

import type { Project } from '#/specs/component/__lib__/test/projects'
import type { Dock } from '#/utils/dock'
import type { Recorder } from '#/utils/recorder'

mockHrtime()

describe('Component - build [reference...]', () => {
  describe('Engine', () => {
    const projectsMap: Record<
      Engine,
      Parameters<typeof createEngineSuite>[1]
    > = {
      [Engine.DOCKER_COMPOSE]: {
        WITH_REFERENCES: projects['build/engine/docker-compose/with-references'],
        NO_REFERENCE: projects['build/engine/docker-compose/no-reference'],
      },
    }

    describe(
      ...createEngineSuite(
        Engine.DOCKER_COMPOSE,
        projectsMap[Engine.DOCKER_COMPOSE],
      ),
    )
  })

  describe('Completion', () => {
    const flags = Object.values(options).map(({ flag }) => flag)

    describe('Project is used', () => {
      describe('Project has references', () => {
        let dock: Dock
        let recorder: Recorder

        beforeAll(async () => {
          const instances = await createTestInstances(
            'completion-with-references',
            projects['build/with-references'],
          )

          dock = instances.dock
          recorder = instances.recorder
        })
        beforeEach(() => { recorder.clearRecording() })

        it('should complete if ran with no arguments', async () => {
          await dock.complete('build ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete if ran with partial reference', async () => {
          await dock.complete('build container-with-')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should filtered complete if ran with complete reference', async () => {
          await dock.complete('build container-with-service-1 ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should filtered complete if ran with multiple references', async () => {
          await dock.complete('build container-with-service-1 service-1 ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete if ran with undefined reference', async () => {
          await dock.complete('build undefined-1 ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete with system completion if ran with exhausted references', async () => {
          const references = [
            'container-no-service-1',
            'container-with-service-1',
            'container-with-service-2',
            'container-same-with-service',
            'service-1',
            'service-2',
            'service-with-install-1',
            'service-with-install-2',
          ]

          await dock.complete(`build ${references.join(' ')} `)

          expect(recorder.recording).toMatchSnapshot()
        })

        describe(
          'Options are provided before command arguments',
          caseFactory.completion.options.createProvidedBeforeArguments(
            'build',
            () => ({ dock, recorder }),
            ['service-1', 'service-2'],
          ),
        )

        describe(
          'Default options',
          caseFactory.completion.options.createDefault(
            'build',
            () => ({ dock, recorder }),
            ['service-1', 'service-2'],
          ),
        )

        describe(
          'Non default options',
          caseFactory.completion.options.createNonDefault(
            'build',
            () => ({ dock, recorder }),
                flags as [string, ...Array<string>],
          ),
        )

        describe(
          'All options',
          caseFactory.completion.options.createAll(
            'build',
            () => ({ dock, recorder }),
                flags as [string, ...Array<string>],
          ),
        )
      })

      describe('Project has no references', () => {
        let dock: Dock
        let recorder: Recorder

        beforeAll(async () => {
          const instances = await createTestInstances(
            'completion-no-reference',
            projects['build/no-reference'],
          )

          dock = instances.dock
          recorder = instances.recorder
        })
        beforeEach(() => { recorder.clearRecording() })

        it('should complete with system completion if ran with no arguments', async () => {
          await dock.complete('build ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete with system completion if ran with arguments', async () => {
          await dock.complete('build ./path ')

          expect(recorder.recording).toMatchSnapshot()
        })

        describe(
          'Options are provided before command arguments',
          caseFactory.completion.options.createProvidedBeforeArguments(
            'build',
            () => ({ dock, recorder }),
            ['./path', './another-path'],
          ),
        )

        describe(
          'Default options',
          caseFactory.completion.options.createDefault(
            'build',
            () => ({ dock, recorder }),
            ['./path', './another-path'],
          ),
        )

        describe(
          'Non default options',
          caseFactory.completion.options.createNonDefault(
            'build',
            () => ({ dock, recorder }),
                flags as [string, ...Array<string>],
          ),
        )

        describe(
          'All options',
          caseFactory.completion.options.createAll(
            'build',
            () => ({ dock, recorder }),
                flags as [string, ...Array<string>],
          ),
        )
      })
    })

    describe('Project is not used', () => {
      const namespace = 'completion-no-project-used'
      const { dock, recorder } = createTestInstances(namespace)

      beforeEach(() => { recorder.clearRecording() })

      it('should complete with system completion if ran with no arguments', async () => {
        await dock.complete('build ')

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should complete with system completion if ran with arguments', async () => {
        await dock.complete('build ./path ')

        expect(recorder.recording).toMatchSnapshot()
      })

      describe(
        'Options are provided before command arguments',
        caseFactory.completion.options.createProvidedBeforeArguments(
          'build',
          () => ({ dock, recorder }),
          ['./path', './another-path'],
        ),
      )

      describe(
        'Default options',
        caseFactory.completion.options.createDefault(
          'build',
          () => ({ dock, recorder }),
          ['./path', './another-path'],
        ),
      )

      describe(
        'Non default options',
        caseFactory.completion.options.createNonDefault(
          'build',
          () => ({ dock, recorder }),
              flags as [string, ...Array<string>],
        ),
      )

      describe(
        'All options',
        caseFactory.completion.options.createAll(
          'build',
          () => ({ dock, recorder }),
              flags as [string, ...Array<string>],
        ),
      )
    })
  })

  describe(
    'Auto reload',
    caseFactory.autoReload.createIntercepted(
      {
        GENERAL: { project: projects['build/with-references'] },
        VALID_CHANGE: { project: projects['build/updated-valid'] },
        INVALID_CHANGE: { project: projects['build/updated-invalid'] },
        REMOVED: { project: projects['build/removed'] },
      },
      {
        runValid: ({ dock }) => dock.run('build'),
        runInvalid: ({ dock }) => dock.run('build undefined'),
      },
    ),
  )

  describe(
    'Version',
    caseFactory.version.createDefault('build', createTestInstances),
  )

  describe(
    'Help',
    caseFactory.help.createDefault('build', createTestInstances),
  )
})

function createEngineSuite(
  engine: Engine,
  projectsMap: {
    WITH_REFERENCES: Project,
    NO_REFERENCE: Project,
  },
): [Engine, Method] {
  function createNameSpace(name: string) { return `${engine}-${name}`}

  function suite() {
    describe('Command', () => {
      describe('Project is used', () => {
        describe('Project has references', () => {
          let dock: Dock
          let recorder: Recorder

          beforeAll(async () => {
            const namespace = createNameSpace('with-references')
            const instances = await createTestInstances(
              namespace,
              projectsMap.WITH_REFERENCES,
            )

            dock = instances.dock
            recorder = instances.recorder
          })
          beforeEach(() => recorder.clearRecording())

          it('should build all containers if ran with no references', async () => {
            await dock.run('build')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should build container if ran with a container reference', async () => {
            await dock.run('build container-no-service-1')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should build containers if ran with multiple container references', async () => {
            await dock.run(
              'build container-no-service-1 container-with-service-1',
            )

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should build service container if ran with a service reference', async () => {
            await dock.run('build service-1')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should build service containers if ran with multiple service references', async () => {
            await dock.run('build service-1 service-2')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should build containers if ran with all types of references', async () => {
            await dock.run('build container-with-service-1 service-2')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should build containers if ran with intersecting container and service container references', async () => {
            await dock.run('build container-same-with-service')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should build containers if ran with non unique references', async () => {
            const references = [
              'container-with-service-1',
              'container-with-service-1',
              'service-2',
              'service-2',
            ]

            await dock.run(`build ${references.join(' ')}`)

            expect(recorder.recording).toMatchSnapshot()
          })

          describe('It should emit error if container or service reference is not defined', () => {
            test('in only argument', async () => {
              await dock.run('build undefined-1')

              expect(recorder.recording).toMatchSnapshot()
            })

            test('in one of multiple arguments', async () => {
              await dock.run('build undefined-1 container-with-service-1')

              expect(recorder.recording).toMatchSnapshot()
            })

            test('in many of multiple arguments', async () => {
              const references = [
                'undefined-1',
                'undefined-2',
                'container-with-service-1',
                'service-1',
              ]
              await dock.run(`build ${references.join(' ')}`)

              expect(recorder.recording).toMatchSnapshot()
            })

            test('in all arguments', async () => {
              await dock.run('build undefined-1 undefined-2 undefined-3')

              expect(recorder.recording).toMatchSnapshot()
            })
          })

          describe('Options', () => {
            describe('--install', () => {
              it('should install if ran with no references', async () => {
                await dock.run('build --install')

                expect(recorder.recording).toMatchSnapshot()
              })

              describe('Ran with references', () => {
                describe('Service reference', () => {
                  it('should skip install for service with no install script', async () => {
                    await dock.run('build service-1 --install')

                    expect(recorder.recording).toMatchSnapshot()
                  })

                  it('should skip install for multiple services with no install scripts', async () => {
                    await dock.run('build service-1 service-2 --install')

                    expect(recorder.recording).toMatchSnapshot()
                  })

                  it('should install for service with install script', async () => {
                    await dock.run('build service-with-install-1 --install')

                    expect(recorder.recording).toMatchSnapshot()
                  })

                  it('should install for multiple services with install scripts', async () => {
                    await dock.run(
                      'build service-with-install-1 service-with-install-2 --install',
                    )

                    expect(recorder.recording).toMatchSnapshot()
                  })

                  it('should install for multiple services with and without install scripts', async () => {
                    await dock.run(
                      'build service-1 service-with-install-2 --install',
                    )

                    expect(recorder.recording).toMatchSnapshot()
                  })
                })

                describe('Container reference', () => {
                  it('should skip install for container', async () => {
                    await dock.run('build container-with-service-1 --install')

                    expect(recorder.recording).toMatchSnapshot()
                  })

                  it('should skip install for multiple containers', async () => {
                    await dock.run(
                      'build container-with-service-1 container-with-service-2 --install',
                    )

                    expect(recorder.recording).toMatchSnapshot()
                  })
                })
              })

              it('should install if ran with service and container references', async () => {
                const references = [
                  'container-no-service-1',
                  'service-with-install-1',
                ]

                await dock.run(`build ${references.join(' ')} --install`)

                expect(recorder.recording).toMatchSnapshot()
              })

              it('should install if ran with multiple service and container references', async () => {
                const references = [
                  'container-no-service-1',
                  'container-same-with-service',
                  'service-with-install-1',
                  'service-with-install-2',
                ]

                await dock.run(`build ${references.join(' ')} --install`)

                expect(recorder.recording).toMatchSnapshot()
              })
            })
          })
        })

        describe('Project has no reference', () => {
          let dock: Dock
          let recorder: Recorder

          beforeAll(async () => {
            const namespace = createNameSpace('no-reference')
            const instances = await createTestInstances(
              namespace,
              projectsMap.NO_REFERENCE,
            )

            dock = instances.dock
            recorder = instances.recorder
          })
          beforeEach(() => recorder.clearRecording())

          it('should build all containers if ran with no references', async () => {
            await dock.run('build')

            expect(recorder.recording).toMatchSnapshot()
          })

          describe('It should emit error if container or service reference is provided', () => {
            test('for one argument', async () => {
              await dock.run('build service-1')

              expect(recorder.recording).toMatchSnapshot()
            })

            test('for multiple arguments', async () => {
              await dock.run('build service-1 service-2 container-no-service-1')

              expect(recorder.recording).toMatchSnapshot()
            })
          })

          describe('Options', () => {
            describe('--install', () => {
              it('should skip install if ran with no references', async () => {
                await dock.run('build --install')

                expect(recorder.recording).toMatchSnapshot()
              })

              it('should emit error if ran with references', async () => {
                await dock.run(
                  'build service-1 service-2 container-no-service-1 --install',
                )

                expect(recorder.recording).toMatchSnapshot()
              })
            })
          })
        })
      })

      describe('Project is not used', () => {
        const namespace = createNameSpace('no-project-used')
        const { dock, recorder } = createTestInstances(namespace)
        const location = projectsMap.WITH_REFERENCES.location.ABSOLUTE

        beforeAll(async () => await dock.run(`register ${location}`))
        beforeEach(() => recorder.clearRecording())

        it('should emit error if ran with no arguments', async () => {
          await dock.run('build')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should emit error if ran with arguments', async () => {
          await dock.run('build service-1 service-2')

          expect(recorder.recording).toMatchSnapshot()
        })

        describe('Options', () => {
          describe('--install', () => {
            it('should emit error if ran with no references', async () => {
              await dock.run('build --install')

              expect(recorder.recording).toMatchSnapshot()
            })

            it('should emit error if ran with references', async () => {
              await dock.run('build service-1 service-2 --install')

              expect(recorder.recording).toMatchSnapshot()
            })
          })
        })
      })
    })
  }

  return [engine, suite]
}
