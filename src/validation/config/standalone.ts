/**
 * @file This is only used for production, to consume AJV's generated standalone
 *  validation code on build
 */
/* istanbul ignore file */
// @ts-expect-error Standalone code is generated on build
import schemaValidate from '~/validation/config/standalone-code'
import { rootValidate } from '~/validation/config'

import type { Validate } from '~/validation/config'

export const validate: Validate = (config) => {
  return rootValidate(config, schemaValidate)
}
