import { createTestInstances, caseFactory } from '#/specs/component/__lib__'
import { projects } from '#/specs/component/__lib__/test'
import { createContextSwitch } from '#/utils/environment'
import { Engine } from '@/config/project'

import type { Project } from '#/specs/component/__lib__/test/projects'
import type { Dock } from '#/utils/dock'
import type { Recorder } from '#/utils/recorder'
import type { ContextSwitch } from '#/utils/environment'

const { dock, recorder } = createTestInstances()

describe('Component - root', () => {
  describe('Engine', () => {
    const projectsMap: Record<
      Engine,
      Parameters<typeof createEngineSuite>[1]
    > = {
      [Engine.DOCKER_COMPOSE]: {
        ROOT: projects['root/engine/docker-compose'],
        WITH_REFERENCES: projects['root/engine/docker-compose/with-references'],
        NO_REFERENCE: projects['root/engine/docker-compose/no-reference'],
      },
    }

    describe(
      ...createEngineSuite(
        Engine.DOCKER_COMPOSE,
        projectsMap[Engine.DOCKER_COMPOSE],
      ),
    )
  })

  describe('Completion', () => {
    beforeEach(() => recorder.clearRecording())

    it('should complete if ran with no sub command', async () => {
      await dock.complete('')

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should complete if ran with partial sub command', async () => {
      await dock.complete('r')

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should complete if ran with full sub command', async () => {
      await dock.complete('completion')

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should complete if ran with single service container command', async () => {
      await dock.complete('yarn')

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should complete if ran with multiple service container commands', async () => {
      await dock.complete('yarn dev')

      expect(recorder.recording).toMatchSnapshot()
    })

    describe(
      'Options are provided before command arguments',
      caseFactory.completion.options.createProvidedBeforeArguments(
        '',
        () => ({ dock, recorder }),
        ['completion', 'enable'],
      ),
    )

    describe(
      'Default options',
      caseFactory.completion.options.createDefault(
        '',
        () => ({ dock, recorder }),
        ['completion', 'enable'],
      ),
    )
  })

  describe(
    'Auto reload',
    caseFactory.autoReload.createIntercepted(
      {
        GENERAL: { project: projects['root/with-references'] },
        VALID_CHANGE: { project: projects['root/updated-valid'] },
        INVALID_CHANGE: { project: projects['root/updated-invalid'] },
        REMOVED: { project: projects['root/removed'] },
      },
      {
        runValid: async ({ dock }, project) => {
          const location = `${project.location.ABSOLUTE}/service`
          const context = createContextSwitch(location)

          context.switch()
          await dock.run('')
          context.reset()
        },
        runInvalid: async ({ dock }, project) => {
          const location = `${project.location.ABSOLUTE}/non-service`
          const context = createContextSwitch(location)

          context.switch()
          await dock.run('')
          context.reset()
        },
      },
    ),
  )

  describe(
    'Version',
    caseFactory.version.createDefault('', () => ({ dock })),
  )

  describe(
    'Help',
    caseFactory.help.createDefault('', () => ({ dock })),
  )
})

function createEngineSuite(
  engine: Engine,
  projectsMap: {
    ROOT: Project,
    WITH_REFERENCES: Project,
    NO_REFERENCE: Project,
  },
): [Engine, Method] {
  function createNameSpace(name: string) { return `${engine}-${name}`}

  function suite() {
    const rootContext = createContextSwitch(projectsMap.ROOT.location.ABSOLUTE)

    describe('Command', () => {
      rootContext.scope()

      describe('Project is used', () => {
        describe('Project has service references', () => {
          const project = projectsMap.WITH_REFERENCES
          let dock: Dock
          let recorder: Recorder

          beforeAll(async () => {
            const namespace = createNameSpace('has-service-references')
            const instances = await createTestInstances(namespace, project)

            dock = instances.dock
            recorder = instances.recorder
          })
          beforeEach(() => recorder.clearRecording())

          describe('Run directory is a service reference', () => {
            const cases = [
              [
                'basic service',
                'service',
              ],
              [
                'service with entry point',
                'service-entry-point',
              ],
              [
                'service with shell',
                'service-with-shell',
              ],
              [
                'service with same reference as its container',
                'container-same-service',
              ],
              [
                'service with same reference as its container, entry point and shell',
                'container-same-service-with-entry-point-and-shell',
              ],
            ]

            it.each(cases)(
              'should open service container if ran with no argument for %s',
              async (description, reference) => {
                const location = `${project.location.ABSOLUTE}/${reference}`
                const context = createContextSwitch(location)

                context.switch()

                await dock.run('')

                expect(recorder.recording).toMatchSnapshot()

                context.reset()
              },
            )

            describe('Ran with arguments', () => {
              describe.each(cases)(
                'It should run command arguments in service container for %s',
                (description, reference) => {
                  const location = `${project.location.ABSOLUTE}/${reference}`
                  let context: ContextSwitch

                  beforeEach(() => {
                    context = createContextSwitch(location)

                    context.switch()
                  })

                  afterEach(() => context.reset())

                  test('with single command', async () => {
                    await dock.run('yarn')

                    expect(recorder.recording).toMatchSnapshot()
                  })

                  test('with multiple commands', async () => {
                    await dock.run('yarn dev')

                    expect(recorder.recording).toMatchSnapshot()
                  })

                  test('with option', async () => {
                    await dock.run('yarn dev --verbose')

                    expect(recorder.recording).toMatchSnapshot()
                  })

                  test('with quoted command', async () => {
                    await dock.run(`'yarn'`)

                    expect(recorder.recording).toMatchSnapshot()
                  })

                  test('with multiple commands, multiple options and varying quoted arguments', async () => {
                    await dock.run(
                      `'yarn' --production "custom install" --verbose`,
                    )

                    expect(recorder.recording).toMatchSnapshot()
                  })
                },
              )
            })
          })

          describe('Run directory is not a service reference', () => {
            it('should notify usage if ran with no arguments', async () => {
              await dock.run('')

              expect(recorder.recording).toMatchSnapshot()
            })

            it('should notify usage if ran with single service container command', async () => {
              await dock.run('yarn')

              expect(recorder.recording).toMatchSnapshot()
            })

            it('should notify usage if ran with multiple service container commands', async () => {
              await dock.run('yarn dev')

              expect(recorder.recording).toMatchSnapshot()
            })
          })
        })

        describe('Project has no service reference', () => {
          let dock: Dock
          let recorder: Recorder

          beforeAll(async () => {
            const namespace = createNameSpace('no-service-reference')
            const instances = await createTestInstances(
              namespace,
              projectsMap.NO_REFERENCE,
            )

            dock = instances.dock
            recorder = instances.recorder
          })
          beforeEach(() => recorder.clearRecording())

          it('should notify usage if ran with no arguments', async () => {
            await dock.run('')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should notify usage if ran with single service container command', async () => {
            await dock.run('yarn')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should notify usage if ran with multiple service container commands', async () => {
            await dock.run('yarn dev')

            expect(recorder.recording).toMatchSnapshot()
          })
        })
      })

      describe('Project is not used', () => {
        const namespace = createNameSpace('no-project-used')
        const { dock,recorder } = createTestInstances(namespace)

        beforeEach(() => recorder.clearRecording())

        it('should emit error if ran with no arguments', async () => {
          await dock.run('')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should emit error if ran with single service container command', async () => {
          await dock.run('yarn')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should emit error if ran with multiple service container commands', async () => {
          await dock.run('yarn dev')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should emit error if ran with service container command and option', async () => {
          await dock.run('yarn dev --verbose')

          expect(recorder.recording).toMatchSnapshot()
        })
      })
    })
  }

  return [engine, suite]
}
