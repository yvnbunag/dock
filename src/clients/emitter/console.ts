import { Emitter as AbstractBaseEmitter } from '~/clients/emitter/abstract-base'

export class Emitter extends AbstractBaseEmitter {
  constructor(private readonly logger: Console = console) { super() }

  emitMessage(message: string): SideEffect<Emitter> {
    this.logger.log(AbstractBaseEmitter.formatMessage(message))

    return this
  }

  emitWarning(warning: string): AbstractBaseEmitter {
    this.logger.warn(AbstractBaseEmitter.formatWarning(warning))

    return this
  }

  emitError(error: string): SideEffect<Emitter> {
    this.logger.error(AbstractBaseEmitter.formatError(error))

    return this
  }
}
