import * as commander from 'commander'

import { projects } from '~/handlers'
import { createActionHandler, decorateWithDefaults } from '~/commands'

import type { Create } from '@/command'

export const name = 'projects'

const description = 'List registered projects'

export const create: Create = (provisions) => {
  const handler = createActionHandler(
    projects.handle,
    projects.complete,
    provisions,
  )
  const command = new commander.Command()
    .command(name)
    .description(description)
    .action(handler)
  const options = projects.columnOptions
    .map(({ flag, description }) => new commander.Option(flag, description))

  options.forEach((option) => command.addOption(option))

  return decorateWithDefaults(command, provisions)
}
