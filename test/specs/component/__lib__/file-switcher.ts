import { copyFile, access, unlink } from 'fs/promises'

export class FileSwitcher<CaseKeys extends string> {
  private _targetLocation: string

  constructor(
    private readonly directory: string,
    private readonly target: string,
    private readonly cases: Record<CaseKeys, string>,
  ) {
    this._targetLocation = `${this.directory}/${this.target}`
  }

  async switch(
    fileCase: CaseKeys,
  ): AsyncSideEffect<FileSwitcher<CaseKeys>> | never {
    const sourceLocation = `${this.directory}${this.cases[fileCase]}`

    await copyFile(sourceLocation, this._targetLocation)

    return this
  }

  async cleanup(): AsyncSideEffect<FileSwitcher<CaseKeys>> | never {
    const fileExists = await access(this._targetLocation)
      .then(() => true)
      .catch(() => false)

    if (fileExists) await unlink(this._targetLocation)

    return this
  }
}
