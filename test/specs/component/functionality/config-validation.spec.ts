import { createTestInstances } from '#/specs/component/__lib__'
import { projects } from '#/specs/component/__lib__/test'

import type { Project } from '#/specs/component/__lib__/test/projects'

const {
  dock,
  recorder,
  $configSource,
} = createTestInstances()

describe('Component - [command] Config validation', () => {
  describe('Config is valid', () => {
    afterAll($configSource.Class.wipe)

    it('should not emit error', async () => {
      recorder.clearRecording()

      await dock.run(`register ${projects.general.location.ABSOLUTE}`)

      expect(recorder.recording).toMatchSnapshot()
    })
  })

  describe('Config is invalid', () => {
    type Cases = Array<[string, Project]>

    const nameCases: Cases = [
      [
        'Name has incorrect type',
        projects['validation/name/has-incorrect-type'],
      ],
      [
        'Name is not provided',
        projects['validation/name/is-not-provided'],
      ],
    ]
    const containerCases: Cases = [
      [
        'Containers has incorrect type',
        projects['validation/containers/has-incorrect-type'],
      ],
      [
        'Container has incorrect type',
        projects['validation/containers/container/has-incorrect-type'],
      ],
      [
        'Container shell is invalid',
        projects['validation/containers/container/shell/is-invalid'],
      ],
      [
        'Container description has incorrect type',
        projects['validation/containers/container/description/has-incorrect-type'],
      ],
      [
        'Container has extra property',
        projects['validation/containers/container/has-extra-property'],
      ],
    ]
    const engineCases: Cases = [
      [
        'Engine is invalid',
        projects['validation/engine/is-invalid'],
      ],
    ]
    const servicesCases: Cases = [
      [
        'Services has incorrect type',
        projects['validation/services/has-incorrect-type'],
      ],
      [
        'Service has incorrect type',
        projects['validation/services/service/has-incorrect-type'],
      ],
      [
        'Service container is not provided',
        projects['validation/services/service/container/is-not-provided'],
      ],
      [
        'Service container has incorrect type',
        projects['validation/services/service/container/has-incorrect-type'],
      ],
      [
        'Service container is not defined',
        projects['validation/services/service/container/is-not-defined'],
      ],
      [
        'Service container is provided with no container definition',
        projects['validation/services/service/container/is-provided-with-no-container-definition'],
      ],
      [
        'Service entry-point has incorrect type',
        projects['validation/services/service/entry-point/has-incorrect-type'],
      ],
      [
        'Service install has incorrect type',
        projects['validation/services/service/install/has-incorrect-type'],
      ],
      [
        'Service install element has incorrect type',
        projects['validation/services/service/install/element/has-incorrect-type'],
      ],
      [
        'Service install has multiple invalid elements',
        projects['validation/services/service/install/has-multiple-invalid-elements'],
      ],
      [
        'Service description has incorrect type',
        projects['validation/services/service/description/has-incorrect-type'],
      ],
      [
        'Service has extra property',
        projects['validation/services/service/has-extra-property'],
      ],
    ]
    const modeCases: Cases = [
      [
        'Modes has incorrect type',
        projects['validation/modes/has-incorrect-type'],
      ],
      [
        'Mode has incorrect type',
        projects['validation/modes/mode/has-incorrect-type'],
      ],
      [
        'Mode containers has incorrect type',
        projects['validation/modes/mode/containers/has-incorrect-type'],
      ],
      [
        'Mode containers element has incorrect type',
        projects['validation/modes/mode/containers/element/has-incorrect-type'],
      ],
      [
        'Mode containers element is not defined',
        projects['validation/modes/mode/containers/element/is-not-defined'],
      ],
      [
        'Mode containers element is provided with no container definition',
        projects['validation/modes/mode/containers/element/is-provided-with-no-container-definition'],
      ],
      [
        'Mode containers has multiple invalid elements',
        projects['validation/modes/mode/containers/has-multiple-invalid-elements'],
      ],
      [
        'Mode containers only has no element',
        projects['validation/modes/mode/containers/only/has-no-element'],
      ],
      [
        'Mode services has incorrect type',
        projects['validation/modes/mode/services/has-incorrect-type'],
      ],
      [
        'Mode services element has incorrect type',
        projects['validation/modes/mode/services/element/has-incorrect-type'],
      ],
      [
        'Mode services element is not defined',
        projects['validation/modes/mode/services/element/is-not-defined'],
      ],
      [
        'Mode services element is provided with no container definition',
        projects['validation/modes/mode/services/element/is-provided-with-no-container-definition'],
      ],
      [
        'Mode services has multiple invalid elements',
        projects['validation/modes/mode/services/has-multiple-invalid-elements'],
      ],
      [
        'Mode services only has no element',
        projects['validation/modes/mode/services/only/has-no-element'],
      ],
      [
        'Mode has neither containers and services',
        projects['validation/modes/mode/containers-and-services/has-neither'],
      ],
      [
        'Mode containers and services has no elements',
        projects['validation/modes/mode/containers-and-services/has-no-elements'],
      ],
      [
        'Mode description has incorrect type',
        projects['validation/modes/mode/description/has-incorrect-type'],
      ],
      [
        'Mode has extra property',
        projects['validation/modes/mode/has-extra-property'],
      ],
    ]
    const descriptionCases: Cases = [
      [
        'Description has incorrect type',
        projects['validation/description/has-incorrect-type'],
      ],
    ]
    const generalCases: Cases = [
      [
        'Multiple properties are invalid',
        projects['validation/general/multiple-properties/are-invalid'],
      ],
      [
        'Config is malformed',
        projects['validation/general/config-is-malformed'],
      ],
      [
        'Config is empty',
        projects['validation/general/config-is-empty'],
      ],
    ]
    const cases: Cases = [
      ...nameCases,
      ...containerCases,
      ...engineCases,
      ...servicesCases,
      ...modeCases,
      ...descriptionCases,
      ...generalCases,
    ]

    describe.each(cases)('%s', (description, project) => {
      beforeAll(async () => {
        recorder.clearRecording()

        await dock.run(`register ${project.location.ABSOLUTE}`)
      })
      afterAll($configSource.Class.wipe)

      it('should emit error', () => {
        expect(recorder.recording).toMatchSnapshot()
      })
    })
  })
})
