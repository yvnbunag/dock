import { ProgramMediator } from '~/lib/program-mediator'

import type { ConfigSource } from '@/clients/config-store'

class MockedProgramMediator
  extends ProgramMediator
  implements jest.Mocked<ProgramMediator>
{
  loadProject = jest.fn()
  activateProject = jest.fn()
  renameProject = jest.fn()
  reloadProject = jest.fn()
  unloadProject = jest.fn()
  getProjects = jest.fn()
  getSettings = jest.fn()
  getConfig = jest.fn()
  getProjectProvider = jest.fn()
  getUsedProjectProvider = jest.fn()
  getProgramProvider = jest.fn()
}

export function create(): jest.Mocked<ProgramMediator> {
  const stubConfigSource = Symbol('config-source') as unknown as ConfigSource

  return new MockedProgramMediator(stubConfigSource)
}
