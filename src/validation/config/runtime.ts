import Ajv from 'ajv/dist/jtd'

import { rootValidate, defaultOptions } from '~/validation/config'
import { schema } from '~/validation/config/schema'

import type { Validate } from '~/validation/config'

export const validate: Validate = (config) => {
  const ajv = new Ajv(defaultOptions)
  const schemaValidate = ajv.compile(schema)

  return rootValidate(config, schemaValidate)
}
