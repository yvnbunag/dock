import * as commander from 'commander'

import { up } from '~/handlers'
import { createActionHandler, decorateWithDefaults } from '~/commands'
import { requireUse, requireUsedConfigFile, autoReload } from '~/middlewares'

import type { Create } from '@/command'

export const name = 'up'

const nameAndArgs = `${name} [reference...]`
const description = 'Bring container/s or service container/s up'
const argumentDescriptions = {
  reference:
    '(Optional) Container or service reference/s. Defaults to all containers',
}

export const create: Create = (provisions) => {
  const handler = createActionHandler(
    up.handle,
    up.complete,
    provisions,
    [requireUse, requireUsedConfigFile, autoReload],
  )
  const command = new commander.Command()
    .command(nameAndArgs)
    .description(description, argumentDescriptions)
    .action(handler)

  return decorateWithDefaults(command, provisions)
}
