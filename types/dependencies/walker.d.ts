declare module 'walker' {
  import type { Stats } from 'fs'

  interface WalkFilter {
    (directory: string, stat: Stats): boolean,
  }
  type WalkEvent = 'entry'
    | 'dir'
    | 'file'
    | 'symlink'
    | 'blockDevice'
    | 'fifo'
    | 'socket'
    | 'characterDevice'
    | 'error'
    | 'end'
  interface WalkListener {
    (entry: string, stat: Stats): void,
  }
  interface WalkErrorListener {
    (error: Error, entry: string, stat: Stats): void,
  }
  interface WalkEndListener {
    (): void,
  }

  class Walker {
    constructor()
    filterDir(filter: WalkFilter): Walker
    on<Event extends WalkEvent>(
      event: Event,
      listen: Event extends Exclude<WalkEvent, 'end' | 'error'>
        ? WalkListener
        : Event extends 'error'
          ? WalkErrorListener
          : Event extends 'end'
            ? WalkEndListener
            : never
    ): Walker
  }

  function createWalker(root: string): Walker

  export = createWalker
}
