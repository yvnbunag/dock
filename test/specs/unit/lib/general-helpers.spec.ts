import { suppress, NOOP } from '#/utils/general-helpers'

describe('suppress', () => {
  describe('Error is suppressed', () => {
    const error = new Error('suppressed-error')

    it('should return suppressed error', () => {
      expect(suppress(() => { throw error } )).toBe(error)
    })
  })

  describe('Error is not suppressed', () => {
    it('should return null', () => {
      expect(suppress(NOOP)).toBeNull()
    })
  })
})
