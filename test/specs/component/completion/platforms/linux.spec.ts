import { createTestInstances } from '#/specs/component/__lib__'

const {
  $environmentReader,
  dock,
  recorder,
} = createTestInstances()

beforeAll(() => $environmentReader.setPlatform('linux'))

describe('Component - completion [instruction]', () => {
  describe('Platform - Linux', () => {
    describe('Command', () => {
      describe('enable', () => {
        it('should emit instructions', async () => {
          jest.clearAllMocks()
          recorder.clearRecording()

          await dock.run('completion enable')

          expect(recorder.recording).toMatchSnapshot()
        })
      })

      describe('script', () => {
        it('should emit completion script', async () => {
          jest.clearAllMocks()
          recorder.clearRecording()

          await dock.run('completion script')

          expect(recorder.recording).toMatchSnapshot()
        })
      })

      describe('disable', () => {
        it('should emit instructions', async () => {
          jest.clearAllMocks()
          recorder.clearRecording()

          await dock.run('completion disable')

          expect(recorder.recording).toMatchSnapshot()
        })
      })
    })
  })
})
