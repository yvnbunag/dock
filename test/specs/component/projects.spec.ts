import { columnOptions } from '~/handlers/projects'

import { createTestInstances, caseFactory } from '#/specs/component/__lib__'
import { projects } from '#/specs/component/__lib__/test'
import { createUpdatedColumnNormalizer } from '#/utils/normalizers'


const { $configSource, dock, recorder } = createTestInstances()

recorder.addTransformer(createUpdatedColumnNormalizer())

describe('Component - projects', () => {
  describe('Command', () => {
    describe('No projects are registered', () => {
      it('should emit no projects registered message', async () => {
        recorder.clearRecording()

        await dock.run('projects')

        expect(recorder.recording).toMatchSnapshot()
      })
    })

    describe('Single project is registered', () => {
      beforeAll(async () => {
        await dock.run(`register ${projects.general.location.ABSOLUTE}`)
      })
      afterAll($configSource.Class.wipe)

      it('should list registered project', async () => {
        recorder.clearRecording()

        await dock.run('projects')

        expect(recorder.recording).toMatchSnapshot()
      })
    })

    describe('Multiple projects are registered', () => {
      beforeAll(async () => {
        await dock.run(`register ${projects.general.location.ABSOLUTE}`)
        await dock.run(`register ${projects.alternative.location.ABSOLUTE}`)
      })
      afterAll($configSource.Class.wipe)

      it('should list registered projects', async () => {
        recorder.clearRecording()

        await dock.run('projects')

        expect(recorder.recording).toMatchSnapshot()
      })
    })

    describe('Options', () => {
      beforeAll(async () => {
        await dock.run(`register ${projects.general.location.ABSOLUTE}`)
        await dock.run(`register ${projects.alternative.location.ABSOLUTE}`)
      })
      afterAll($configSource.Class.wipe)

      describe('Columns', () => {
        describe('Explicit include', () => {
          describe('Active', () => {
            beforeAll(async () => {
              recorder.clearRecording()

              await dock.run('projects --active-only')
            })

            it('should list registered projects with active column only', async () => {
              expect(recorder.recording).toMatchSnapshot()
            })
          })

          describe('Location', () => {
            beforeAll(async () => {
              recorder.clearRecording()

              await dock.run('projects --location-only')
            })

            it('should list registered projects with location column only', async () => {
              expect(recorder.recording).toMatchSnapshot()
            })
          })
        })

        describe('Include', () => {
          describe('Updated', () => {
            beforeAll(async () => {
              recorder.clearRecording()

              await dock.run('projects --include-updated')
            })

            it('should list registered projects with updated column included', async () => {
              expect(recorder.recording).toMatchSnapshot()
            })
          })

          describe('Version', () => {
            beforeAll(async () => {
              recorder.clearRecording()

              await dock.run('projects --include-version')
            })

            it('should list registered projects with version column included', async () => {
              expect(recorder.recording).toMatchSnapshot()
            })
          })
        })

        describe('Combination', () => {
          beforeAll(async () => {
            recorder.clearRecording()

            const options = [
              '--active-only',
              '--location-only',
              '--include-updated',
              '--include-version',
            ]

            await dock.run(`projects ${options.join(' ')}`)
          })

          it('should list registered projects with columns based on provided column options',async () => {
            expect(recorder.recording).toMatchSnapshot()
          })
        })
      })
    })
  })

  describe('Completion', () => {
    const flags = columnOptions.map(({ flag }) => flag)

    beforeEach(() => recorder.clearRecording())

    it('should complete with system completion if ran with no options', async () => {
      await dock.complete('projects ')

      expect(recorder.recording).toMatchSnapshot()
    })

    describe(
      'Options are provided before command arguments',
      caseFactory.completion.options.createProvidedBeforeArguments(
        'projects',
        () => ({ dock, recorder }),
        ['./path', './another-path'],
      ),
    )

    describe(
      'Default options',
      caseFactory.completion.options.createDefault(
        'projects',
        () => ({ dock, recorder }),
        ['./path', './another-path'],
      ),
    )

    describe(
      'Non default options',
      caseFactory.completion.options.createNonDefault(
        'projects',
        () => ({ dock, recorder }),
        flags as [string, ...Array<string>],
      ),
    )

    describe(
      'All options',
      caseFactory.completion.options.createAll(
        'projects',
        () => ({ dock, recorder }),
        flags as [string, ...Array<string>],
      ),
    )
  })

  describe(
    'Version',
    caseFactory.version.createDefault('projects', () => ({ dock })),
  )

  describe(
    'Help',
    caseFactory.help.createDefault('projects', () => ({ dock })),
  )
})
