import * as commander from 'commander'

import { down } from '~/handlers'
import { createActionHandler, decorateWithDefaults } from '~/commands'
import { requireUse, requireUsedConfigFile, autoReload } from '~/middlewares'

import type { Create } from '@/command'

export const name = 'down'

const nameAndArgs = `${name} [reference...]`
const description = 'Stop and remove container/s or service container/s'
const argumentDescriptions = {
  reference:
    '(Optional) Container or service reference/s. Defaults to all containers',
}

export const create: Create = (provisions) => {
  const handler = createActionHandler(
    down.handle,
    down.complete,
    provisions,
    [requireUse, requireUsedConfigFile, autoReload],
  )
  const command = new commander.Command()
    .command(nameAndArgs)
    .description(description, argumentDescriptions)
    .action(handler)

  return decorateWithDefaults(command, provisions)
}
