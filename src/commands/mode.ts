import * as commander from 'commander'

import { mode } from '~/handlers'
import { createActionHandler, decorateWithDefaults } from '~/commands'
import {
  requireArguments,
  requireUse,
  requireUsedConfigFile,
  autoReload,
  requireModes,
} from '~/middlewares'

import type { Create } from '@/command'

export const name = 'mode'

const nameAndArgs = `${name} [mode...]`
const description
  = 'Switch running containers with provided mode container/s and service container/s'
const argumentDescriptions = { mode: 'Mode name' }

export const create: Create = (provisions) => {
  const handler = createActionHandler(
    mode.handle,
    mode.complete,
    provisions,
    [
      requireArguments(nameAndArgs),
      requireUse,
      requireUsedConfigFile,
      autoReload,
      requireModes,
    ],
  )
  const command = new commander.Command()
    .command(nameAndArgs)
    .description(description, argumentDescriptions)
    .action(handler)

  return decorateWithDefaults(command, provisions)
}
