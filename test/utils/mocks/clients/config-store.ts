import type { ConfigProvider, ConfigSource } from '@/clients/config-store'

export function createConfigProvider(): jest.Mocked<ConfigProvider> {
  return {
    get: jest.fn(),
    set: jest.fn(),
    getAll: jest.fn(),
    setAll: jest.fn(),
    clear: jest.fn(),
  }
}

export function createConfigSource(): jest.Mocked<ConfigSource> {
  return {
    getProvider: jest.fn(),
  }
}
