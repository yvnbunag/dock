type Predicate = (...args: Array<any>)=> boolean // eslint-disable-line @typescript-eslint/no-explicit-any

export function not<WrappedPredicate extends Predicate>(
  predicate: WrappedPredicate,
): Predicate {
  return (...args: Array<unknown>) => !predicate(...args)
}

export function equalTo(value: unknown): (comparator: unknown)=> boolean {
  return (comparator)=> comparator === value
}

export function hasEntryIn<Type>(list: Array<Type>): (entry: Type)=> boolean {
  return (entry) => list.includes(entry)
}

export function hasRecordIn<
  Key extends PrimitiveKey,
  Value extends Primitive | Method,
  SourceObject extends Record<Key, Value>,
>(object: SourceObject): (key: Key)=> boolean {
  return (key) => key in object
}

export function has<
  Value,
  Comparator extends { indexOf: (value: Value, fromIndex?: number)=> number },
>(value: Value): (comparator: Comparator)=> boolean {
  return (comparator)=> comparator.indexOf(value) > -1
}
