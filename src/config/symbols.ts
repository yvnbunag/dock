export const CHECK_MARK = '✓'

export const SINGLE_QUOTE = `'`

export const DOUBLE_QUOTE = '"'

export const QUOTES = [
  SINGLE_QUOTE,
  DOUBLE_QUOTE,
]
