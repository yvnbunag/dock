import path from 'path'

import { isObject } from 'lodash'

import { ValidationError, ValidationErrorSet } from '~/errors/validation-error'
import { toSentence } from '~/lib/print-formatter/list'

import type { ErrorObject, ValidateFunction } from 'ajv'

import type { Project } from '@/config'

export type Validate = (config: Project.RawConfig)=> ValidationErrorSet | null

type PickedErrorObject = Pick<
  ErrorObject,
  'instancePath' | 'message' | 'keyword' | 'params'
>

interface ConfigValidationObject {
  messageIdentifiers?: ValidationError['identifiers'],
}

type ConfigValidationErrorObject = PickedErrorObject & ConfigValidationObject

type ConfigValidationErrorObjectsList = Array<ConfigValidationErrorObject>

enum Keyword {
  ENUM = 'enum',
  ELEMENTS = 'elements',
  PROPERTIES = 'properties',
}

export const standaloneCodeLocation = path.join(
  __dirname,
  './standalone-code.js',
)
export const defaultOptions = {
  allErrors: true,
}
export const rootPath = '~/validation/config'

const validationPaths = {
  CONTAINER: normalizeInstancePath('/containers'),
  SERVICES: normalizeInstancePath('/services'),
}

export function rootValidate(
  config: Project.RawConfig,
  schemaValidate: ValidateFunction,
): ValidationErrorSet | null {
  const errorMessages: Array<ConfigValidationErrorObject> = []

  if (!schemaValidate(config)) {
    errorMessages.push(...(schemaValidate.errors || []))
  }

  if (config) {
    errorMessages.push(...dynamicValidate(config))
  }

  if (!errorMessages.length) {
    return null
  }

  const validationErrors = errorMessages.map(transformToValidationError)

  return new ValidationErrorSet(validationErrors)
}

export function dynamicValidate(
  config: Project.RawConfig,
): ConfigValidationErrorObjectsList {
  const [
    containers,
    services,
    modes,
  ] = [config.containers, config.services, config.modes].map((property) => {
    return (!property || typeof property !== 'object')
      ? []
      : Object.keys(property)
  })

  return [
    ...validateServices(services, containers, config),
    ...validateModes(modes, containers, services, config),
  ]
}

export function validateServices(
  services: Array<Project.ServiceName>,
  containers: Array<Project.ContainerName>,
  config: Project.RawConfig,
): ConfigValidationErrorObjectsList {
  return services.flatMap((name) => validateService(name, containers, config))
}

export function validateService(
  name: Project.ServiceName,
  containers: Array<Project.ContainerName>,
  config: Project.RawConfig,
): ConfigValidationErrorObjectsList {
  const service = config.services?.[name]

  if (
    isObject(service)
    && service.container
    && !containers.includes(service.container)
  ) {
    const { CONTAINER } = validationPaths

    return [{
      instancePath: `/services/${name}/container`,
      keyword: Keyword.PROPERTIES,
      params: {},
      message: `${service.container} is not defined in ${CONTAINER}`,
      messageIdentifiers: [CONTAINER],
    }]
  }

  return []
}

export function validateModes(
  modes: Array<Project.ModeName>,
  containers: Array<Project.ContainerName>,
  services: Array<Project.ServiceName>,
  config: Project.RawConfig,
): ConfigValidationErrorObjectsList {
  return modes
    .flatMap((name) => validateMode(name, containers, services, config))
}

export function validateMode(
  name: Project.ModeName,
  containers: Array<Project.ContainerName>,
  services: Array<Project.ServiceName>,
  config: Project.RawConfig,
): ConfigValidationErrorObjectsList {
  const mode = config.modes?.[name]

  if (!isObject(mode)) return []

  return [
    ...validateModeContainers(name, mode, containers),
    ...validateModeServices(name, mode, services),
    ...validateModeContainersAndServices(name, mode),
  ]
}

export function validateModeContainers(
  name: Project.ModeName,
  mode: Project.Mode,
  containers: Array<Project.ContainerName>,
): ConfigValidationErrorObjectsList {
  if (!mode.containers) return []

  const errors: ConfigValidationErrorObjectsList = []
  const { CONTAINER } = validationPaths

  mode.containers.forEach((container, index) => {
    if (!containers.includes(container)) {
      errors.push({
        instancePath: `/modes/${name}/containers/${index}`,
        keyword: Keyword.ELEMENTS,
        params: {},
        message: `${container} is not defined in ${CONTAINER}`,
        messageIdentifiers: [CONTAINER],
      })
    }
  })

  return errors
}

export function validateModeServices(
  name: Project.ModeName,
  mode: Project.Mode,
  services: Array<Project.ServiceName>,
): ConfigValidationErrorObjectsList {
  if (!mode.services) return []

  const errors: ConfigValidationErrorObjectsList = []
  const { SERVICES } = validationPaths

  mode.services.forEach((service, index) => {
    if (!services.includes(service)) {
      errors.push({
        instancePath: `/modes/${name}/services/${index}`,
        keyword: Keyword.ELEMENTS,
        params: {},
        message: `${service} is not defined in ${SERVICES}`,
        messageIdentifiers: [SERVICES],
      })
    }
  })

  return errors
}

export function validateModeContainersAndServices(
  name: Project.ModeName,
  mode: Project.Mode,
): ConfigValidationErrorObjectsList {
  const { containers, services } = mode
  const modeHasContainers = containers && containers.length
  const modeHasServices = services && services.length

  if (modeHasContainers || modeHasServices) return []

  return [{
    instancePath: `/modes/${name}`,
    keyword: Keyword.PROPERTIES,
    params: {},
    message:
      `One of mode containers or services should be defined with an element`,
  }]
}

export function transformToValidationError(
  {
    instancePath,
    message = 'is invalid',
    keyword,
    params,
    messageIdentifiers = [],
  }: ConfigValidationErrorObject,
): ValidationError {
  const identifier = normalizeInstancePath(instancePath)
  const validationMessage = [
    `${identifier} ${message}`,
    (keyword === Keyword.ENUM ? `: ${toSentence(params.allowedValues)}`: ''),
  ].join('')
  const identifiers = [identifier, ...messageIdentifiers]

  return new ValidationError(validationMessage, identifiers)
}

export function normalizeInstancePath(path: string): string {
  return `config${path}`
}
