import { exec } from 'child_process'

import rimraf from 'rimraf'
import createWalker from 'walker'
import createPrettyFileTree from 'pretty-file-tree'

jest.setTimeout(30000)

describe('Binaries', () => {
  function execute(command: string): Promise<string | Error> {
    return new Promise((resolve, reject) => {
      exec(command, (error, stdout, stderr) => {
        if (error) return reject(error)
        if (stderr) return reject(stderr)
        resolve(stdout)
      })
    })
  }

  function removeFile(file: string): AsyncSideEffect {
    return new Promise((resolve, reject) => {
      try { rimraf(file, () => resolve()) }
      catch (error) { reject(error) }
    })
  }

  describe('dock', () => {
    describe('Build test required packages', () => {
      it.each([
        ['cp', 'which cp'],
        ['sed', 'which sed'],
        ['chmod', 'which chmod'],
        ['webpack', 'node_modules/.bin/webpack --version'],
      ])('%s should be installed', async (packageName, checkCommand) => {
        await expect(execute(checkCommand)).resolves.not.toThrow()
      })
    })

    describe('Build test', () => {
      const webpackConfig = require('+/webpack.config.js') // eslint-disable-line @typescript-eslint/no-var-requires
      const rootDirectory = process.cwd()
      const currentDirectory = __dirname
      const testBuildDirectory = `${currentDirectory}/dist.test`
      const config = {
        path: {
          ACTUAL: `${rootDirectory}/webpack.config.js`,
          TEST: `${currentDirectory}/webpack.build.test.config.js`,
        },
      }
      const replacement = {
        path: {
          ACTUAL: `path: build.path,`,
          TEST: `path: '${testBuildDirectory}',`,
        },
      }

      // Replace test build output directory
      function replaceTestBuildOutputDirectory() {
        if (process.platform === 'darwin') {
          return execute(`sed -i '' -e "s@${replacement.path.ACTUAL}@${replacement.path.TEST}@g" ${config.path.TEST}`)
        }

        return execute(`sed -i "s@${replacement.path.ACTUAL}@${replacement.path.TEST}@g" ${config.path.TEST}`)
      }

      // Correct actual build location references in test build config
      function correctBuildReferences() {
        if (process.platform === 'darwin') {
          return execute(`sed -i '' -e "s@__dirname@'${rootDirectory}'@g" ${config.path.TEST}`)
        }

        return execute(`sed -i "s@__dirname@'${rootDirectory}'@g" ${config.path.TEST}`)
      }

      beforeAll(async () => {
      // Create a test build config from actual config
        await execute(
          `cp ${config.path.ACTUAL} ${config.path.TEST}`,
        )

        await replaceTestBuildOutputDirectory()
        await correctBuildReferences()
      })

      afterAll(async () => {
        await Promise.all([
          removeFile(config.path.TEST),
          removeFile(testBuildDirectory),
        ])
      })

      it('should compile successfully', async () => {
        const command =
        `node_modules/.bin/webpack --config ${config.path.TEST}`

        await expect(execute(command)).resolves.not.toThrow()
      })

      it('should compile to expected structure', async () => {
        const structure = await new Promise<string>((resolve, reject) => {
          const tree: Array<string> = []

          createWalker(testBuildDirectory)
            .on('entry', (entry) => tree.push(entry))
            .on('error', (error) => reject(error))
            .on('end', () => resolve(createPrettyFileTree([...tree].sort())))
        })
        const determinantStructure = structure.replace(
          testBuildDirectory,
          testBuildDirectory.split('/').pop() as string,
        )

        expect(determinantStructure).toMatchSnapshot()
      })

      test('compiled binary should be invoke-able with no errors', async () => {
        const script = `${testBuildDirectory}/${webpackConfig.output.filename}`
        const command = `node ${script} --help`

        await expect(execute(command)).resolves.not.toThrow()
      })
    })
  })
})
