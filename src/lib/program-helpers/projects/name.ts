import { exitCodes } from '~/config'
import { toBulletedSentence } from '~/lib/print-formatter/list'

import type { Name } from '@/config/project'
import type { Provisions } from '@/command'

export function handleNotRegistered(
  name: Name,
  names: Array<Name>,
  provisions: Provisions,
): SideEffect<never> {
  const registeredProjectsMessage = names.length
    ? `Registered projects are:\n${toBulletedSentence(names)}`
    : 'No projects registered'

  return provisions.emitter
    .emitError(`Project '${name}' not currently registered`)
    .emitError(registeredProjectsMessage)
    .exit(exitCodes.NOT_FOUND)
}

export function handleAlreadyRegistered(
  name: Name,
  provisions: Provisions,
): SideEffect<never> {
  return provisions.emitter
    .emitError(`Project '${name}' already registered`)
    .emitError(`Run 'dock projects' to list registered projects`)
    .exit(exitCodes.CONFLICT)
}
