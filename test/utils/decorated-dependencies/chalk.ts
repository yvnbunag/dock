import type * as chalk from 'chalk'

type Chalk = typeof chalk
interface ChalkTarget {
  (): undefined,
  chain: string,
}

const ROOT_CHAIN = 'chalk'

export function create(chain = ROOT_CHAIN): Chalk {
  const target: ChalkTarget = Object.assign(
    /* istanbul ignore next - Proxy prevents invocation*/
    () => undefined,
    { chain },
  )

  return new Proxy(
    target,
    {
      get({ chain }, prop: string) {
        const extension = chain === ROOT_CHAIN && prop === 'default'
          ? ''
          : `-${prop}`

        return create(`${chain}${extension}`)
      },
      apply({ chain }, thisArg, argumentsList) {
        return `<${chain}>${argumentsList.map(String).join(' ')}</${chain}>`
      },
    },
  ) as unknown as Chalk
}
