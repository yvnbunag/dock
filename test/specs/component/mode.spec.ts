import { createTestInstances, caseFactory } from '#/specs/component/__lib__'
import { projects } from '#/specs/component/__lib__/test'
import { Engine } from '@/config/project'

import type { Project } from '#/specs/component/__lib__/test/projects'
import type { Dock } from '#/utils/dock'
import type { Recorder } from '#/utils/recorder'

describe('Component - mode [mode...]', () => {
  describe('Engine', () => {
    const projectsMap: Record<
      Engine,
      Parameters<typeof createEngineSuite>[1]
    > = {
      [Engine.DOCKER_COMPOSE]: {
        WITH_MODES: projects['mode/engine/docker-compose/with-modes'],
        NO_MODE: projects['mode/engine/docker-compose/no-mode'],
      },
    }

    describe(
      ...createEngineSuite(
        Engine.DOCKER_COMPOSE,
        projectsMap[Engine.DOCKER_COMPOSE],
      ),
    )
  })

  describe('Completion', () => {
    describe('Project is used', () => {
      describe('Project has modes', () => {
        let dock: Dock
        let recorder: Recorder

        beforeAll(async () => {
          const instances = await createTestInstances(
            'completion-with-modes',
            projects['mode/with-modes'],
          )

          dock = instances.dock
          recorder = instances.recorder
        })
        beforeEach(() => { recorder.clearRecording() })

        it('should complete if ran with no arguments', async () => {
          await dock.complete('mode ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete if ran with partial mode', async () => {
          await dock.complete('mode mode-with-')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should filtered complete if ran with complete mode', async () => {
          await dock.complete('mode mode-with-container ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should filtered complete if ran with multiple modes', async () => {
          await dock.complete(
            'mode mode-with-container mode-with-service ',
          )

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete if ran with undefined mode', async () => {
          await dock.complete('mode undefined-1 ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete with system completion if ran with exhausted modes', async () => {
          const modes = [
            'mode-with-container',
            'mode-with-service',
            'mode-with-container-and-service',
            'mode-with-intersecting-container-and-service',
            'mode-with-multiple-containers-and-services',
            'mode-with-all-containers-and-services',
          ]

          await dock.complete(`mode ${modes.join(' ')} `)

          expect(recorder.recording).toMatchSnapshot()
        })

        describe(
          'Options are provided before command arguments',
          caseFactory.completion.options.createProvidedBeforeArguments(
            'mode',
            () => ({ dock, recorder }),
            ['mode-with-container', 'mode-with-service'],
          ),
        )

        describe(
          'Default options',
          caseFactory.completion.options.createDefault(
            'mode',
            () => ({ dock, recorder }),
            ['mode-with-container', 'mode-with-service'],
          ),
        )
      })

      describe('Project has no modes', () => {
        let dock: Dock
        let recorder: Recorder

        beforeAll(async () => {
          const instances = await createTestInstances(
            'completion-no-mode',
            projects['mode/no-mode'],
          )

          dock = instances.dock
          recorder = instances.recorder
        })
        beforeEach(() => { recorder.clearRecording() })

        it('should complete with system completion if ran with no arguments', async () => {
          await dock.complete('mode ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete with system completion if ran with arguments', async () => {
          await dock.complete('mode ./path ')

          expect(recorder.recording).toMatchSnapshot()
        })

        describe(
          'Options are provided before command arguments',
          caseFactory.completion.options.createProvidedBeforeArguments(
            'mode',
            () => ({ dock, recorder }),
            ['./path', './another-path'],
          ),
        )

        describe(
          'Default options',
          caseFactory.completion.options.createDefault(
            'mode',
            () => ({ dock, recorder }),
            ['./path', './another-path'],
          ),
        )
      })
    })

    describe('Project is not used', () => {
      const namespace = 'completion-no-project-used'
      const { dock, recorder } = createTestInstances(namespace)

      beforeEach(() => { recorder.clearRecording() })

      it('should complete with system completion if ran with no arguments', async () => {
        await dock.complete('mode ')

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should complete with system completion if ran with arguments', async () => {
        await dock.complete('mode ./path ')

        expect(recorder.recording).toMatchSnapshot()
      })

      describe(
        'Options are provided before command arguments',
        caseFactory.completion.options.createProvidedBeforeArguments(
          'mode',
          () => ({ dock, recorder }),
          ['./path', './another-path'],
        ),
      )

      describe(
        'Default options',
        caseFactory.completion.options.createDefault(
          'mode',
          () => ({ dock, recorder }),
          ['./path', './another-path'],
        ),
      )
    })
  })

  describe(
    'Auto reload',
    caseFactory.autoReload.createIntercepted(
      {
        GENERAL: { project: projects['mode/with-modes'] },
        VALID_CHANGE: { project: projects['mode/updated-valid'] },
        INVALID_CHANGE: { project: projects['mode/updated-invalid'] },
        REMOVED: { project: projects['mode/removed'] },
      },
      {
        runValid: ({ dock }) => dock.run('mode mode-with-container'),
        runInvalid: ({ dock }) => dock.run('mode undefined'),
      },
    ),
  )

  describe(
    'Version',
    caseFactory.version.createDefault('mode', createTestInstances),
  )

  describe(
    'Help',
    caseFactory.help.createDefault('mode', createTestInstances),
  )
})

function createEngineSuite(
  engine: Engine,
  projectsMap: {
    WITH_MODES: Project,
    NO_MODE: Project,
  },
): [Engine, Method] {
  function createNameSpace(name: string) { return `${engine}-${name}`}

  function suite() {
    describe('Command', () => {
      describe('Project is used', () => {
        describe('Project has modes', () => {
          let dock: Dock
          let recorder: Recorder

          beforeAll(async () => {
            const namespace = createNameSpace('with-modes')
            const instances = await createTestInstances(
              namespace,
              projectsMap.WITH_MODES,
            )

            dock = instances.dock
            recorder = instances.recorder
          })
          beforeEach(() => recorder.clearRecording())

          it('should emit error if ran with no arguments', async () => {
            await dock.run('mode')

            expect(recorder.recording).toMatchSnapshot()
          })

          describe('It should switch containers if ran with', () => {
            const modeCases = [
              [
                'container',
                'mode-with-container',
              ],
              [
                'service',
                'mode-with-service',
              ],
              [
                'container and service',
                'mode-with-container-and-service',
              ],
              [
                'intersecting container and service',
                'mode-with-intersecting-container-and-service',
              ],
              [
                'multiple containers and services',
                'mode-with-multiple-containers-and-services',
              ],
              [
                'all containers and services',
                'mode-with-all-containers-and-services',
              ],
            ]

            describe('One mode with', () => {
              test.each(modeCases)('%s', async (description, mode) => {
                await dock.run(`mode ${mode}`)

                expect(recorder.recording).toMatchSnapshot()
              })
            })

            test('multiple modes', async () => {
              await dock.run('mode mode-with-container mode-with-service')

              expect(recorder.recording).toMatchSnapshot()
            })

            test('multiple intersecting modes', async () => {
              const modes = [
                'mode-with-container',
                'mode-with-service',
                'mode-with-multiple-containers-and-services',
              ].join(' ')

              await dock.run(`mode ${modes}`)

              expect(recorder.recording).toMatchSnapshot()
            })

            test('all modes', async () => {
              const modes = modeCases
                .map(([,mode]) => mode)
                .join(' ')

              await dock.run(`mode ${modes}`)

              expect(recorder.recording).toMatchSnapshot()
            })
          })

          it('should emit error if ran with undefined mode', async () => {
            await dock.run('mode undefined-1')

            expect(recorder.recording).toMatchSnapshot()
          })
        })

        describe('Project has no mode', () => {
          let dock: Dock
          let recorder: Recorder

          beforeAll(async () => {
            const namespace = createNameSpace('no-mode')
            const instances = await createTestInstances(
              namespace,
              projectsMap.NO_MODE,
            )

            dock = instances.dock
            recorder = instances.recorder
          })
          beforeEach(() => recorder.clearRecording())

          it('should emit error if ran with no arguments', async () => {
            await dock.run('mode')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should emit error if ran with arguments', async () => {
            await dock.run('mode mode-with-container mode-with-service')

            expect(recorder.recording).toMatchSnapshot()
          })
        })
      })

      describe('Project is not used', () => {
        const namespace = createNameSpace('no-project-used')
        const { dock, recorder } = createTestInstances(namespace)
        const location = projectsMap.WITH_MODES.location.ABSOLUTE

        beforeAll(async () => await dock.run(`register ${location}`))
        beforeEach(() => recorder.clearRecording())

        it('should emit error if ran with no arguments', async () => {
          await dock.run('mode')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should emit error if ran with arguments', async () => {
          await dock.run('mode mode-with-container mode-with-service')

          expect(recorder.recording).toMatchSnapshot()
        })
      })
    })
  }

  return [engine, suite]
}
