import type { CommandArguments, Provisions, Options } from '@/command'

/**
 * Command middleware
 */
export interface Middleware<
  ProvisionKeys extends keyof Provisions = keyof Provisions,
> {
  (
    commandArguments: CommandArguments,
    options: Options<ProvisionKeys>
  ): AsyncSideEffect | never,
}
