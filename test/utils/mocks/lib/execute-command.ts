import { createObservableMethod } from '#/utils/observable'

import type { ChildProcess } from 'child_process'
import type { ExecuteCommand } from '~/lib/execute-command'
import type { Observable, ObserverHandler } from '#/utils/observable'
import type { Recorder } from '#/utils/recorder'

export type ObservableExecuteCommand = Observable<
  ExecuteCommand,
  Parameters<ExecuteCommand>
>

export function create(): ObservableExecuteCommand {
  const executeCommand
    = createObservableMethod<ExecuteCommand, Parameters<ExecuteCommand>>(
      (commands) => {
        executeCommand.emit(commands)

        const child = Symbol('child') as unknown as ChildProcess

        return Promise.resolve([null, child])
      },
    )

  return executeCommand
}

export function createRecorderHandler(
  recorder: Recorder,
): ObserverHandler<Parameters<ExecuteCommand>> {
  return (commands) => recorder.record('executed-command', commands)
}
