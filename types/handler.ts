import type { CommandArguments, Options } from '@/command'

export interface Handler<
  ProvidedArguments extends CommandArguments = CommandArguments,
> {
  (
    commandArguments: ProvidedArguments,
    options: Options
  ): AsyncSideEffect | never,
}

export type CompletionHandler<
  ProvidedArguments extends CommandArguments = CommandArguments,
> = Handler<ProvidedArguments>
