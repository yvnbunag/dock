export function trim(
  values = [`\s`], // eslint-disable-line no-useless-escape
): (value: string)=> string {
  const joinedValues = values.join('')
  const pattern = new RegExp(`^[${joinedValues}]|[${joinedValues}]$`, 'g')

  return (value) => value.replace(pattern, '')
}

export function extract<
  Key extends PrimitiveKey,
  Value extends Primitive | Method,
  SourceObject extends { [key in Key]: Value },
>(key: Key): (object: SourceObject)=> SourceObject[Key] {
  return (object) => object[key]
}

export function pushStringWithGrouping(
  groupIdentifiers: Array<string>,
): (target: Array<string>, value: string)=> Array<string> {
  return (target, value) => {
    if (!target.length) return [value]

    const lastIndex = target.length - 1
    const lastArg = target[lastIndex]
    const previousIsInGroup = groupIdentifiers.includes(lastArg[0])
    const previousIsClosedGroup = previousIsInGroup
      && lastArg.length > 1
      && lastArg[0] === lastArg[lastArg.length - 1]
    const includeInLastGroup = previousIsInGroup && !previousIsClosedGroup

    if (includeInLastGroup) target[lastIndex] = `${lastArg} ${value}`
    else target.push(value)

    return target
  }
}
