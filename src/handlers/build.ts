import { isEmpty, kebabCase } from 'lodash'

import { install } from '~/handlers'
import { programHelpers } from '~/lib'
import { extractCompletionMetadata } from '~/lib/completion'
import { extract } from '~/lib/transform-factory'

import type { Options, Provisions } from '@/command'
import type { Handler, CompletionHandler } from '@/handler'
import type { References } from '@/config/project'

type Arguments = [References]

enum BuildOption {
  INSTALL = 'install',
}

type BuildOptionProperties = {
  flag: `--${string}`,
  description: string,
}

export const options: Record<BuildOption, BuildOptionProperties> = {
  [BuildOption.INSTALL]: {
    flag: `--${kebabCase(BuildOption.INSTALL)}`,
    description:
      'Run service install scripts for service references. Defaults to all if no reference is provided',
  },
}

export const handle: Handler<Arguments> = async function (
  [references],
  options,
) {
  const { provisions, option } = options
  const undefinedReferences = await programHelpers.projects.references
    .extractUndefined(references, provisions)

  if (undefinedReferences.length) {
    return programHelpers.projects.references
      .handleNotDefined(undefinedReferences, provisions)
  }

  await handleBuild(references, provisions)

  if (BuildOption.INSTALL in option) {
    await handleInstall(references, options)
  }
}

async function handleBuild(
  references: References,
  provisions: Provisions,
): AsyncSideEffect | never {
  const [
    { configPath, rawConfig },
    containers,
  ] = await Promise.all([
    provisions.programMediator.getUsedProjectProvider()
      .then((provider) => provider.getAll()),
    programHelpers.projects.references
      .extractContainers(references, provisions),
  ])

  await provisions.orchestrationEngineFactory
    .create(configPath, rawConfig.engine)
    .build(containers)
}

async function handleInstall(
  references: References,
  options: Options,
): AsyncSideEffect | never {
  if (!references.length) return handleInstallAll(options)

  return handleInstallReferences(references, options)
}

async function handleInstallAll(options: Options): AsyncSideEffect {
  const { provisions } = options
  const installMap = await programHelpers.projects.services
    .getInstallMap(provisions)

  if (isEmpty(installMap)) {
    const skipWarning
      = 'Install skipped as there are no services with install scripts'

    provisions.emitter.emitWarning(skipWarning)

    return
  }

  return install.handle([[]], options)
}

async function handleInstallReferences(
  references: References,
  options: Options,
): AsyncSideEffect {
  const { provisions } = options
  const services = await programHelpers.projects.references
    .extractServices(references, provisions)

  if (!services.length) {
    const skipWarning
      = 'Install skipped as there is no service in the provided references'

    provisions.emitter.emitWarning(skipWarning)

    return
  }

  return install.handle([services], options)
}

export const complete: CompletionHandler<Arguments> = async function (
  commandArguments,
  { provisions },
) {
  const metadata = extractCompletionMetadata(provisions)

  if (metadata.isOptionCompletion()) {
    return programHelpers.completion.completeOptions(
      metadata,
      provisions,
      Object.values(options).map(extract('flag')),
    )
  }

  const references = await programHelpers.projects.references
    .getKeys(provisions)

  return programHelpers.completion
    .variadicComplete(metadata, provisions, references)
}
