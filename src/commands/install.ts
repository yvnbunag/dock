import * as commander from 'commander'

import { install } from '~/handlers'
import { createActionHandler, decorateWithDefaults } from '~/commands'
import {
  requireUse,
  requireUsedConfigFile,
  autoReload,
  requireServices,
  requireServiceInstalls,
} from '~/middlewares'
import { logDuration } from '~/lib/decorators'

import type { Create } from '@/command'

export const name = 'install'

const nameAndArgs = `${name} [service...]`
const description = 'Run install script/s for services'
const argumentDescriptions = {
  service: '(Optional) Service reference/s. Defaults to all services',
}

export const create: Create = (provisions) => {
  const handler = createActionHandler(
    logDuration(install.handle, provisions.emitter, 'Installation'),
    install.complete,
    provisions,
    [
      requireUse,
      requireUsedConfigFile,
      autoReload,
      requireServices,
      requireServiceInstalls,
    ],
  )
  const command = new commander.Command()
    .command(nameAndArgs)
    .description(description, argumentDescriptions)
    .action(handler)

  return decorateWithDefaults(command, provisions)
}
