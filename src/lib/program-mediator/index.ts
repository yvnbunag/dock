import { defaultsDeep, omit, pick } from 'lodash'

import { fallback } from '~/config'
import { version } from '~/config/config'
import { NoUsedProjectError } from '~/lib/program-mediator/errors'
import { not, equalTo } from '~/lib/predicate-factory'

import type { Project, Program } from '@/config'
import type { ConfigProvider, ConfigSource } from '@/clients/config-store'

export class ProgramMediator {
  constructor(private readonly configSource: ConfigSource) {}

  async loadProject(
    rawConfig: Project.RawConfig,
    configPath: Project.ConfigPath,
    updated: Project.Updated,
  ): AsyncSideEffect<Project.Config> {
    const { name } = rawConfig
    const projectConfig
      = this.buildProjectConfig(rawConfig, configPath, updated)
    const metadata = { version, configPath, updated }
    const [storedProjectConfig] = await Promise.all([
      this.getProjectProvider(name)
        .then((provider) => provider.setAll(projectConfig)),
      this.addProjectMetadata(name, metadata),
    ])

    return storedProjectConfig
  }

  async activateProject(name: Project.Name): AsyncSideEffect {
    const provider = await this.getProgramProvider()
    const settings = await provider.get('settings')
    const updatedSettings = {
      ...settings,
      active: name,
    }

    await provider.set('settings', updatedSettings)
  }

  async reloadProject(
    name: Project.Name,
    reloadConfig: Project.RawConfig,
    updated: Project.Updated,
  ): AsyncSideEffect<Project.Config> {
    const latestName = reloadConfig.name

    if (latestName !== name) await this.renameProject(name, latestName)

    const provider = await this.getProjectProvider(latestName)
    const configPath = await provider.get('configPath')
    const projectConfig
      = this.buildProjectConfig(reloadConfig, configPath, updated)

    const [storedProjectConfig] = await Promise.all([
      provider.setAll(projectConfig),
      this.updateProjectMetadata(latestName, { version, updated }),
    ])

    return storedProjectConfig
  }

  async renameProject(
    oldName: Project.Name,
    newName: Project.Name,
  ): AsyncSideEffect<Project.Config> {
    const [settings, projectConfig] = await Promise.all([
      this.getSettings(),
      this.getProjectProvider(oldName).then((provider) => provider.getAll()),
    ])
    const projectIsActive = settings.active === oldName
    const metadata = pick(projectConfig, ['version', 'configPath', 'updated'])
    const [storedProjectConfig] = await Promise.all([
      this.getProjectProvider(newName)
        .then((provider) => provider.setAll(projectConfig)),
      this.getProjectProvider(oldName)
        .then((provider) => provider.clear()),
      this.removeProjectMetadata(oldName)
        .then(() => this.addProjectMetadata(newName, metadata))
        .then(() => { projectIsActive && this.activateProject(newName) }),
    ])

    return storedProjectConfig
  }

  async unloadProject(name: Project.Name): AsyncSideEffect {
    await Promise.all([
      this.getProjectProvider(name).then((provider) => provider.clear()),
      this.removeProjectMetadata(name),
    ])
  }

  async getProjects(): Promise<Readonly<Program.Config['projects']>> {
    return this.getProgramProvider()
      .then((provider) => provider.get('projects'))
      .then((projects) => defaultsDeep(
        projects,
        fallback.config.PROGRAM.projects),
      )
  }

  async getSettings(): Promise<Readonly<Program.Config['settings']>> {
    return this.getProgramProvider()
      .then((provider) => provider.get('settings'))
      .then((projects) => defaultsDeep(
        projects,
        fallback.config.PROGRAM.settings),
      )
  }

  async getConfig(): Promise<Readonly<Program.Config>> {
    return this.getProgramProvider()
      .then((provider) => provider.getAll())
      .then((config) => defaultsDeep(config, fallback.config.PROGRAM))
  }

  async getProjectProvider(
    name: Project.Name,
  ): Promise<ConfigProvider<Project.Config>> {
    return this.configSource.getProvider<Project.Config>(
      `projects/${name}`,
      fallback.config.PROJECT,
    )
  }

  async getUsedProjectProvider():
    Promise<ConfigProvider<Project.Config>> | never
  {
    const { active } = await this.getSettings()

    if (!active) throw new NoUsedProjectError()

    return this.getProjectProvider(active)
  }

  async getProgramProvider(): Promise<ConfigProvider<Program.Config>> {
    return this.configSource.getProvider<Program.Config>(
      'program',
      fallback.config.PROGRAM,
    )
  }

  private buildProjectConfig(
    rawConfig: Project.RawConfig,
    configPath: Project.ConfigPath,
    updated: Project.Updated,
  ): Project.Config {
    const filledRawConfig: Required<Project.RawConfig> = defaultsDeep(
      rawConfig,
      fallback.rawConfig,
    )
    const containerValues = Object.keys(filledRawConfig.containers)
    const serviceValues = Object.keys(filledRawConfig.services)
    const modeValues = Object.keys(filledRawConfig.modes)
    const referenceValues = [...new Set([...containerValues, ...serviceValues])]
    const projectConfig: Project.Config = {
      version,
      configPath,
      updated,
      rawConfig,
      aggregates: {
        containers: { values: containerValues },
        services: { values: serviceValues },
        modes: { values: modeValues },
        references: { values: referenceValues },
      },
    }

    return projectConfig
  }

  private async addProjectMetadata(
    name: Project.Name,
    metadata: Program.ProjectMetadata,
  ): AsyncSideEffect {
    const projects = await this.getProjects()
    const updatedProjects = {
      names: [...new Set([...projects.names, name])],
      configPaths: [...new Set([...projects.configPaths, metadata.configPath])],
      metadata: {
        ...projects.metadata,
        [name]: metadata,
      },
    }

    await this.getProgramProvider()
      .then((provider) => provider.set('projects', updatedProjects))
  }

  private async updateProjectMetadata(
    name: Project.Name,
    metadata: Pick<Program.ProjectMetadata, 'version' | 'updated'>,
  ): AsyncSideEffect {
    const projects = await this.getProjects()
    const updatedMetadata = {
      ...projects.metadata,
      [name]: {
        ...projects.metadata[name],
        ...metadata,
      },
    }
    const updatedProjects = {
      ...projects,
      metadata: updatedMetadata,
    }

    await this.getProgramProvider()
      .then((provider) => provider.set('projects', updatedProjects))
  }

  private async removeProjectMetadata(name: Project.Name): AsyncSideEffect {
    const { projects, settings } = await this.getConfig()
    const { configPath } = projects.metadata[name]
    const updatedProjects = {
      names: projects.names.filter(not(equalTo(name))),
      configPaths: projects.configPaths.filter(not(equalTo(configPath))),
      metadata: omit(projects.metadata, [name]),
    }
    const updatedSettings = {
      ...settings,
      ...(name === settings.active ? { active: null } : {}),
    }
    const programConfig = {
      projects: updatedProjects,
      settings: updatedSettings,
    }

    await this.getProgramProvider()
      .then((provider) => provider.setAll(programConfig))
  }
}
