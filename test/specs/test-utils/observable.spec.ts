import { createObservableMethod } from '#/utils/observable'
import { primitive } from '#/utils/values'

import type { Observable } from '#/utils/observable'

describe('createObservableMethod', () => {
  const observableMethod: Observable<Method, Array<unknown>>
    = createObservableMethod((...args) => observableMethod.emit(...args))

  it('should observe if subscribed', () => {
    const observer = jest.fn()

    observableMethod('before-subscribe')
    observableMethod.subscribe(observer)
    observableMethod('after-subscribe')
    observableMethod()
    observableMethod(...Object.values(primitive))

    expect(observer.mock.calls).toMatchSnapshot()

    observableMethod.unsubscribe(observer)
  })

  it('should stop observing if unsubscribed', () => {
    const observer = jest.fn()

    observableMethod('before-subscribe')
    observableMethod.subscribe(observer)
    observableMethod('after-subscribe')
    observableMethod('before-unsubscribe')
    observableMethod.unsubscribe(observer)
    observableMethod('after-unsubscribe')

    expect(observer.mock.calls).toMatchSnapshot()
  })
})
