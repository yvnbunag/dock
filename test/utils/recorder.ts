type RecordType = string
type RecordEntry = unknown
type Record = Array<RecordEntry>
type Recording = [RecordType, Record]
type Transformer = (record: RecordEntry)=> RecordEntry

export class Recorder {
  private _recording: Array<Recording> = []
  private readonly _transformers: Array<Transformer> = []

  constructor() {
    this.record = this.record.bind(this)
  }

  get recording(): string {
    if (!this._recording.length) return ''

    return this.transformedRecording.reduce(
      (
        stringRecording,
        [key, rawRecord = []],
        index,
        originalRecording,
      ) => {
        const record = rawRecord.length ? rawRecord : ['<empty-record>']
        const end = index < originalRecording.length - 1 ? '\n' : ''

        stringRecording += `[${key}]\n`
        stringRecording += `${record.map(String).join('\n')}${end}`

        return stringRecording
      },
      '',
    )
  }

  private get transformedRecording(): Array<Recording> {
    return this._recording.map(([type, record]) => {
      const transformedRecord = this._transformers.reduce(
        (transformedRecord, transform) => transformedRecord.map(transform),
        record,
      )

      return [type, transformedRecord]
    })
  }

  record(key: RecordType, value: Record = []): SideEffect {
    this._recording.push([key, value])
  }

  clearRecording(): SideEffect<string> {
    const previousRecording = this.recording

    this._recording = []

    return previousRecording
  }

  addTransformer(transformer: Transformer): Recorder {
    this._transformers.push(transformer)

    return this
  }

  removeTransformer(transformer: Transformer): Recorder {
    const index = this._transformers.indexOf(transformer)

    if (index > -1) this._transformers.splice(index, 1)

    return this
  }
}
