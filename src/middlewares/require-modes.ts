import { NOT_FOUND } from '~/config/exit-codes'

import type { Middleware } from '@/middleware'

type RequireModes = Middleware<'emitter' | 'programMediator'>

export const requireModes: RequireModes = async function (
  commandArguments,
  { provisions },
) {
  const { values } = await provisions.programMediator
    .getUsedProjectProvider()
    .then((provider) => provider.get('aggregates'))
    .then(({ modes }) => modes)

  if (values.length) return

  return provisions.emitter
    .emitError('There are no modes defined in the project config')
    .exit(NOT_FOUND)
}
