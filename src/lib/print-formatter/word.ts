export function toString(value: Primitive): string {
  if (typeof value === 'string') return value
  if (typeof value === 'object') return JSON.stringify(value)

  return String(value)
}

export function singleQuote(word: Primitive): string {
  return `'${toString(word)}'`
}
