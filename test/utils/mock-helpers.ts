/**
 * Any jest.MockInstance extender (jest.Mock, jst.SpyInstance)
 */
export type Mock<T = any, Y extends Array<any> = any> = jest.MockInstance<T, Y> // eslint-disable-line @typescript-eslint/no-explicit-any

/**
 * Get first arguments of a mock function
 */
export function getFirstArguments(mock: Mock): Array<Primitive> {
  return mock.mock.calls.slice(0, 1).shift()
}

/**
 * Get all arguments of a mock function
 */
export function getAllArguments(mock: Mock): Array<Array<Primitive>> {
  return [...mock.mock.calls]
}
