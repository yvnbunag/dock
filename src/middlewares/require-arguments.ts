import { exitCodes } from '~/config'
import { toSentence } from '~/lib/print-formatter/list'

import type { Middleware } from '@/middleware'

export function requireArguments(
  nameAndArgs: string,
  until?: number,
): Middleware<'optionMediator' | 'emitter'> {
  return async (commandArguments, { provisions }) => {
    const nameAndArgsList = nameAndArgs.split(' ')
    const name = nameAndArgsList.slice(0, 1)
    const args = nameAndArgsList
      .slice(1)
      .map((arg) => arg.replace(/[^\w]/g, ''))
    const commandArgumentsCount = commandArguments
      .filter(Boolean) // Remove empty space and undefined entries
      .filter( // Remove empty variadic arguments
        (argument) => !Array.isArray(argument) || argument.length,
      )
      .length
    const requiredArgsCount = Math.max(
      typeof until === 'number' && until < args.length ? until : args.length,
      0,
    )
    const missingArgsCount = requiredArgsCount - commandArgumentsCount

    if (!Math.max(missingArgsCount, 0)) return

    const missingArgs = toSentence(
      args.slice(0, requiredArgsCount).slice(-missingArgsCount),
    )

    return provisions.emitter
      .emitError(`Missing required argument/s ${missingArgs}`)
      .emitError(`Run dock ${name} --help for usage`)
      .exit(exitCodes.INCOMPLETE)
  }
}
