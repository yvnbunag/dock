import { CommandError } from '~/errors'

export class AbortError extends CommandError {
  name = 'AbortError'
}
