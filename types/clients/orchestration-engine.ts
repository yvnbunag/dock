import type {
  ConfigPath,
  ContainerNames,
  ContainerName,
  Engine,
  EntryPoint,
  Shell,
} from '@/config/project'
import type { ExecuteCommand } from '~/lib/execute-command'

export interface ContainerCommandOptions {
  shell?: Shell,
  entryPoint?: EntryPoint,
}

export type ContainerCommand = string

export type ContainerCommands = Array<ContainerCommand>

export interface OrchestrationEngineInterface {
  build(
    containers?: ContainerNames
  ): Promise<OrchestrationEngineInterface> | never,
  start(
    containers?: ContainerNames
  ): Promise<OrchestrationEngineInterface> | never,
  stop(
    containers?: ContainerNames
  ): Promise<OrchestrationEngineInterface> | never,
  remove(
    containers?: ContainerNames
  ): Promise<OrchestrationEngineInterface> | never,
  down(
    containers?: ContainerNames
  ): Promise<OrchestrationEngineInterface> | never,
  run(
    container: ContainerName,
    commands: ContainerCommands,
    options?: ContainerCommandOptions
  ): Promise<OrchestrationEngineInterface> | never,
  open(
    container: ContainerName,
    options?: Pick<ContainerCommandOptions, 'shell' | 'entryPoint'>
  ): Promise<OrchestrationEngineInterface> | never,
}

export interface OrchestrationEngineConstructor {
  new (
    configPath: ConfigPath,
    executeCommand: ExecuteCommand
  ): OrchestrationEngineInterface,
}

export interface OrchestrationEngineFactory {
  create(configPath: ConfigPath, engine?: Engine): OrchestrationEngineInterface,
}
