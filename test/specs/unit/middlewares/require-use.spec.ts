import { requireUse } from '~/middlewares'
import { NoUsedProjectError } from '~/lib/program-mediator/errors'

import { Recorder } from '#/utils/recorder'
import { programMediator as programMediatorFactory } from '#/utils/mocks/lib'
import { emitter as emitterFactory } from '#/utils/mocks/clients'
import { NOOP } from '#/utils/general-helpers'
import { createOptions } from '#/utils/command'

const recorder = new Recorder()
const emitter = emitterFactory
  .create({ exit: <emitterFactory.Overrides['exit']>NOOP })
  .subscribe(emitterFactory.createRecorderHandler(recorder))
const programMediator = programMediatorFactory.create()
const provisions = { emitter, programMediator }
const options = createOptions({ provisions })

it('should continue if project is used', async () => {
  await requireUse([], options)

  expect(recorder.recording).toBeFalsy()
})

it('should exit with error if project is not used', async () => {
  const error = new NoUsedProjectError()

  programMediator.getUsedProjectProvider.mockRejectedValueOnce(error)

  await requireUse([], options)

  expect(recorder.recording).toMatchSnapshot()
})

it('should propagate error if non no used project error is thrown', async () => {
  const error = new Error('non-no-used-project-error')

  programMediator.getUsedProjectProvider.mockRejectedValueOnce(error)

  await expect(requireUse([], options)).rejects.toBe(error)
})
