import { load } from 'js-yaml'

import { fallback } from '~/config'
import { validate } from '~/validation/config/runtime'
import { programHelpers } from '~/lib'
import { completeFilename, extractCompletionMetadata } from '~/lib/completion'

import type { Handler, CompletionHandler } from '@/handler'
import type { Project } from '@/config'

export const handle: Handler = async function (
  commandArguments,
  { provisions },
) {
  const {
    configPath,
    rawConfig: currentConfig,
  } = await provisions.programMediator
    .getUsedProjectProvider()
    .then((provider) => provider.getAll())
  const fileName = `${configPath}/${fallback.project.CONFIG.name}`
  const [file, updated] = await Promise.all([
    programHelpers.projects.config.read(fileName),
    programHelpers.projects.config.getUpdateDate(fileName),
  ])
  const reloadConfig = load(file) as Project.RawConfig
  const error = validate(reloadConfig)

  if (error) {
    return programHelpers.projects.config
      .handleValidationError(error, provisions)
  }

  if (reloadConfig.name !== currentConfig.name) {
    const projects = await provisions.programMediator.getProjects()

    if (projects.names.includes(reloadConfig.name)) {
      return programHelpers.projects.name
        .handleAlreadyRegistered(reloadConfig.name, provisions)
    }
  }

  await provisions.programMediator
    .reloadProject(currentConfig.name, reloadConfig, updated)
}

export const complete: CompletionHandler = async function (
  commandArguments,
  { provisions },
) {
  const metadata = extractCompletionMetadata(provisions)

  if (metadata.isOptionCompletion()) {
    return programHelpers.completion.completeOptions(metadata, provisions)
  }

  return completeFilename(provisions.emitter)
}
