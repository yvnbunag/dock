import { createTestInstances, caseFactory } from '#/specs/component/__lib__'
import { projects } from '#/specs/component/__lib__/test'

const { $configSource, dock, recorder } = createTestInstances()

describe('Component - use [project]', () => {
  describe('Command', () => {
    describe('Target project is not provided', () => {
      beforeAll(async () => {
        await dock.run(`use`)
      })

      it('should require project argument', () => {
        expect(recorder.recording).toMatchSnapshot()
      })
    })

    describe('Target project is registered', () => {
      describe('No project is in use', () => {
        beforeAll(async () => {
          await dock.run(`register ${projects.general.location.ABSOLUTE}`)
          await dock.run(`register ${projects.alternative.location.ABSOLUTE}`)

          recorder.clearRecording()

          await dock.run(`use ${projects.general.name}`)
        })
        afterAll($configSource.Class.wipe)

        it('should be activated for use', async () => {
          await dock.run(`projects --active-only`)

          expect(recorder.recording).toMatchSnapshot()
        })
      })

      describe('Target project is in use', () => {
        beforeAll(async () => {
          await dock.run(`register ${projects.general.location.ABSOLUTE}`)
          await dock.run(`register ${projects.alternative.location.ABSOLUTE}`)
          await dock.run(`use ${projects.general.name}`)

          recorder.clearRecording()

          await dock.run(`use ${projects.general.name}`)
        })
        afterAll($configSource.Class.wipe)

        it('should notify that the project is already in use', () => {
          expect(recorder.recording).toMatchSnapshot()
        })

        it('should retain target project in use', async () => {
          recorder.clearRecording()

          await dock.run(`projects --active-only`)

          expect(recorder.recording).toMatchSnapshot()
        })
      })

      describe('Different project is in use', () => {
        beforeAll(async () => {
          await dock.run(`register ${projects.general.location.ABSOLUTE}`)
          await dock.run(`register ${projects.alternative.location.ABSOLUTE}`)
          await dock.run(`use ${projects.alternative.name}`)

          recorder.clearRecording()

          await dock.run(`use ${projects.general.name}`)
        })
        afterAll($configSource.Class.wipe)

        it('should be activated for use', async () => {
          await dock.run(`projects --active-only`)

          expect(recorder.recording).toMatchSnapshot()
        })
      })
    })

    describe('Target project is not registered', () => {
      describe('With no other projects', () => {
        it('should fail activation for use', async () => {
          recorder.clearRecording()

          await dock.run(`use ${projects.general.name}`)

          expect(recorder.recording).toMatchSnapshot()
        })
      })

      describe('With other projects', () => {
        beforeAll(async () => {
          await dock.run(`register ${projects.alternative.location.ABSOLUTE}`)
        })
        afterAll($configSource.Class.wipe)

        it('should fail activation for use', async () => {
          recorder.clearRecording()

          await dock.run(`use ${projects.general.name}`)

          expect(recorder.recording).toMatchSnapshot()
        })
      })
    })

    describe('Side effects', () => {
      beforeAll(async () => {
        await dock.run(`register ${projects.general.location.ABSOLUTE}`)
        await dock.run(`register ${projects.alternative.location.ABSOLUTE}`)
        await dock.run(`use ${projects.general.name}`)
        await dock.run(`unregister ${projects.general.name}`)

        recorder.clearRecording()
      })
      afterAll($configSource.Class.wipe)

      describe('Used project is unregistered', () => {
        it('should be un-used', async () => {
          await dock.run(`projects --active-only`)

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should not be active when re-registered', async () => {
          await dock.run(`register ${projects.general.location.ABSOLUTE}`)

          recorder.clearRecording()

          await dock.run(`projects --active-only`)

          expect(recorder.recording).toMatchSnapshot()
        })
      })
    })
  })

  describe('Completion', () => {
    describe('Projects are registered', () => {
      beforeAll(async () => {
        await dock.run(`register ${projects.general.location.ABSOLUTE}`)
        await dock.run(`register ${projects.alternative.location.ABSOLUTE}`)
      })
      afterAll($configSource.Class.wipe)

      beforeEach(() => recorder.clearRecording())

      it('should complete if ran with no project', async () => {
        await dock.complete('use ')

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should complete if ran with no matching project', async () => {
        await dock.complete('use no-match')

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should complete if ran with partial matching project', async () => {
        const partialProject = projects.general.name.slice(0, -1)

        await dock.complete(`use ${partialProject}`)

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should complete if ran with full matching project', async () => {
        await dock.complete(`use ${projects.general.name}`)

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should complete with system completion for succeeding arguments', async () => {
        const argument = projects.general.name

        await Promise.all([
          dock.complete(`use ${argument} `),
          dock.complete(`use ${argument} ${argument}`),
        ])

        expect(recorder.recording).toMatchSnapshot()
      })

      describe(
        'Options are provided before command arguments',
        caseFactory.completion.options.createProvidedBeforeArguments(
          'use',
          () => ({ dock, recorder }),
          [projects.general.name, './path'],
        ),
      )

      describe(
        'Default options',
        caseFactory.completion.options.createDefault(
          'use',
          () => ({ dock, recorder }),
          [projects.general.name, './path'],
        ),
      )
    })

    describe('No Projects are registered', () => {
      it('should complete with system completion if ran with no registered project', async () => {
        recorder.clearRecording()

        await dock.complete('use ')

        expect(recorder.recording).toMatchSnapshot()
      })

      describe(
        'Options are provided before command arguments',
        caseFactory.completion.options.createProvidedBeforeArguments(
          'use',
          () => ({ dock, recorder }),
          ['./path', './another-path'],
        ),
      )

      describe(
        'Default options',
        caseFactory.completion.options.createDefault(
          'use',
          () => ({ dock, recorder }),
          ['./path', './another-path'],
        ),
      )
    })
  })

  describe(
    'Auto reload',
    caseFactory.autoReload.createPostProcessed(
      {
        GENERAL: { project: projects.general },
        VALID_CHANGE: { project: projects['use/updated-valid'] },
        INVALID_CHANGE: { project: projects['use/updated-invalid'] },
        REMOVED: { project: projects['use/removed'] },
      },
      {
        runValid: ({ dock }, project) => dock.run(`use ${project.name}`),
        runInvalid: ({ dock }) => dock.run('use undefined'),
      },
    ),
  )

  describe(
    'Version',
    caseFactory.version.createDefault('use', () => ({ dock })),
  )

  describe(
    'Help',
    caseFactory.help.createDefault('use', () => ({ dock })),
  )
})
