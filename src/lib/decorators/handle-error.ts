import { DockExit } from '~/errors'

export type ErrorHandler = (error: Error)=> never

export function handleError<Invoke extends Async<Method>>(
  invoke: Invoke,
  handle: ErrorHandler,
): (...args: Parameters<Invoke>)=> Promise<Awaited<ReturnType<Invoke>>> {
  return async (...args) => {
    try {
      return await invoke(...args)
    } catch (error) {
      if (error instanceof DockExit) throw error

      return handle(error)
    }
  }
}
