# @ianbunag/dock Contributing Guide 1.0

<!-- omit in toc -->
## Contents

- [Requirements](#requirements)
- [Optional Requirements](#optional-requirements)
- [Development](#development)
  - [Setup](#setup)
  - [Running dock with on the fly interpretation](#running-dock-with-on-the-fly-interpretation)
  - [Running dock with compiled script](#running-dock-with-compiled-script)
- [Tests](#tests)
  - [Component tests](#component-tests)
  - [Integration tests](#integration-tests)
  - [Unit tests](#unit-tests)
  - [Build tests](#build-tests)
  - [Test util tests](#test-util-tests)
- [Code Structure](#code-structure)
  - [src](#src)
  - [types](#types)
  - [build](#build)
  - [test](#test)
- [Implementation Questions](#implementation-questions)
  - [How do I add an orchestration engine?](#how-do-i-add-an-orchestration-engine)
  - [How do I implement completion for other platforms?](#how-do-i-implement-completion-for-other-platforms)
  - [Why is dock bundled with webpack?](#why-is-dock-bundled-with-webpack)
  - [Why does the execute-command not propagate errors to its consumers?](#why-does-the-execute-command-not-propagate-errors-to-its-consumers)
  - [Where are the program and project states stored?](#where-are-the-program-and-project-states-stored)

<br/>

---

## Requirements

1. [NodeJS](https://nodejs.org/en/) version 22.12 or higher

<br/>

---

## Optional Requirements

The repository is configured with docker, docker-compose and dock, which may
be used to run the tests in the same image as the Continuous Integration
environment

1. [Docker](https://www.docker.com/) (for Linux) version 19.03 or higher
2. [Docker Compose](https://docs.docker.com/compose/) version 1.27 or higher
3. [Yarn](https://classic.yarnpkg.com/lang/en/) version 1.22 or higher
   - Optional as npm or any other package manager should be applicable, just
    that package manager commands in this guide uses yarn
   - Not tested with [Yarn 2](https://yarnpkg.com/)

<br/>

---

## Development

### Setup

1. Install dependencies
```sh
yarn install
```

2. Register git hooks
```sh
yarn prepare-development
```

### Running dock with on the fly interpretation

```sh
yarn dock:dev --help
```

### Running dock with compiled script

#### One time compilation

```sh
yarn build
```

#### Watched compilation

```sh
yarn build:watch
```

#### Making the script available globally

```sh
yarn link
```

#### Running the script with yarn

```sh
yarn dock --help
```

#### Running the globally linked script

```sh
dock --help
```

<br/>

---

## Tests

The tests for this repository are inspired by an article for
[Testing Strategies in a Microservice Architecture](https://martinfowler.com/articles/microservice-testing)

They are divided into three main suites - [component](#component-tests),
[integration](#integration-tests) and [unit](#unit-tests) tests, with an
additional two supporting suites - [build](#build-tests) and
[test util](#test-util-tests) tests

It is encouraged to develop the repository with the following approaches:
- Behavior Driven Development (BDD)
- Test Driven Development (TDD)

Test coverage is not required to be met per suite, but for the combined result
when all tests are ran, which should be above `95%`

To run all tests:
```sh
yarn test

# Or through the containerized environment
dock yarn test
```

To run specific tests:
```sh
# Path to file or directory
yarn test:focused [path]
```

### Component tests

Test for the functionalities of dock, such as commands and background processes.
This is where the BDD approach is practiced, with the following flow:
1. Create a suite of failing command tests
2. Implement, make a test pass
3. Refactor
4. Repeat from 1 or 2, until requirements and sufficient cases and coverage are
met

Only the functionality of dock is being tested  - all external components are
dependency injected a
[test double](https://martinfowler.com/bliki/TestDouble.html). This would also
mean faster tests compared to end-to-end tests in terms of following BDD

To run component tests:
```sh
yarn test:component
```

### Integration tests

Test for components that interact with an external API or with the system, like
saving to a data store or executing a command. These tests are used to cover the
components that were dependency injected with a test double in the
[component tests](#component-tests)

As much as possible, avoid test doubles for these tests, but may be necessary
for cases like when a component involves consumption of the NodeJS process,
with which it would be ideal to isolate invocations within the test context
instead of allowing to propagate

To run integration tests:
```sh
yarn test:integration
```

### Unit tests

Test for individual components, providing test doubles in place of all
dependencies. These kinds of tests are least utilized for this repository, for
cases where:
- The component being developed is too complex for BDD, and it would be
  practical to create it via TDD
- The component is not tested in previous suites due to:
  - Being replaced by a test double in the [component tests](#component-tests)
  - Not falling into the category of [integration tests](#integration-tests)

To run unit tests:
```sh
yarn test:unit
```

### Build tests

With BDD approach, it makes compiling dock for manual testing optional. Through
the continuous integration pipeline, this suite ensures that the build process
is succeeding and the result consumable before the roll out process

To run build tests:
```sh
yarn test:build
```

### Test util tests

Test for the utility components consumed by other suites. These are to ensure
that the utilities are behaving as expected and prevent false positives or
incorrect failures in their consumers

Basically a unit test, complex utilities should be tested here before
consumption

Unused utilities may also be detected through this suite as it affects test
coverage, and an action maybe applied, whether to remove the utility or add a
test for it

To run test util tests:
```sh
yarn test:test-utils
```

<br/>

---

## Code Structure

### src

Source code directory, with the following sub directories and files:
- **clients**
  - Interchangeable service providers
- **commands**
  - Where dock commands are defined and its functionalities composed
  - Comparable to routers (in the root command) and controllers of back-end APIs
- **config**
  - Centralized configuration location
- **errors**
  - Centralized error definitions
  - Modules may define their own errors module, which could then extend from the
    definitions here
- **handlers**
  - Contains command handlers
  - Focused on domain functionality, composition is the responsibility of the
    commands
- **lib**
  - General purpose components
- **middlewares**
  - Contains re-usable command interceptors and post-processors
- **scripts**
  - Contains non JavaScript / TypeScript scripts
- **validation**
  - Contains validation code
  - Interpreted on tests and when running dock with on the fly interpretation
  - Pre-compiled during build
- **index.ts**
  - Entry point of dock
- **main.ts**
  - Main script of dock, where
    - Dependencies are injected
    - Commands are built
    - Errors are handled

### types

Type definitions directory. Partially mirrors [src](src) directory, with
additional sub directories and files:
- **dependencies**
  - Type definitions for dependencies with no defined types
- **global**
  - Contains utility typings
- **process/env.ts**
  - Contains enumeration of environment variable values

### build

Contains build related scripts per module / component. Partially mirrors
[src](src) directory

### test

Test directory, with the following sub directories:
- **setup**
  - Setup directory
- **specs**
  - Suites directory
  - Has sub directory of suites as mentioned in the [tests](#tests) section
    - [build](#build-tests)
    - [component](#component-tests)
    - [integration](#integration-tests)
    - [test-utils](#test-util-tests)
    - [unit](#unit-tests)
- **utils**
  - Utilities directory

<br/>

---

## Implementation Questions

### How do I add an orchestration engine?

Follow BDD approach with these steps:
1. Add an enumeration for the engine in
[types/config/project.ts](types/config/project.ts)
2. Find references to the engine enumeration in the component tests
([test/specs/component](test/specs/component))
3. If the enumeration is used in an engine suite, define an entry for the new
engine
1. Make the tests pass by implementing the engine in
[src/clients/orchestration-engine](src/clients/orchestration-engine)
    - It should implement `OrchestrationEngineInterface` in
      [types/clients/orchestration-engine.ts](types/clients/orchestration-engine.ts)
    - It may extend base class in
      [src/clients/orchestration-engine/abstract-base.ts](src/clients/orchestration-engine/abstract-base.ts)
    - It should be mapped in the engine factory in
      [src/clients/orchestration-engine/index.ts](src/clients/orchestration-engine/index.ts)

### How do I implement completion for other platforms?

Follow BDD approach with these steps:
1. Create a component test for the platform in
[test/specs/component/completion/platforms](test/specs/component/completion/platforms)
2. Include the platform in the required platforms middleware definition in
[src/commands/completion.ts](src/commands/completion.ts)
3. Make the tests pass by creating the completion scripts in
[src/scripts/completion](src/scripts/completion)
    - disable
      - Platform script with command instruction to disable completion for
      platform
    - enable
      - Platform script with command instruction to enable completion for platform
    - script
      - Platform script that will be invoked during completion which performs
    [dock completion](#dock-completion)

> Completion scripts are referenced with `NodeJS.Platform` values

#### Dock completion

To invoke dock's completion mechanism, call dock with the raw arguments passed
to the completion (excluding the script name), with an additional option
`--completion` with double quoted raw arguments (excluding the script name)
inclusive of trailing white spaces as its value:
```sh
  dock --completion "up mysql " up mysql 
```

If a list of completion words are provided, complete

If nothing is provided with an exit code of 0, complete with the system's
default completion

**Why would the same arguments for the completion be passed in the completion
option?**
- Argument data piped to the script does not include extra white spaces, which
  prevents determining:
  - If completion is new, or a continuation of a current word
  - The current completion position

### Why is dock bundled with webpack?

- When dock was started, bundling is a way to take advantage of module aliasing
for both development and production use
- Webpack is the bundler that the author is already familiar with
- A little bit of runtime performance bump may have been gained from bundling as
well

### Why does the [execute-command](src/lib/execute-command.ts) not propagate errors to its consumers?

- Because the spawned child process already inherit the parent process' stderr
and stdout
- Errors resulting from the command will be propagated directly to the user
- An implementation was tried to check the child process' exit code, and throw
an error if it is non zero. It was scratched as keyboard interrupts are being
flagged as an error

### Where are the program and project states stored?

As documented in [configstore](https://www.npmjs.com/package/configstore)
dependency, they are stored in `$XDG_CONFIG_HOME`. Specifically in
`~/.config/configstore/dock`

Contains the following sub directories and files:
- **program.json**
  - Program configuration
  - Contains project metadata and program settings
- **projects**
  - Contains registered project configurations
- **test**
  - Contains configuration created during tests
