import { createTestInstances, caseFactory } from '#/specs/component/__lib__'
import { projects } from '#/specs/component/__lib__/test'
import { Engine } from '@/config/project'

import type { Project } from '#/specs/component/__lib__/test/projects'
import type { Dock } from '#/utils/dock'
import type { Recorder } from '#/utils/recorder'

describe('Component - open [reference]', () => {
  describe('Engine', () => {
    const projectsMap: Record<
      Engine,
      Parameters<typeof createEngineSuite>[1]
    > = {
      [Engine.DOCKER_COMPOSE]: {
        WITH_REFERENCES: projects['open/engine/docker-compose/with-references'],
        NO_REFERENCE: projects['open/engine/docker-compose/no-reference'],
      },
    }

    describe(
      ...createEngineSuite(
        Engine.DOCKER_COMPOSE,
        projectsMap[Engine.DOCKER_COMPOSE],
      ),
    )
  })

  describe('Completion', () => {
    describe('Project is used', () => {
      describe('Project has references', () => {
        let dock: Dock
        let recorder: Recorder

        beforeAll(async () => {
          const instances = await createTestInstances(
            'completion-with-references',
            projects['open/with-references'],
          )

          dock = instances.dock
          recorder = instances.recorder
        })
        beforeEach(() => { recorder.clearRecording() })

        it('should complete if ran with no arguments', async () => {
          await dock.complete('open ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete if ran with partial reference', async () => {
          await dock.complete('open container-with')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete if ran with full reference', async () => {
          await dock.complete('open container-with-service')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete with system completion for succeeding arguments', async () => {
          await dock.complete('open container-with-service ')

          expect(recorder.recording).toMatchSnapshot()
        })

        describe(
          'Options are provided before command arguments',
          caseFactory.completion.options.createProvidedBeforeArguments(
            'open',
            () => ({ dock, recorder }),
            ['container-with-service', './path'],
          ),
        )

        describe(
          'Default options',
          caseFactory.completion.options.createDefault(
            'open',
            () => ({ dock, recorder }),
            ['container-with-service', './path'],
          ),
        )
      })

      describe('Project has no references', () => {
        let dock: Dock
        let recorder: Recorder

        beforeAll(async () => {
          const instances = await createTestInstances(
            'completion-no-reference',
            projects['open/no-reference'],
          )

          dock = instances.dock
          recorder = instances.recorder
        })
        beforeEach(() => { recorder.clearRecording() })

        it('should complete with system completion if ran with no arguments', async () => {
          await dock.complete('open ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete with system completion if ran with arguments', async () => {
          await dock.complete('open ./path ')

          expect(recorder.recording).toMatchSnapshot()
        })

        describe(
          'Options are provided before command arguments',
          caseFactory.completion.options.createProvidedBeforeArguments(
            'open',
            () => ({ dock, recorder }),
            ['./path', './another-path'],
          ),
        )

        describe(
          'Default options',
          caseFactory.completion.options.createDefault(
            'open',
            () => ({ dock, recorder }),
            ['./path', './another-path'],
          ),
        )
      })
    })

    describe('Project is not used', () => {
      const namespace = 'completion-no-project-used'
      const { dock, recorder } = createTestInstances(namespace)

      beforeEach(() => { recorder.clearRecording() })

      it('should complete with system completion if ran with no arguments', async () => {
        await dock.complete('open ')

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should complete with system completion if ran with arguments', async () => {
        await dock.complete('open ./path ')

        expect(recorder.recording).toMatchSnapshot()
      })

      describe(
        'Options are provided before command arguments',
        caseFactory.completion.options.createProvidedBeforeArguments(
          'open',
          () => ({ dock, recorder }),
          ['./path', './another-path'],
        ),
      )

      describe(
        'Default options',
        caseFactory.completion.options.createDefault(
          'open',
          () => ({ dock, recorder }),
          ['./path', './another-path'],
        ),
      )
    })
  })

  describe(
    'Auto reload',
    caseFactory.autoReload.createIntercepted(
      {
        GENERAL: { project: projects['open/with-references'] },
        VALID_CHANGE: { project: projects['open/updated-valid'] },
        INVALID_CHANGE: { project: projects['open/updated-invalid'] },
        REMOVED: { project: projects['open/removed'] },
      },
      {
        runValid: ({ dock }) => dock.run('open service'),
        runInvalid: ({ dock }) => dock.run('open undefined'),
      },
    ),
  )

  describe(
    'Version',
    caseFactory.version.createDefault('open', createTestInstances),
  )

  describe(
    'Help',
    caseFactory.help.createDefault('open', createTestInstances),
  )
})

function createEngineSuite(
  engine: Engine,
  projectsMap: {
    WITH_REFERENCES: Project,
    NO_REFERENCE: Project,
  },
): [Engine, Method] {
  function createNameSpace(name: string) { return `${engine}-${name}`}

  function suite() {
    describe('Command', () => {
      describe('Project is used', () => {
        describe('Project has references', () => {
          let dock: Dock
          let recorder: Recorder

          beforeAll(async () => {
            const namespace = createNameSpace('with-references')
            const instances = await createTestInstances(
              namespace,
              projectsMap.WITH_REFERENCES,
            )

            dock = instances.dock
            recorder = instances.recorder
          })
          beforeEach(() => recorder.clearRecording())

          it('should open container for container reference', async () => {
            await dock.run('open container-no-service')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should open container for service reference', async () => {
            await dock.run('open service')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should open container for service with entry-point reference', async () => {
            await dock.run('open service-entry-point')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should open container for service with shell', async () => {
            await dock.run('open service-with-shell')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should open container for service of the same name', async () => {
            await dock.run('open container-same-service')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should open container for service of the same name with entry point', async () => {
            await dock.run('open container-same-service-with-entry-point')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should open container for service of the same name with shell', async () => {
            await dock.run('open container-same-service-with-shell')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should open container for service of the same name with entry point and shell', async () => {
            await dock.run(
              'open container-same-service-with-entry-point-and-shell',
            )

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should emit error if ran with no reference', async () => {
            await dock.run('open')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should emit error if ran with unknown reference', async () => {
            await dock.run('open unknown-reference')

            expect(recorder.recording).toMatchSnapshot()
          })
        })

        describe('Project has no reference', () => {
          let dock: Dock
          let recorder: Recorder

          beforeAll(async () => {
            const namespace = createNameSpace('no-reference')
            const instances = await createTestInstances(
              namespace,
              projectsMap.NO_REFERENCE,
            )

            dock = instances.dock
            recorder = instances.recorder
          })
          beforeEach(() => recorder.clearRecording())

          it('should emit error if ran with no reference', async () => {
            await dock.run('open')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should emit error if ran with reference', async () => {
            await dock.run('open service')

            expect(recorder.recording).toMatchSnapshot()
          })
        })
      })

      describe('Project is not used', () => {
        const namespace = createNameSpace('no-project-used')
        const { dock ,recorder } = createTestInstances(namespace)
        const location = projectsMap.WITH_REFERENCES.location.ABSOLUTE

        beforeAll(async () => await dock.run(`register ${location}`))
        beforeEach(() => recorder.clearRecording())

        it('should emit error if ran with no reference', async () => {
          await dock.run('open')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should emit error if ran with reference', async () => {
          await dock.run('open service')

          expect(recorder.recording).toMatchSnapshot()
        })
      })
    })
  }

  return [engine, suite]
}
