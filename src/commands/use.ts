import * as commander from 'commander'

import { use } from '~/handlers'
import { createActionHandler, decorateWithDefaults } from '~/commands'
import {
  requireArguments,
  requireUsedConfigFile,
  autoReload,
} from '~/middlewares'

import type { Create } from '@/command'

export const name = 'use'
export const nameAndArgs = `${name} [project]`

const description
  = 'Activate project for usage when invoking docker abstraction engine commands'
const argumentDescriptions = { project: 'Project name' }

export const create: Create = (provisions) => {
  const handler = createActionHandler(
    use.handle,
    use.complete,
    provisions,
    [requireArguments(nameAndArgs)],
    [requireUsedConfigFile, autoReload],
  )
  const command = new commander.Command()
    .command(nameAndArgs)
    .description(description, argumentDescriptions)
    .action(handler)

  return decorateWithDefaults(command, provisions)
}
