import {
  createWriteStream,
  readFileSync,
  accessSync,
  mkdirSync,
} from 'fs'
import { readFile, unlink } from 'fs/promises'
import { randomBytes } from 'crypto'

import findRemoveSync from 'find-remove'

import type { WriteStream } from 'fs'

const exitSignals = [
  'SIGINT',
  'SIGTERM',
  'SIGQUIT',
  'exit',
  'uncaughtException',
]

/**
 * What are the use cases for this?
 *  - If we want to pipe stdout and stderr into a WriteStream which we could
 *    read and discard after
 *
 * Why fs' write stream? Why not implement in memory Write stream?
 *  - child_process' stdio option is particular about the type of Streams that
 *    it accepts
 *  - See https://github.com/sindresorhus/into-stream/issues/10#issuecomment-541159459
 */
export class TemporaryWriteStreamMediator {
  private _closeData: Nullable<Buffer> = null

  constructor(readonly stream: WriteStream) {}

  get data(): string {
    return this.rawData.toString().trim()
  }

  private get rawData(): Buffer {
    return this?._closeData || readFileSync(this.stream.path)
  }

  close(): Promise<Array<SideEffect>> {
    const endSignal = new Promise<SideEffect>(
      (resolve) => this.stream.end(resolve),
    )
    const readSignal = readFile(this.stream.path)
      .then((rawData) => this._closeData = rawData)
      .then(() => unlink(this.stream.path))

    return Promise.all([endSignal, readSignal])
  }

  static defaultDirectory = '/tmp/dock/test/utils/temporary-writable-stream'

  static async exists(location: string): Promise<boolean> {
    try {
      accessSync(location)

      return true
    } catch (err) {
      return false
    }
  }

  static cleanupDefaultDirectory(): SideEffect {
    const { exists, defaultDirectory } = TemporaryWriteStreamMediator

    if (!exists(defaultDirectory)) return

    /**
     * Only remove files older than 1 minute because Jest runs test suites in
     *  separate processes, invoking the cleanup script for each will remove
     *  un-closed stream mediators
     */
    findRemoveSync(
      defaultDirectory,
      { files: '*.*', extensions: 'txt', age: { seconds: 60 } },
    )
  }

  static initializeDefaultDirectory(): SideEffect {
    const { defaultDirectory } = TemporaryWriteStreamMediator

    mkdirSync(defaultDirectory, { recursive: true })
  }

  static async createDefaultWriteStream(): Promise<WriteStream> {
    const { defaultDirectory } = TemporaryWriteStreamMediator

    const fileName = `${randomBytes(16).toString('hex')}.txt`
    const stream = createWriteStream(`${defaultDirectory}/${fileName}`)

    await new Promise((resolve) => stream.once('ready', resolve))

    return stream
  }

  static async create(): Promise<TemporaryWriteStreamMediator> {
    TemporaryWriteStreamMediator.initializeDefaultDirectory()

    const stream = await TemporaryWriteStreamMediator.createDefaultWriteStream()
    const mediator = new TemporaryWriteStreamMediator(stream)

    exitSignals.forEach(
      (signal) => process.once(signal, () => mediator.close()),
    )

    return mediator
  }
}
