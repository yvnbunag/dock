import * as commander from 'commander'

import { completion } from '~/handlers'
import { createActionHandler, decorateWithDefaults } from '~/commands'
import { requirePlatforms, requireArguments } from '~/middlewares'
import { toIndentedStringMatrix } from '~/lib/print-formatter/matrix'

import type { Create } from '@/command'

export const name = 'completion'

export enum Instruction {
  ENABLE = 'enable',
  SCRIPT = 'script',
  DISABLE = 'disable',
}

export const instructionValues = Object.values(Instruction)

const nameAndArgs = `${name} [instruction]`
const description = 'Show instructions to enable/disable dock completion'
const argumentDescriptions = { instruction: 'Print specified instruction' }
const instructionDescriptions: Record<Instruction, string> = {
  [Instruction.ENABLE]: 'Print instruction to enable completion',
  [Instruction.SCRIPT]: 'Print completion script',
  [Instruction.DISABLE]: 'Print instruction to disable completion',
}
const instructionDescriptionsMatrix = toIndentedStringMatrix(
  Object.entries(instructionDescriptions),
  { initial: 2, left: 7 },
)
const instructionsHelpSection
  = `\nInstructions:\n${instructionDescriptionsMatrix}`

export const create: Create = (provisions) => {
  const handler = createActionHandler(
    completion.handle,
    completion.complete,
    provisions,
    [
      requirePlatforms(['linux', 'darwin'], 'Completion'),
      requireArguments(nameAndArgs),
    ],
  )
  const command = new commander.Command()
    .command(nameAndArgs)
    .description(description, argumentDescriptions)
    .addHelpText('after', instructionsHelpSection)
    .action(handler)

  return decorateWithDefaults(command, provisions)
}
