import { open, run } from '~/handlers'
import { programHelpers } from '~/lib'
import { extractCompletionMetadata, completeFilename } from '~/lib/completion'

import type { Provisions } from '@/command'
import type { Handler, CompletionHandler } from '@/handler'
import type { ContainerCommands } from '@/clients/orchestration-engine'

type Arguments = [ContainerCommands]

export const handle: Handler<Arguments> = async function (
  [commands],
  options,
) {
  const { provisions } = options
  const { runDirectory } = provisions.environmentReader
  const services = await programHelpers.projects.services.getKeys(provisions)

  if (!services.includes(runDirectory)) {
    return handleNotServiceDirectory(runDirectory, provisions)
  }

  if (commands.filter(Boolean).length) {
    return run.handle([runDirectory, commands], options)
  }

  return open.handle([runDirectory], options)
}

function handleNotServiceDirectory(
  runDirectory: string,
  provisions: Provisions,
): SideEffect {
  provisions.emitter
    .emitMessage(`Current directory (${runDirectory}) is not a service reference`)
    .emitMessage('Run dock --help for usage')
}

export const complete: CompletionHandler<Arguments> = async function (
  commandArguments,
  { provisions },
) {
  const metadata = extractCompletionMetadata(provisions, 0)

  if (metadata.isOptionCompletion()) {
    return programHelpers.completion.completeOptions(metadata, provisions)
  }

  return completeFilename(provisions.emitter)
}
