import { pick } from 'lodash'

import { NOT_FOUND } from '~/config/exit-codes'
import { toBulletedSentence } from '~/lib/print-formatter/list'
import { not, hasEntryIn } from '~/lib/predicate-factory'

import type { Provisions } from '@/command'
import type {
  ServiceName,
  ServiceNames,
  Service,
  ContainerName,
  Container,
} from '@/config/project'

export function handleNotDefined(
  undefinedServices: ServiceNames | ServiceName,
  provisions: Provisions,
): SideEffect<never> {
  const normalizedServices = Array.isArray(undefinedServices)
    ? undefinedServices
    : [undefinedServices]
  const servicesList = toBulletedSentence(normalizedServices)

  return provisions.emitter
    .emitError(`The following service/s are not defined:\n${servicesList}`)
    .emitError('Make sure that they are defined in the project config')
    .exit(NOT_FOUND)
}

export async function extractUndefined(
  services: ServiceNames,
  provisions: Provisions,
): Promise<ServiceNames> | never {
  const definedServices = await getKeys(provisions)
  const undefinedServices = services.filter(not(hasEntryIn(definedServices)))

  return [...new Set(undefinedServices)]
}

export async function getKeys(
  provisions: Provisions,
): Promise<ServiceNames> | never {
  const provider = await provisions.programMediator.getUsedProjectProvider()
  const { services } = await provider.get('aggregates')

  return services.values
}

export function get(
  provisions: Provisions,
): Promise<Record<ServiceName, Service>> {
  return provisions.programMediator.getUsedProjectProvider()
    .then((provider) => provider.get('rawConfig'))
    .then(({ services }) => services || {})
}

interface InstallAggregate {
  container: {
    name: ContainerName,
    shell: Container['shell'],
  },
  install: NonNullable<Service['install']>,
  entryPoint: Service['entry-point'],
}

export type InstallMap = Record<ServiceName, InstallAggregate>

export async function getInstallMap(
  provisions: Pick<Provisions, 'programMediator'>,
  names: ServiceNames = [],
): Promise<InstallMap> {
  const { services = {}, containers = {} } = await provisions.programMediator
    .getUsedProjectProvider()
    .then((provider) => provider.get('rawConfig'))
  const installMap: InstallMap = Object.fromEntries(
    Object
      .entries(services)
      .map<[string, InstallAggregate]>(([name, service]) => {
        const container = {
          name: service.container,
          shell: containers[service.container]?.shell,
        }
        const installAggregate: InstallAggregate = {
          container,
          install: service.install || [],
          entryPoint: service['entry-point'],
        }

        return [name, installAggregate]
      })
      .filter(([, { install }]) => install.length),
  )

  if (names.length) return pick(installMap, names)

  return installMap
}
