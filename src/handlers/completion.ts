import { readFile } from 'fs/promises'

import { Instruction, instructionValues } from '~/commands/completion'
import { exitCodes } from '~/config'
import { programHelpers } from '~/lib'
import { toSentence } from '~/lib/print-formatter/list'
import { extractCompletionMetadata } from '~/lib/completion'

import type { Provisions } from '@/command'
import type { Handler, CompletionHandler } from '@/handler'

type Arguments = [Instruction]

export const handle: Handler<Arguments> = async function (
  [instruction],
  { provisions },
) {
  if (!instructionValues.includes(instruction)) {
    return handleUnknownInstruction(instruction, provisions)
  }

  return handleCompletion(instruction, provisions)
}

function handleUnknownInstruction(
  instruction: Instruction,
  provisions: Provisions,
): SideEffect<never> {
  const message = {
    unknownInstruction: `Unknown completion instruction '${instruction}'`,
    validInstructions:
      `Valid instructions are ${toSentence(instructionValues)}`,
  }

  return provisions.emitter
    .emitError(message.unknownInstruction)
    .emitError(message.validInstructions)
    .exit(exitCodes.UNKNOWN)
}

async function handleCompletion(
  instruction: Instruction,
  provisions: Provisions,
): Promise<SideEffect> {
  const { platform, scriptDirectory } = provisions.environmentReader
  const instructionMessage = await readFile(
    `${scriptDirectory}/scripts/completion/${platform}/${instruction}.sh`,
    'utf-8',
  )

  provisions.emitter.emitMessage(instructionMessage)
}

export const complete: CompletionHandler<Arguments> = async function (
  commandArguments,
  { provisions },
) {
  const metadata = extractCompletionMetadata(provisions)

  if (metadata.isOptionCompletion()) {
    return programHelpers.completion.completeOptions(metadata, provisions)
  }

  return programHelpers.completion
    .positionalComplete(metadata, provisions, [Object.values(Instruction)])
}
