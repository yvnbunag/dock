import {
  toSentence,
  determineLinkingVerb,
  toSentenceWithLinkingVerb,
  toBulletedSentence,
} from '~/lib/print-formatter/list'
import { primitive } from '#/utils/values'

describe('toSentenceWithLinkingVerb', () => {
  it(`should format list with no entry`, () => {
    expect(toSentenceWithLinkingVerb([])).toMatchInlineSnapshot(`"is"`)
  })

  it(`should format list with 1 entry`, () => {
    const result = toSentenceWithLinkingVerb(['apple'])

    expect(result).toMatchInlineSnapshot(`"'apple' is"`)
  })

  it(`should format list with more than 1 entries`, () => {
    const result = toSentenceWithLinkingVerb(['apple', 'banana'])

    expect(result).toMatchInlineSnapshot(`"'apple' and 'banana' are"`)
  })
})

describe('toSentence', () => {
  it('should return empty string for list with no entries', () => {
    const result = toSentence([])

    expect(result).toMatchInlineSnapshot(`""`)
  })

  it('should format to sentence a list with 1 entry', () => {
    const result = toSentence(['apple'])

    expect(result).toMatchInlineSnapshot(`"'apple'"`)
  })

  it('should format to sentence a list with 2 entries', () => {
    const result = toSentence(['apple', 'bananas'])

    expect(result).toMatchInlineSnapshot(`"'apple' and 'bananas'"`)
  })

  it('should format to sentence a list with more than 2 entries', () => {
    const result = toSentence(['apple', 'banana', 'cherries'])

    expect(result).toMatchInlineSnapshot(`"'apple', 'banana' and 'cherries'"`)
  })

  it('should format to sentence a list with varying data types', () => {
    expect(toSentence(Object.values(primitive))).toMatchSnapshot()
  })
})

describe('determineLinkingVerb', () => {
  it(`should return 'is' if list has no entry`, () => {
    expect(determineLinkingVerb([])).toMatchInlineSnapshot(`"is"`)
  })

  it(`should return 'is' if list has 1 entry`, () => {
    expect(determineLinkingVerb(['apple'])).toMatchInlineSnapshot(`"is"`)
  })

  it(`should return 'are' if list has more than 1 entries`, () => {
    const result = determineLinkingVerb(['apple', 'banana'])

    expect(result).toMatchInlineSnapshot(`"are"`)
  })
})

describe('toBulletedSentence', () => {
  it('should return empty string for list with no entries', () => {
    const result = toBulletedSentence([])

    expect(result).toMatchInlineSnapshot(`""`)
  })

  it('should format to bulleted sentence a list with 1 entry', () => {
    const result = toBulletedSentence(['apple'])

    expect(result).toMatchSnapshot()
  })

  it('should format to bulleted sentence a list with 2 entries', () => {
    const result = toBulletedSentence(['apple', 'bananas'])

    expect(result).toMatchSnapshot()
  })

  it('should format to bulleted sentence a list with more than 2 entries', () => {
    const result = toBulletedSentence(['apple', 'banana', 'cherries'])

    expect(result).toMatchSnapshot()
  })

  it('should format to bulleted sentence a list with varying data types', () => {
    expect(toBulletedSentence(Object.values(primitive))).toMatchSnapshot()
  })
})
