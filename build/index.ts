import { existsSync } from 'fs'
import { resolve } from 'path'
import { execSync } from 'child_process'

import * as root from 'app-root-path'
import { BannerPlugin } from 'webpack'

import * as completionScripts from '+/build/scripts/completion'
import * as configValidation from '+/build/validation/config'

import type { Compiler } from 'webpack'

export const filename = 'dock.js'

export const path = resolve(root.path, 'dist')

const makeScriptExecutablePlugin = (() => {
  const pluginName = '+/build/make-script-executable'

  function makeScriptExecutable(): SideEffect {
    const scriptLocation = resolve(path, filename)

    if (existsSync(scriptLocation)) execSync(`chmod +x ${scriptLocation}`)
  }

  return {
    apply: (compiler: Compiler): void => {
      compiler.hooks.done.tap(pluginName, makeScriptExecutable)
    },
  }
})()

const injectShebangPlugin = new BannerPlugin({
  banner: '#!/usr/bin/env node',
  raw: true,
})

export const plugins = [
  ...configValidation.plugins,
  ...completionScripts.plugins,
  injectShebangPlugin,
  makeScriptExecutablePlugin,
]
