export const HELP = [
  '-h',
  '--help',
]

export const VERSION = [
  '-v',
  '--version',
]

export const ALL = [
  ...HELP,
  ...VERSION,
]
