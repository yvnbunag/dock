import { defaultOptions } from '~/config'
import { not, hasEntryIn } from '~/lib/predicate-factory'
import { emitCompletion, SystemCompletion } from '~/lib/completion'

import type { Provisions } from '@/command'
import type { CompletionMetadata } from '~/lib/completion'

export function completeOptions(
  metadata: CompletionMetadata,
  provisions: Provisions,
  commandOptions: Array<string> = [],
): SideEffect {
  const options = [...commandOptions, ...defaultOptions.ALL]
    .filter(not(hasEntryIn(metadata.optionArguments)))
    .join(' ')

  return emitCompletion(options, provisions.emitter)
}

export function positionalComplete(
  metadata: CompletionMetadata,
  provisions: Provisions,
  positionalChoices: Array<Array<string>> = [],
): SideEffect {
  const { commandArguments, lastCommandArgument, position } = metadata

  if (
    commandArguments.length <= positionalChoices.length
    && (!lastCommandArgument && metadata.isNew() || metadata.isContinuation())
  ) {
    const choices = positionalChoices[position - 1].join(' ')

    return emitCompletion(
      choices || SystemCompletion.DEFAULT,
      provisions.emitter,
    )
  }

  return emitCompletion(SystemCompletion.DEFAULT, provisions.emitter)
}

export function variadicComplete(
  metadata: CompletionMetadata,
  provisions: Provisions,
  choices: Array<string> = [],
): SideEffect {
  const unusedChoices = choices
    .filter(not(hasEntryIn(metadata.commandArguments)))
    .join(' ')

  return emitCompletion(
    unusedChoices || SystemCompletion.DEFAULT,
    provisions.emitter,
  )
}
