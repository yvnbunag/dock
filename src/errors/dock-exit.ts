import { DockError } from '~/errors/dock-error'
import { exitCodes } from '~/config'


export class DockExit extends DockError {
  name = 'DockExit'
  constructor(readonly code = exitCodes.SUCCESS) { super(code) }
}
