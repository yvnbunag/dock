import { programHelpers } from '~/lib'
import { extractCompletionMetadata } from '~/lib/completion'

import type { Provisions } from '@/command'
import type { Handler, CompletionHandler } from '@/handler'
import type { Project } from '@/config'

type Arguments = [Project.Name]

export const handle: Handler<Arguments> = async function (
  [name],
  { provisions },
) {
  const { names } = await provisions.programMediator.getProjects()

  if (!names.includes(name)) {
    return programHelpers.projects.name
      .handleNotRegistered(name, names, provisions)
  }

  return handleUnregister(name, provisions)
}

async function handleUnregister(
  name: Project.Name,
  provisions: Provisions,
): AsyncSideEffect {
  await provisions.programMediator.unloadProject(name)

  provisions.emitter.emitMessage(`Project '${name}' unregistered`)
}

export const complete: CompletionHandler<Arguments> = async function (
  commandArguments,
  { provisions },
) {
  const metadata = extractCompletionMetadata(provisions)

  if (metadata.isOptionCompletion()) {
    return programHelpers.completion.completeOptions(metadata, provisions)
  }

  const { names } = await provisions.programMediator.getProjects()

  return programHelpers.completion
    .positionalComplete(metadata, provisions, [names])
}
