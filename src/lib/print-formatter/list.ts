import { toString, singleQuote } from '~/lib/print-formatter/word'

export function toSentenceWithLinkingVerb(list: Array<Primitive>): string {
  const sentence = toSentence(list)
  const space = sentence ? ' ' : ''
  const linkingVerb = determineLinkingVerb(list)

  return `${sentence}${space}${linkingVerb}`
}

export function toSentence(list: Array<Primitive>): string {
  const localList = list
    .map(toString)
    .filter(Boolean)
    .map((entry) => entry.trim())
    .map(singleQuote)
  const lastEntry = localList.pop()

  if (!lastEntry) return ''
  if (!localList.length) return lastEntry

  return [localList.join(', '), lastEntry].join(' and ')
}

export function determineLinkingVerb(list: Array<Primitive>): string {
  if (list.length > 1) return 'are'

  return 'is'
}

export function toBulletedSentence(list: Array<Primitive>): string {
  return list
    .map(toString)
    .map((entry) => `• ${entry}`)
    .join('\n')
}
