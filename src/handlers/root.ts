import { programHelpers } from '~/lib'
import { extractCompletionMetadata } from '~/lib/completion'
import { has } from '~/lib/predicate-factory'
import { extract } from '~/lib/transform-factory'

import type { Factory, CommandArguments } from '@/command'
import type { Handler, CompletionHandler } from '@/handler'

/**
 * Create completion decorated root handler from handler implementation
 */
export function create<RootArguments extends CommandArguments>(
  rootHandler: Handler<RootArguments>,
  rootCompletion: CompletionHandler<RootArguments>,
  subCommands: Array<Factory>,
): {
  handle: Handler<RootArguments>,
  complete: CompletionHandler<RootArguments>,
} {
  return {
    handle: rootHandler,
    complete: createCompletionHandler(subCommands, rootCompletion),
  }
}

function createCompletionHandler<RootArguments extends CommandArguments>(
  subCommands: Array<Factory>,
  rootCompletion: CompletionHandler<RootArguments>,
): CompletionHandler<RootArguments> {
  return async function (commandArguments, options) {
    const subCommandNames = subCommands.map(extract('name'))
    const firstArgument = Array.isArray(commandArguments[0])
      ? commandArguments[0][0]
      : commandArguments[0]

    if (firstArgument && !subCommandNames.find(has(firstArgument))) {
      return rootCompletion(commandArguments, options)
    }

    const { provisions } = options
    const metadata = extractCompletionMetadata(provisions, 0)

    if (metadata.isOptionCompletion()) {
      return programHelpers.completion.completeOptions(metadata, provisions)
    }

    return programHelpers.completion
      .positionalComplete(metadata, provisions, [subCommandNames])
  }
}
