import { down, up } from '~/handlers'
import { programHelpers } from '~/lib'
import { extractCompletionMetadata } from '~/lib/completion'

import type { Options } from '@/command'
import type { Handler, CompletionHandler } from '@/handler'
import type { ModeNames } from '@/config/project'

type Arguments = [ModeNames]

export const handle: Handler<Arguments> = async function ([modes], options) {
  const { provisions } = options
  const undefinedModes = await programHelpers.projects.modes
    .extractUndefined(modes, provisions)

  if (undefinedModes.length) {
    return programHelpers.projects.modes
      .handleNotDefined(undefinedModes, provisions)
  }

  return handleMode(modes, options)
}

async function handleMode(
  modes: ModeNames,
  options: Options,
): AsyncSideEffect | never {
  const { provisions } = options
  const modeMap = await programHelpers.projects.modes.getMap(provisions, modes)
  const modeContainers = Object.values(modeMap).flat()
  const uniqueModeContainers = [...new Set(modeContainers)]

  await down.handle([[]], options)
  await up.handle([uniqueModeContainers], options)
}

export const complete: CompletionHandler<Arguments> = async function (
  commandArguments,
  { provisions },
) {
  const metadata = extractCompletionMetadata(provisions)

  if (metadata.isOptionCompletion()) {
    return programHelpers.completion.completeOptions(metadata, provisions)
  }

  const modes = await programHelpers.projects.modes.getKeys(provisions)

  return programHelpers.completion
    .variadicComplete(metadata, provisions, modes)
}
