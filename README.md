# @ianbunag/dock

![@ianbunag/dock](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/logo/dock-x-spaced.svg)

<br/>

[![npm version](https://img.shields.io/npm/v/@ianbunag/dock.svg?style=flat)](https://www.npmjs.com/package/@ianbunag/dock)
[![nodejs version](https://img.shields.io/node/v/@ianbunag/dock)](https://nodejs.org/en/)
[![codecov](https://codecov.io/gl/ianbunag/dock/branch/master/graph/badge.svg?token=83LTCASX03)](https://codecov.io/gl/ianbunag/dock)
[![npms.io (quality)](https://img.shields.io/npms-io/quality-score/@ianbunag/dock)](https://npms.io/search?q=%40ianbunag%2Fdock)
[![Known Vulnerabilities](https://snyk.io/test/npm/@ianbunag/dock/badge.svg)](https://snyk.io/test/npm/@ianbunag/dock)
[![Continuous Integration](https://img.shields.io/badge/dynamic/json?color=informational&label=%E2%9A%99%20CI&query=%24%5B0%5D.status&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F23372153%2Fpipelines%3Fref%3Dmaster%26per_page%3D1)](https://gitlab.com/ianbunag/dock/-/pipelines?page=1&scope=all&ref=master)
[![Continuous Delivery](https://img.shields.io/badge/dynamic/json?color=informational&label=%E2%9A%99%20CD&query=%24%5B0%5D.status&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F23372153%2Fpipelines%3Fscope%3Dtags%26per_page%3D1)](https://gitlab.com/ianbunag/dock/-/pipelines?page=1&scope=tags)
[![license](https://img.shields.io/npm/l/@ianbunag/dock)](https://gitlab.com/ianbunag/dock/-/blob/master/LICENSE)

A CLI tool to help manage and consume development environments containerized
with docker and configured with docker-compose

<br/>

---

<!-- omit in toc -->
## Contents

- [When would I use dock?](#when-would-i-use-dock)
- [What docker orchestration engines are currently supported?](#what-docker-orchestration-engines-are-currently-supported)
- [Requirements](#requirements)
- [Installation](#installation)
  - [Globally with npm](#globally-with-npm)
  - [Globally with yarn](#globally-with-yarn)
- [Configuration](#configuration)
- [Usage](#usage)
  - [completion](#completion)
  - [register](#register)
  - [projects](#projects)
  - [use](#use)
  - [build](#build)
  - [install](#install)
  - [up](#up)
  - [down](#down)
  - [mode](#mode)
  - [open](#open)
  - [run](#run)
  - [[root]](#root)
  - [reload](#reload)
  - [unregister](#unregister)
- [Example Projects](#example-projects)
- [Contributing](#contributing)
- [Exit Codes](#exit-codes)
- [Repository](#repository)

<br/>

---

## When would I use dock?

- If you need to manage your containerized development environment globally
- If you need ease when managing your containers via CLI only due to limited
  development machine resources
  - When having to bring up or down specific containers for services currently
    being developed
  - When occasionally running commands in a container, without having to
    remember the engine command and options needed

<br/>

---

## What docker orchestration engines are currently supported?

Only [docker-compose](https://docs.docker.com/compose/). Other engines may be
readily supported

<br/>

---

## Requirements

1. [Docker](https://www.docker.com/) version 19.03 or higher
2. [Docker Compose](https://docs.docker.com/compose/) version 1.27 or higher

<br/>

---

## Installation

### Globally with npm

```sh
npm install -g @ianbunag/dock
```

### Globally with yarn

```sh
yarn global add @ianbunag/dock
```

<br/>

---

## Configuration

Before a project may be registered and used with dock, a dock configuration
`(dock.config.yml)` must be present in the project directory, along with the
[docker-compose configuration](https://docs.docker.com/compose/compose-file/)

See [Configuration Reference](https://gitlab.com/ianbunag/dock/-/blob/master/documentation/configuration.md)

<details>
  <summary>
    Expand to see example configuration from
    <a href="https://gitlab.com/ianbunag/library-system-stack">
      Library System Stack
    </a>
  </summary>

  ```yml
  name: library-system

  containers:
    nginx:
      shell: sh
    mysql: {}
    node16:
      shell: sh

  services:
    library-system:
      container: node16
      entry-point: ./applications/library-system
      install:
        - yarn
        - cp /shared/applications/library-system/.env .env
    library-system-api:
      container: node16
      entry-point: ./microservices/library-system-api
      install:
        - yarn
        - cp /shared/microservices/library-system-api/.env .env
        - yarn database:sync
        - yarn database:seed

  modes:
    front-end:
      containers:
        - nginx
      services:
        - library-system
    back-end:
      containers:
        - nginx
        - mysql
      services:
        - library-system-api
    full-stack:
      containers:
        - nginx
        - mysql
      services:
        - library-system
        - library-system-api

  ```
</details>

<br/>

---

## Usage

Listed below are the commands to help you manage your development environments.
You may use the built in help command for additional options and information
```sh
dock --help

# Or for specific commands
dock [command] --help
```

The dock command may be invoked without a sub command. See [[root]](#root)
command usage reference

---

### completion

Show instructions to enable/disable dock completion

#### Enable completion

```sh
dock completion enable
```

<details>
  <summary>Expand to see recording</summary>

  ![Enable completion](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/completion-enable.gif)
</details>

#### Disable completion

```sh
dock completion disable
```

<details>
  <summary>Expand to see recording</summary>

  ![Disable completion](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/completion-disable.gif)
</details>

---

### register

Register project

#### Register current directory

```sh
dock register
```

<details>
  <summary>Expand to see recording</summary>

  ![Register current directory](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/register-current-directory.gif)
</details>

#### Register provided path to directory

```sh
dock register [path]
```

<details>
  <summary>Expand to see recording</summary>

  ![Register provided directory](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/register-provided-directory.gif)
</details>

---

### projects

List registered projects

```sh
dock projects
```

<details>
  <summary>Expand to see recording</summary>

  ![List projects](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/projects.gif)
</details>

---

### use

Activate project for usage when invoking docker abstraction engine commands

```sh
dock use [project]
```

<details>
  <summary>Expand to see recording</summary>

  ![Use project](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/use.gif)
</details>

---

### build

Build container/s or service container/s

#### Build all containers

```sh
dock build
```

<details>
  <summary>Expand to see recording</summary>

  ![Build all containers](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/build-all.gif)
</details>

#### Build specific container/s or service container/s

```sh
dock build [reference...]
```

<details>
  <summary>Expand to see recording</summary>

  ![Build specific references](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/build-references.gif)
</details>

---

### install

Run install script/s for services

#### Run all install scripts

```sh
dock install
```

<details>
  <summary>Expand to see recording</summary>

  ![Build all containers](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/install-all.gif)
</details>

#### Run install scripts of specific service/s

```sh
dock install [service...]
```

<details>
  <summary>Expand to see recording</summary>

  ![Build specific references](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/install-references.gif)
</details>

---

### up

Bring container/s or service container/s up

#### Bring up all containers

```sh
dock up
```

<details>
  <summary>Expand to see recording</summary>

  ![Bring up all containers](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/up-all.gif)
</details>

#### Bring up specific container/s or service container/s

```sh
dock up [reference...]
```

<details>
  <summary>Expand to see recording</summary>

  ![Bring up specific references](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/up-reference.gif)
</details>

---

### down

Stop and remove container/s or service container/s

#### Bring down all containers

```sh
dock down
```

<details>
  <summary>Expand to see recording</summary>

  ![Bring down all containers](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/down-all.gif)
</details>

#### Bring down specific container/s or service container/s

```sh
dock down [reference...]
```

<details>
  <summary>Expand to see recording</summary>

  ![Bring down specific references](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/down-reference.gif)
</details>

---

### mode

Switch running containers with provided mode container/s and service container/s

```sh
dock mode [mode...]
```

<details>
  <summary>Expand to see recording</summary>

  ![Switch to mode](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/mode.gif)
</details>

---

### open

Open interactive shell of container or service container

#### Open container

```sh
dock open [container]
```

<details>
  <summary>Expand to see recording</summary>

  ![Open container](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/open-container.gif)
</details>

#### Open service container

```sh
dock open [service]
```

<details>
  <summary>Expand to see recording</summary>

  ![Open service container](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/open-service.gif)
</details>

---

### run

Run command in container or service container

#### Run command in container

```sh
dock run [container] [command...]
```

<details>
  <summary>Expand to see recording</summary>

  ![Run command in container](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/run-container.gif)
</details>

#### Run command in service container

```sh
dock run [service] [command...]
```

<details>
  <summary>Expand to see recording</summary>

  ![Run command in service container](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/run-service.gif)
</details>

---

### [root]

Given that the first argument is not a command and current directory is a service reference:
- Open interactive shell of service container if no arguments are provided
- Run command in service container if arguments are provided

#### Open service container

```sh
dock
```

<details>
  <summary>Expand to see recording</summary>

  ![Open service container](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/root-open.gif)
</details>

#### Run command in service container

```sh
dock [service-command...]
```

<details>
  <summary>Expand to see recording</summary>

  ![Run command in service container](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/root-run.gif)
</details>

---

### reload

Reload used project

```sh
dock reload
```

<details>
  <summary>Expand to see recording</summary>

  ![Reload used project](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/reload.gif)
</details>

> Command not needed to be used for every new change in the configuration file
> as project gets automatically reloaded every time a orchestration engine
> command is invoked

---

### unregister

Unregister project

```sh
dock unregister [project]
```

<details>
  <summary>Expand to see recording</summary>

  ![Unregister project](https://gitlab.com/ianbunag/dock/-/raw/master/documentation/graphics/commands/unregister.gif)
</details>

<br/>

---

## Example Projects

See [Library System Stack](https://gitlab.com/ianbunag/library-system-stack)
repository for example configuration and usage

<br/>

---

## Contributing

See [Contributing Guide](https://gitlab.com/ianbunag/dock/-/blob/master/CONTRIBUTING.md)

<br/>

---

## Exit Codes

See [Exit Codes Reference](https://gitlab.com/ianbunag/dock/-/blob/master/documentation/exit-codes.md)

<br/>

---

## Repository

See [Repository](https://gitlab.com/ianbunag/dock)
