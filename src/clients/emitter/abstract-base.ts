import chalk from 'chalk'

import { DockExit } from '~/errors'

import type { Emitter as EmitterInterface } from '@/clients'

export abstract class Emitter implements EmitterInterface {
  abstract emitMessage(message: string): SideEffect<Emitter>

  abstract emitWarning(warning: string): SideEffect<Emitter>

  abstract emitError(error: string): SideEffect<Emitter>

  exit(code: number): SideEffect<never> {
    throw new DockExit(code)
  }

  static formatMessage(message: string): string {
    return message
  }

  static formatWarning(message: string): string {
    return chalk.yellow(message)
  }

  static formatError(message: string): string {
    return chalk.red(message)
  }
}
