import type {
  ConfigProvider as ConfigProviderInterface,
  ConfigSource as ConfigSourceInterface,
  Config as ConfigInterface,
  Key as KeyInterface,
} from '@/clients/config-store'


export class ConfigProvider<
  Config extends ConfigInterface = ConfigInterface,
> implements ConfigProviderInterface<Config> {
  private _cacheIsPartial = true
  private _cache: Partial<Config> = {}

  constructor(
    private readonly configProvider: ConfigProviderInterface<Config>,
  ) {}

  async get<Key extends KeyInterface<Config>>(key: Key): Promise<Config[Key]> {
    if (key in this._cache) return this._cache[key] as Config[Key]

    const value = await this.configProvider.get(key)

    return this.appendToCache(key, value)
  }

  async set<Key extends KeyInterface<Config>>(
    key: Key,
    value: Config[Key],
  ): Promise<Config[Key]> {
    const updatedValue = await this.configProvider.set(key, value)

    return this.appendToCache(key, updatedValue)
  }

  async getAll(): Promise<Config> {
    if (!this._cacheIsPartial) return this._cache as Config

    const config = await this.configProvider.getAll()

    return this.overwriteCache(config)
  }

  async setAll(values: Config): Promise<Config> {
    const updatedConfig = await this.configProvider.setAll(values)

    return this.overwriteCache(updatedConfig)
  }

  clear(): AsyncSideEffect {
    this._cacheIsPartial = true
    this._cache = {}

    return this.configProvider.clear()
  }

  private appendToCache<Key extends KeyInterface<Config>>(
    key: Key,
    value: Config[Key],
  ): SideEffect<Config[Key]> {
    this._cache[key] = value

    return value
  }

  private overwriteCache(config: Config): SideEffect<Config> {
    this._cacheIsPartial = false
    this._cache = config

    return config
  }
}

export class ConfigSource implements ConfigSourceInterface {
  private _cache: Record<string, Promise<unknown> | unknown> = {}

  constructor(private readonly configSource: ConfigSourceInterface) {}

  async getProvider<Config extends ConfigInterface = ConfigInterface>(
    name: string,
    defaultConfig: Config | Partial<Config> = {},
  ): Promise<ConfigProvider<Config>> {
    if (!this._cache[name]) {
      this._cache[name] = new Promise<ConfigProvider<Config>>(
        (resolve, reject) => {
          this.configSource.getProvider<Config>(name, defaultConfig)
            .then((provider) => new ConfigProvider(provider))
            .then(resolve)
            .catch(reject)
        },
      )
    }

    return this._cache[name] as Promise<ConfigProvider<Config>>
  }
}
