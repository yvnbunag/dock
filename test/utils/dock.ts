import * as main from '~/main'
import { QUOTES } from '~/config/symbols'
import { DockExit } from '~/errors'
import { trim, pushStringWithGrouping } from '~/lib/transform-factory'

import type { Dependencies, NodeArguments } from '@/main'

class CommanderExit extends Error {}

export class Dock {
  static initialArguments = [
    'dummy-binary',
    'dummy-script',
  ]
  readonly dependencies: Dependencies

  constructor(dependencies: Dependencies = {}) {
    this.dependencies = {
      $createErrorHandler: Dock.createErrorHandler,
      ...dependencies,
    }
  }

  /**
   * Run program passing in arguments and injected dependencies
   *
   * @param args - Program arguments
   */
  async run(args: string): AsyncSideEffect {
    await main.run(
      [...Dock.initialArguments, ...this.createTestProcessArgv(args)],
      this.dependencies,
    )
  }

  protected createTestProcessArgv(args: string): NodeArguments {
    if (!args) return []
    if (args.replace(/\s+/g, ' ') === ' ') return []

    return args
      .replace(/\s$/, '') // Remove last space
      .split(/\s+/) // Split by one or more spaces
      .reduce(pushStringWithGrouping(QUOTES), [])
      .map(trim(QUOTES))
  }

  /**
   * Run program completion, passing in arguments and injected dependencies
   *
   * @param args - Program arguments
   */
  async complete(args: string): AsyncSideEffect {
    const trimmedArgs = args.trimStart()

    await this.run(`--completion "${trimmedArgs}" ${trimmedArgs}`)
  }

  async getVersion(args: string): Promise<string> {
    return this.captureCommanderOutput(`${args} --version`)
  }

  /**
   * Capture commander initiated help output
   *
   * Why not use help methods `outputHelp` and `helpInformation`?
   *  - They would both introduce complexity without adding value/functionality
   *  - `helpInformation` does not include custom help texts
   */
  async getHelp(args: string): Promise<string> {
    return this.captureCommanderOutput(
      `${args} --help`,
      [
        /^error: unknown option '--help'\n$/,
      ],
    )
  }

  /**
   * Capture commander initiated output (direct to stdout and stderr)
   *
   * @param args - Program arguments
   */
  private async captureCommanderOutput(
    command: string,
    exclusions: Array<RegExp> = [],
  ) {
    const spies = {
      exit: jest.spyOn(process, 'exit'),
      outWriter: jest.spyOn(process.stdout, 'write'),
      errorWriter: jest.spyOn(process.stderr, 'write'),
    }
    const messages: Array<string> = []

    // Intended as commander built in output mechanism exits process
    spies.exit.mockImplementation(() => { throw new CommanderExit() })
    // @ts-expect-error Intended to capture help messages
    spies.outWriter.mockImplementation((message) => messages.push(message))
    // @ts-expect-error Intended to capture error messages
    spies.errorWriter.mockImplementation((error) => messages.push(error))

    try {
      await this.run(command)
    } catch (error) {
      if (!(error instanceof CommanderExit)) throw error
    }

    Object.values(spies).forEach((spy) => spy.mockRestore())

    const includedMessages = messages.filter((message) => {
      return !exclusions.some((exclusion) => message.match(exclusion))
    })

    return includedMessages.join('\n').replace(/\n$/g, '')
  }

  private static createErrorHandler() {
    return (error: Error): never => {
      if (error instanceof DockExit) return undefined as never

      throw error
    }
  }
}
