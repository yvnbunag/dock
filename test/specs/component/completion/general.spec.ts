import * as command from '~/commands/completion'

import { createTestInstances, caseFactory } from '#/specs/component/__lib__'

const {
  $environmentReader,
  dock,
  recorder,
} = createTestInstances()

describe('Component - completion [instruction]', () => {
  describe('Command', () => {
    describe('Unsupported platform', () => {
      describe('It should emit unsupported platform error', () => {
        beforeEach(() => {
          jest.clearAllMocks()
          recorder.clearRecording()
        })

        test.each(command.instructionValues)(
          `for '%s' instruction`,
          async (instruction) => {
            $environmentReader.setPlatform('unsupported' as NodeJS.Platform)

            await dock.run(`completion ${instruction}`)

            expect(recorder.recording).toMatchSnapshot()

            $environmentReader.resetPlatform()
          },
        )
      })
    })

    describe('Missing instruction arguments', () => {
      it('should emit missing argument error', async () => {
        jest.clearAllMocks()
        recorder.clearRecording()

        await dock.run(`completion`)

        expect(recorder.recording).toMatchSnapshot()
      })
    })

    describe('Unknown instruction', () => {
      it('should emit unknown instruction error', async () => {
        jest.clearAllMocks()
        recorder.clearRecording()

        await dock.run(`completion unknown-instruction`)

        expect(recorder.recording).toMatchSnapshot()
      })
    })
  })

  describe('Completion', () => {
    beforeEach(() => {
      jest.clearAllMocks()
      recorder.clearRecording()
    })

    it('should complete if ran with no instruction', async () => {
      await dock.complete('completion ')

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should complete if ran with no matching instruction', async () => {
      await dock.complete('completion invalid-instruction')

      expect(recorder.recording).toMatchSnapshot()
    })

    describe('It should complete if ran with partial matching instruction', () => {
      test.each(command.instructionValues)(
        `for partial '%s' instruction match`,
        async (instruction) => {
          await dock.complete(`completion ${instruction.slice(0, -1)}`)

          expect(recorder.recording).toMatchSnapshot()
        },
      )
    })

    describe('It should complete if ran with full matching instruction', () => {
      test.each(command.instructionValues)(
        `for full '%s' instruction match`,
        async (instruction) => {
          await dock.complete(`completion ${instruction}`)

          expect(recorder.recording).toMatchSnapshot()
        },
      )
    })

    it('should complete with system completion for succeeding arguments', async () => {
      const [argument] = command.instructionValues

      await Promise.all([
        dock.complete(`completion ${argument} `),
        dock.complete(`completion ${argument} ${argument}`),
      ])

      expect(recorder.recording).toMatchSnapshot()
    })

    describe(
      'Options are provided before command arguments',
      caseFactory.completion.options.createProvidedBeforeArguments(
        'completion',
        () => ({ dock, recorder }),
        [command.instructionValues[0], './path'],
      ),
    )

    describe(
      'Default options',
      caseFactory.completion.options.createDefault(
        'completion',
        () => ({ dock, recorder }),
        [command.instructionValues[0], './path'],
      ),
    )
  })

  describe(
    'Version',
    caseFactory.version.createDefault('completion', () => ({ dock })),
  )

  describe(
    'Help',
    caseFactory.help.createDefault('completion', () => ({ dock })),
  )
})
