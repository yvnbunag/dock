import { Option } from 'commander'

import type * as commander from 'commander'

export type GlobalFlag = {
  readonly name: string,
  readonly flag: string,
  readonly option: commander.Option,
}

export const COMPLETION = ((): GlobalFlag => {
  const name = 'completion'
  const flag = `--${name}`
  const option = new Option(
    `${flag} [position]`,
    'dock completions entry point',
  )
    .hideHelp()

  return { name, flag, option }
})()
