import { replace } from 'lodash'

import { DockError } from '~/errors/dock-error'
import { exitCodes } from '~/config'


export class ValidationError extends DockError {
  name = 'ValidationError'
  constructor(message?: string, readonly identifiers: Array<string> = []) {
    super(exitCodes.INVALID, message)
  }

  formatMessageIdentifier(format: (identifier: string)=> string): string {
    return [...new Set(this.identifiers)].reduce(
      (identifiedMessage, identifier) => {
        const pattern = new RegExp(identifier, 'g')

        return replace(identifiedMessage, pattern, format(identifier))
      },
      this.message,
    )
  }
}

export class ValidationErrorSet extends ValidationError {
  name = 'ValidationErrorSet'
  constructor(readonly errors: Array<ValidationError>) {
    super()
  }
}
