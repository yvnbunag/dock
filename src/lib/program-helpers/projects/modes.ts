import { pick } from 'lodash'

import { NOT_FOUND } from '~/config/exit-codes'
import { toBulletedSentence } from '~/lib/print-formatter/list'
import { not, hasEntryIn } from '~/lib/predicate-factory'

import type { Provisions } from '@/command'
import type { ModeName, ModeNames, ContainerNames } from '@/config/project'

export function handleNotDefined(
  undefinedModes: ModeNames | ModeName,
  provisions: Provisions,
): SideEffect<never> {
  const normalizedModes = Array.isArray(undefinedModes)
    ? undefinedModes
    : [undefinedModes]
  const modeList = toBulletedSentence(normalizedModes)
  const undefinedModeMessages = {
    NOT_DEFINED: `The following mode/s are not defined:\n${modeList}`,
    ENSURE_DEFINED: 'Make sure that they are defined in the project config',
  }

  return provisions.emitter
    .emitError(undefinedModeMessages.NOT_DEFINED)
    .emitError(undefinedModeMessages.ENSURE_DEFINED)
    .exit(NOT_FOUND)
}

export async function extractUndefined(
  modes: ModeNames,
  provisions: Provisions,
): Promise<ModeNames> | never {
  const definedModes = await getKeys(provisions)
  const undefinedModes = modes.filter(not(hasEntryIn(definedModes)))

  return [...new Set(undefinedModes)]
}

export async function getKeys(
  provisions: Provisions,
): Promise<ModeNames> | never {
  const provider = await provisions.programMediator.getUsedProjectProvider()
  const { modes } = await provider.get('aggregates')

  return modes.values
}

type Map = Record<ModeName, ContainerNames>

export async function getMap(
  provisions: Pick<Provisions, 'programMediator'>,
  names: ModeNames = [],
): Promise<Map> {
  const { modes = {}, services = {} } = await provisions.programMediator
    .getUsedProjectProvider()
    .then((provider) => provider.get('rawConfig'))
  const map: Map = Object.fromEntries(
    Object
      .entries(modes)
      .map(([name, mode]) => {
        const modeContainers = []
        const serviceContainers = (mode.services || [])
          .map((service) => services[service].container)

        modeContainers.push(...(mode.containers || []))
        modeContainers.push(...serviceContainers)

        return [name, [...new Set(modeContainers)]]
      }),
  )

  if (names.length) return pick(map, names)

  return map
}
