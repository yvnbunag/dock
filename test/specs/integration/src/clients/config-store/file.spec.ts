import * as fs from 'fs'

import ConfigStore from 'configstore'

import { file } from '~/clients/config-store'

describe('file', () => {
  function generateNamespace(name: string): string {
    return `dock/test/file-config-store-${name}`
  }

  const { ConfigProvider, ConfigSource } = file

  describe('ConfigProvider', () => {
    function createProvider(scope: string) {
      const configStore = new ConfigStore(generateNamespace(scope))

      return new ConfigProvider(configStore)
    }

    type TestConfigProvider = ReturnType<typeof createProvider>

    describe('Methods', () => {
      describe('get', () => {
        let provider: TestConfigProvider

        beforeAll(() => {
          provider = createProvider('get')
        })

        afterAll(async () => {
          await provider.clear()
        })

        it('should return undefined if config is not set', async () => {
          await expect(provider.get('unset-key')).resolves.toBeUndefined()
        })

        it('should return config value if set', async () => {
          const key = 'get-key'
          const value = 'get-value'

          await provider.set(key, value)

          await expect(provider.get(key)).resolves.toBe(value)
        })
      })

      describe('set', () => {
        const key = 'set-key'
        const value = 'set-value'
        let provider: TestConfigProvider
        let result: ReturnType<Class.Property<TestConfigProvider, 'set'>>

        beforeAll(() => {
          provider = createProvider('set')

          result = provider.set(key, value)
        })

        afterAll(async () => {
          await provider.clear()
        })

        it('should return set value', async () => {
          await expect(result).resolves.toBe(value)
        })

        it('should be stored', async () => {
          await expect(provider.get(key)).resolves.toBe(value)
        })
      })

      describe('getAll', () => {
        it('should return empty object if config is not set', async () => {
          const provider = createProvider('get-all-unset')

          await expect(provider.getAll()).resolves.toStrictEqual({})

          await provider.clear()
        })

        it('should return config value if set', async () => {
          const provider = createProvider('get-all')

          await provider.setAll({
            firstKey: 'get-all-first-value',
            lastKey: 'get-all-last-value',
          })

          await expect(provider.getAll()).resolves.toMatchSnapshot()

          await provider.clear()
        })
      })

      describe('setAll', () => {
        let provider: TestConfigProvider
        let result: ReturnType<Class.Property<TestConfigProvider, 'setAll'>>

        beforeAll(() => {
          provider = createProvider('set-all')

          result = provider.setAll({
            firstKey: 'set-all-first-value',
            lastKey: 'set-all-last-value',
          })
        })

        afterAll(async () => {
          await provider.clear()
        })

        it('should return set value', async () => {
          await expect(result).resolves.toMatchSnapshot()
        })

        it('should be stored', async () => {
          await expect(provider.getAll()).resolves.toMatchSnapshot()
        })
      })

      describe('clear', () => {
        describe('Behavior', () => {
          let store: ConfigStore
          let provider: TestConfigProvider

          beforeAll(async () => {
            store = new ConfigStore(generateNamespace('clear'))
            provider = new ConfigProvider(store)

            await provider.setAll({
              firstKey: 'clear-all-first-value',
              lastKey: 'clear-all-last-value',
            })

            await provider.clear()
          })

          it('should clear store', async () => {
            await expect(provider.getAll()).resolves.toStrictEqual({})
          })

          it('should be removed from file storage', () => {
            expect(fs.existsSync(store.path)).toBeFalsy()
          })
        })

        describe('On error', () => {
          const error = new Error('Config store clear error')
          let store: ConfigStore
          let provider: TestConfigProvider

          beforeAll(async () => {
            store = new ConfigStore(generateNamespace('clear-error'))
            provider = new ConfigProvider(store)

            jest.spyOn(store, 'clear')
              .mockImplementationOnce(() => { throw error })
          })

          test('error should propagate upwards', async () => {
            await expect(provider.clear()).rejects.toBe(error)
          })
        })
      })
    })
  })

  describe('ConfigSource', () => {
    let configSource: file.ConfigSource

    beforeAll(() => {
      configSource = new ConfigSource(generateNamespace('config-source'))
    })

    describe('Methods', () => {
      describe('getProvider', () => {
        it('should return ConfigProvider instance', async () => {
          const provider = await configSource.getProvider('root')

          expect(provider).toBeInstanceOf(ConfigProvider)
        })
      })
    })
  })
})
