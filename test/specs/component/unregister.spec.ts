import { createTestInstances, caseFactory } from '#/specs/component/__lib__'
import { projects } from '#/specs/component/__lib__/test'

const { $configSource, dock, recorder } = createTestInstances()

describe('Component - unregister [project]', () => {
  describe('Command', () => {
    describe('Single project is registered', () => {
      const { location, name } = projects.general

      beforeAll(async () => {
        await dock.run(`register ${location.ABSOLUTE}`)
      })

      it('should unregister successfully', async () => {
        recorder.clearRecording()

        await dock.run(`unregister ${name}`)

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should not be included in registered projects', async () => {
        recorder.clearRecording()

        await dock.run('projects --location-only')

        expect(recorder.recording).toMatchSnapshot()
      })
    })

    describe('Multiple projects are registered', () => {
      const { general, alternative } = projects

      beforeAll(async () => {
        await dock.run(`register ${general.location.ABSOLUTE}`)
        await dock.run(`register ${alternative.location.ABSOLUTE}`)
      })

      test('projects should unregister successfully', async () => {
        recorder.clearRecording()

        await dock.run(`unregister ${general.name}`)
        await dock.run(`unregister ${alternative.name}`)

        expect(recorder.recording).toMatchSnapshot()
      })

      test('unregistered projects should not be included in registered projects', async () => {
        recorder.clearRecording()

        await dock.run('projects --location-only')

        expect(recorder.recording).toMatchSnapshot()
      })
    })

    describe('Project is not registered', () => {
      describe('With no other projects', () => {
        it('should fail un-registration', async () => {
          recorder.clearRecording()

          await dock.run(`unregister ${projects.general.name}`)

          expect(recorder.recording).toMatchSnapshot()
        })
      })

      describe('With other projects', () => {
        beforeAll(async () => {
          await dock.run(`register ${projects.alternative.location.ABSOLUTE}`)
        })
        afterAll($configSource.Class.wipe)

        it('should fail un-registration', async () => {
          recorder.clearRecording()

          await dock.run(`unregister ${projects.general.name}`)

          expect(recorder.recording).toMatchSnapshot()
        })
      })
    })
  })

  describe('Completion', () => {
    describe('Projects are registered', () => {
      beforeAll(async () => {
        await dock.run(`register ${projects.general.location.ABSOLUTE}`)
        await dock.run(`register ${projects.alternative.location.ABSOLUTE}`)
      })
      afterAll($configSource.Class.wipe)

      beforeEach(() => recorder.clearRecording())

      it('should complete if ran with no project', async () => {
        await dock.complete('unregister ')

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should complete if ran with no matching project', async () => {
        await dock.complete('unregister no-match')

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should complete if ran with partial matching project', async () => {
        const partialProject = projects.general.name.slice(0, -1)

        await dock.complete(`unregister ${partialProject}`)

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should complete if ran with full matching project', async () => {
        await dock.complete(`unregister ${projects.general.name}`)

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should complete with system completion for succeeding arguments', async () => {
        const argument = projects.general.name

        await Promise.all([
          dock.complete(`unregister ${argument} `),
          dock.complete(`unregister ${argument} ${argument}`),
        ])

        expect(recorder.recording).toMatchSnapshot()
      })

      describe(
        'Options are provided before command arguments',
        caseFactory.completion.options.createProvidedBeforeArguments(
          'unregister',
          () => ({ dock, recorder }),
          [projects.general.name, './path'],
        ),
      )

      describe(
        'Default options',
        caseFactory.completion.options.createDefault(
          'unregister',
          () => ({ dock, recorder }),
          [projects.general.name, './path'],
        ),
      )
    })

    describe('No projects are registered', () => {
      it('should complete with system completion', async () => {
        recorder.clearRecording()

        await dock.complete('unregister ')

        expect(recorder.recording).toMatchSnapshot()
      })

      describe(
        'Options are provided before command arguments',
        caseFactory.completion.options.createProvidedBeforeArguments(
          'unregister',
          () => ({ dock, recorder }),
          ['./path', './another-path'],
        ),
      )

      describe(
        'Default options',
        caseFactory.completion.options.createDefault(
          'unregister',
          () => ({ dock, recorder }),
          ['./path', './another-path'],
        ),
      )
    })
  })

  describe(
    'Version',
    caseFactory.version.createDefault('unregister', () => ({ dock })),
  )

  describe(
    'Help',
    caseFactory.help.createDefault('unregister', () => ({ dock })),
  )
})
