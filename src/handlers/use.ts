import { programHelpers } from '~/lib'
import { extractCompletionMetadata } from '~/lib/completion'

import type { Provisions } from '@/command'
import type { Handler, CompletionHandler } from '@/handler'
import type { Project } from '@/config'

type Arguments = [Project.Name]

export const handle: Handler<Arguments> = async function (
  [name],
  { provisions },
) {
  const { projects, settings } = await provisions.programMediator.getConfig()
  const { names } = projects

  if (!names.includes(name)) {
    return programHelpers.projects.name
      .handleNotRegistered(name, names, provisions)
  }

  if (settings.active === name) {
    return handleAlreadyUsed(name, provisions)
  }

  return handleUse(name, provisions)
}

function handleAlreadyUsed(
  name: Project.Name,
  provisions: Provisions,
): SideEffect {
  provisions.emitter.emitMessage(`Project '${name}' already activated`)
}

async function handleUse(
  name: Project.Name,
  provisions: Provisions,
): AsyncSideEffect {
  await provisions.programMediator.activateProject(name)

  provisions.emitter.emitMessage(`Project '${name}' activated`)
}

export const complete: CompletionHandler<Arguments> = async function (
  commandArguments,
  { provisions },
): AsyncSideEffect {
  const metadata = extractCompletionMetadata(provisions)

  if (metadata.isOptionCompletion()) {
    return programHelpers.completion.completeOptions(metadata, provisions)
  }

  const { names } = await provisions.programMediator.getProjects()

  return programHelpers.completion
    .positionalComplete(metadata, provisions, [names])
}
