import type { Project } from '@/config'

export interface ProjectMetadata {
  version: Project.ConfigVersion,
  configPath: Project.ConfigPath,
  updated: Project.Updated,
}

export type ProjectAggregates = {
  metadata: Record<Project.Name, ProjectMetadata>,
  names: Array<Project.Name>,
  configPaths: Array<Project.ConfigPath>,
}

export type ProgramSettings = {
  active: Nullable<Project.Name>,
}

export type Config = {
  projects: ProjectAggregates,
  settings: ProgramSettings,
}
