export type EmittedData = Array<unknown>

export type ObserverHandler<
  EmittedDataType extends EmittedData = EmittedData,
> =(...emittedData: EmittedDataType)=> SideEffect

export type Observable<
  Base,
  EmittedDataType extends EmittedData = EmittedData,
>
  = Base & {
  subscribe: (
    handler: ObserverHandler<EmittedDataType>
  )=> SideEffect<Observable<Base, EmittedDataType>>,
  unsubscribe: (
    handler: ObserverHandler<EmittedDataType>
  )=> SideEffect<Observable<Base, EmittedDataType>>,
  emit: (
    ...emittedData: EmittedDataType
  )=> SideEffect<Observable<Base, EmittedDataType>>,
}

export function createObservableMethod<
  MethodType extends Method | jest.MockedFunction<Method>,
  EmittedDataType extends EmittedData = EmittedData,
>(method: MethodType): Observable<MethodType, EmittedDataType> {
  type Handler = ObserverHandler<EmittedDataType>
  type ObservableInjectedMethod = Observable<MethodType, EmittedDataType>

  const handlers: Array<Handler> = []

  return Object.assign(
    method,
    {
      subscribe: (handler: Handler) => {
        handlers.push(handler)

        return method as ObservableInjectedMethod
      },
      unsubscribe: (handler: Handler) => {
        const index = handlers.indexOf(handler)

        if (index > -1) handlers.splice(index, 1)

        return method as ObservableInjectedMethod
      },
      emit: (...emittedData: EmittedDataType) => {
        handlers.forEach((handle) => handle(...emittedData))

        return method as ObservableInjectedMethod
      },
    },
  )
}
