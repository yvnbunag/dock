import { services as servicesHelper } from '~/lib/program-helpers/projects'
import { NOT_FOUND } from '~/config/exit-codes'
import { toBulletedSentence } from '~/lib/print-formatter/list'
import { not, hasEntryIn, hasRecordIn } from '~/lib/predicate-factory'

import type { Provisions } from '@/command'
import type {
  ContainerNames,
  ContainerName,
  ServiceNames,
  References,
  Reference,
} from '@/config/project'

export function handleNotDefined(
  undefinedReferences: References | Reference,
  provisions: Provisions,
): SideEffect<never> {
  const normalizedReferences = Array.isArray(undefinedReferences)
    ? undefinedReferences
    : [undefinedReferences]
  const referenceList = toBulletedSentence(normalizedReferences)
  const undefinedReferenceMessages = {
    NOT_DEFINED: `The following reference/s are not defined:\n${referenceList}`,
    ENSURE_DEFINED:
      'Make sure that they are defined as a container or service in the project config',
  }

  return provisions.emitter
    .emitError(undefinedReferenceMessages.NOT_DEFINED)
    .emitError(undefinedReferenceMessages.ENSURE_DEFINED)
    .exit(NOT_FOUND)
}

export async function extractUndefined(
  references: References,
  provisions: Provisions,
): Promise<References> | never {
  const definedReferences = await getKeys(provisions)
  const undefinedReferences = references.filter(
    not(hasEntryIn(definedReferences)),
  )

  return [...new Set(undefinedReferences)]
}

export async function getKeys(
  provisions: Provisions,
): Promise<References> | never {
  const provider = await provisions.programMediator.getUsedProjectProvider()
  const { references } = await provider.get('aggregates')

  return references.values
}

export async function extractContainers(
  references: References,
  provisions: Provisions,
): Promise<ContainerNames> | never {
  const services = await servicesHelper.get(provisions)
  const containers = references.map((reference) => {
    if (reference in services) return services[reference].container

    return reference
  })

  return [...new Set(containers)]
}

export async function extractContainer(
  reference: Reference,
  provisions: Provisions,
): Promise<ContainerName> | never {
  const services = await servicesHelper.get(provisions)

  if (reference in services) return services[reference].container

  return reference
}

export async function extractServices(
  references: References,
  provisions: Provisions,
): Promise<ServiceNames> | never {
  const services = await servicesHelper.get(provisions)
  const referencedServices = references.filter(hasRecordIn(services))

  return [...new Set(referencedServices)]
}
