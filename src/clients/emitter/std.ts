import { Emitter as AbstractBaseEmitter } from '~/clients/emitter/abstract-base'

interface LoggerProcess {
  stdout: Pick<NodeJS.Process['stdout'], 'write'>,
  stderr: Pick<NodeJS.Process['stderr'], 'write'>,
}

export class Emitter extends AbstractBaseEmitter {
  private readonly _process: LoggerProcess

  constructor(process: LoggerProcess = global.process) {
    super()

    this._process = {
      stdout: { write: (message) => process.stdout.write(`${message}\n`) },
      stderr: { write: (message) => process.stderr.write(`${message}\n`) },
    }
  }

  emitMessage(message: string): SideEffect<Emitter> {
    this._process.stdout.write(AbstractBaseEmitter.formatMessage(message))

    return this
  }

  emitWarning(warning: string): AbstractBaseEmitter {
    this._process.stdout.write(AbstractBaseEmitter.formatWarning(warning))

    return this
  }

  emitError(error: string): SideEffect<Emitter> {
    this._process.stderr.write(AbstractBaseEmitter.formatError(error))

    return this
  }
}
