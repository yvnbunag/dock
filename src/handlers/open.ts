import { programHelpers } from '~/lib'
import { extractCompletionMetadata } from '~/lib/completion'

import type { Provisions } from '@/command'
import type { Handler, CompletionHandler } from '@/handler'
import type { ContainerName, Reference, Service } from '@/config/project'

type Arguments = [Reference]

export const handle: Handler<Arguments> = async function (
  [reference],
  { provisions },
) {
  const references = await programHelpers.projects.references
    .getKeys(provisions)

  if (!references.includes(reference)) {
    return programHelpers.projects.references
      .handleNotDefined(reference, provisions)
  }

  return handleOpen(reference, provisions)
}

async function handleOpen(
  reference: Reference,
  provisions: Provisions,
): AsyncSideEffect | never {
  const [
    { configPath, rawConfig },
    container,
  ] = await Promise.all([
    provisions.programMediator.getUsedProjectProvider()
      .then((provider) => provider.getAll()),
    programHelpers.projects.references
      .extractContainer(reference, provisions),
  ])
  const { containers, services = {} } = rawConfig
  const { shell } = containers?.[container] || {}
  const service = services[reference]
  const entryPoint = service?.['entry-point']

  handleOpenMessage(reference, container, service, provisions)
  await provisions.orchestrationEngineFactory
    .create(configPath, rawConfig.engine)
    .open(container, { shell, entryPoint })
}

function handleOpenMessage(
  reference: Reference,
  container: ContainerName,
  service: Service | undefined,
  provisions: Provisions,
): SideEffect {
  const message = (() => {
    if (!service) return `Opening ${container} container`

    const serviceMessage = ['Opening', reference, 'in', container, 'container']
    const entryPoint = service?.['entry-point']

    if (entryPoint) serviceMessage.push('with', 'entry-point', entryPoint)

    return serviceMessage.join(' ')
  })()

  provisions.emitter.emitMessage(message)
}

export const complete: CompletionHandler<Arguments> = async function (
  commandArguments,
  { provisions },
) {
  const metadata = extractCompletionMetadata(provisions)

  if (metadata.isOptionCompletion()) {
    return programHelpers.completion.completeOptions(metadata, provisions)
  }

  const references = await programHelpers.projects.references
    .getKeys(provisions)

  return programHelpers.completion
    .positionalComplete(metadata, provisions, [references])
}
