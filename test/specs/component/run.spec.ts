import { createTestInstances, caseFactory } from '#/specs/component/__lib__'
import { projects } from '#/specs/component/__lib__/test'
import { Engine } from '@/config/project'

import type { Project } from '#/specs/component/__lib__/test/projects'
import type { Dock } from '#/utils/dock'
import type { Recorder } from '#/utils/recorder'

describe('Component - run [reference] [command...]', () => {
  describe('Engine', () => {
    const projectsMap: Record<
      Engine,
      Parameters<typeof createEngineSuite>[1]
    > = {
      [Engine.DOCKER_COMPOSE]: {
        WITH_REFERENCES: projects['run/engine/docker-compose/with-references'],
        NO_REFERENCE: projects['run/engine/docker-compose/no-reference'],
      },
    }

    describe(
      ...createEngineSuite(
        Engine.DOCKER_COMPOSE,
        projectsMap[Engine.DOCKER_COMPOSE],
      ),
    )
  })

  describe('Completion', () => {
    describe('Project is used', () => {
      describe('Project has references', () => {
        let dock: Dock
        let recorder: Recorder

        beforeAll(async () => {
          const instances = await createTestInstances(
            'completion-with-references',
            projects['run/with-references'],
          )

          dock = instances.dock
          recorder = instances.recorder
        })
        beforeEach(() => { recorder.clearRecording() })

        it('should complete if ran with no arguments', async () => {
          await dock.complete('run ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete if ran with partial reference', async () => {
          await dock.complete('run container-with')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete if ran with full reference', async () => {
          await dock.complete('run container-with-service')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete with system completion for run command arguments', async () => {
          await dock.complete('run container-with-service ')
          await dock.complete('run container-with-service yarn')
          await dock.complete('run container-with-service yarn dev')

          expect(recorder.recording).toMatchSnapshot()
        })

        describe(
          'Options are provided before command arguments',
          caseFactory.completion.options.createProvidedBeforeArguments(
            'run',
            () => ({ dock, recorder }),
            ['service', 'yarn', 'dev'],
          ),
        )

        describe(
          'Default options',
          caseFactory.completion.options.createDefault(
            'run',
            () => ({ dock, recorder }),
            ['service', 'yarn', 'dev'],
          ),
        )
      })

      describe('Project has no reference', () => {
        let dock: Dock
        let recorder: Recorder

        beforeAll(async () => {
          const instances = await createTestInstances(
            'completion-no-reference',
            projects['run/no-reference'],
          )

          dock = instances.dock
          recorder = instances.recorder
        })
        beforeEach(() => { recorder.clearRecording() })

        it('should complete with system completion if ran with no arguments', async () => {
          await dock.complete('run ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete with system completion if ran with arguments', async () => {
          await dock.complete('run ./path ')

          expect(recorder.recording).toMatchSnapshot()
        })

        describe(
          'Options are provided before command arguments',
          caseFactory.completion.options.createProvidedBeforeArguments(
            'run',
            () => ({ dock, recorder }),
            ['service', './path', './another-path'],
          ),
        )

        describe(
          'Default options',
          caseFactory.completion.options.createDefault(
            'run',
            () => ({ dock, recorder }),
            ['service', './path', './another-path'],
          ),
        )
      })
    })

    describe('Project is not used', () => {
      const namespace = 'completion-no-project-used'
      const { dock, recorder } = createTestInstances(namespace)

      beforeEach(() => { recorder.clearRecording() })

      it('should complete with system completion if ran with no arguments', async () => {
        await dock.complete('run ')

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should complete with system completion if ran with arguments', async () => {
        await dock.complete('run ./path ')

        expect(recorder.recording).toMatchSnapshot()
      })

      describe(
        'Options are provided before command arguments',
        caseFactory.completion.options.createProvidedBeforeArguments(
          'run',
          () => ({ dock, recorder }),
          ['service', './path', './another-path'],
        ),
      )

      describe(
        'Default options',
        caseFactory.completion.options.createDefault(
          'run',
          () => ({ dock, recorder }),
          ['service', './path', './another-path'],
        ),
      )
    })
  })

  describe(
    'Auto reload',
    caseFactory.autoReload.createIntercepted(
      {
        GENERAL: { project: projects['run/with-references'] },
        VALID_CHANGE: { project: projects['run/updated-valid'] },
        INVALID_CHANGE: { project: projects['run/updated-invalid'] },
        REMOVED: { project: projects['run/removed'] },
      },
      {
        runValid: ({ dock }) => dock.run('run service yarn dev'),
        runInvalid: ({ dock }) => dock.run('run undefined yarn dev'),
      },
    ),
  )

  describe(
    'Version',
    caseFactory.version.createDefault('run', createTestInstances),
  )

  describe(
    'Help',
    caseFactory.help.createDefault('run', createTestInstances),
  )
})

function createEngineSuite(
  engine: Engine,
  projectsMap: {
    WITH_REFERENCES: Project,
    NO_REFERENCE: Project,
  },
): [Engine, Method] {
  function createNameSpace(name: string) { return `${engine}-${name}`}

  function suite() {
    describe('Command', () => {
      describe('Project is used', () => {
        describe('Project has references', () => {
          let dock: Dock
          let recorder: Recorder

          beforeAll(async () => {
            const namespace = createNameSpace('with-references')
            const instances = await createTestInstances(
              namespace,
              projectsMap.WITH_REFERENCES,
            )

            dock = instances.dock
            recorder = instances.recorder
          })
          beforeEach(() => recorder.clearRecording())

          describe('Reference with valid command cases', () => {
            const cases = [
              [
                'Basic container',
                'container-no-service',
              ],
              [
                'Container with shell',
                'container-with-shell',
              ],
              [
                'Basic service',
                'service',
              ],
              [
                'Service with entry point',
                'service-entry-point',
              ],
              [
                'Service with shell',
                'service-with-shell',
              ],
              [
                'Service with same reference as its container',
                'container-same-service',
              ],
              [
                'Service with same reference as its container and entry point',
                'container-same-service-with-entry-point',
              ],
              [
                'Service with same reference as its container and shell',
                'container-same-service-with-shell',
              ],
              [
                'Service with same reference as its container, entry point and shell',
                'container-same-service-with-entry-point-and-shell',
              ],
            ]

            describe.each(cases)('%s', (description, reference) => {
              it('should run command', async () => {
                await dock.run(`run ${reference} yarn`)

                expect(recorder.recording).toMatchSnapshot()
              })

              it('should run multiple commands', async () => {
                await dock.run(`run ${reference} yarn dev`)

                expect(recorder.recording).toMatchSnapshot()
              })

              it('should run command with options', async () => {
                await dock.run(`run ${reference} yarn dev --verbose`)

                expect(recorder.recording).toMatchSnapshot()
              })

              it('should run multiple commands with options in between', async () => {
                await dock.run(
                  `run ${reference} yarn --production install --verbose`,
                )

                expect(recorder.recording).toMatchSnapshot()
              })

              it('should run command with single quoted arguments', async () => {
                await dock.run(`run ${reference} 'yarn'`)

                expect(recorder.recording).toMatchSnapshot()
              })

              it('should run multiple commands with single quoted arguments', async () => {
                await dock.run(`run ${reference} yarn 'spaced script'`)

                expect(recorder.recording).toMatchSnapshot()
              })

              it('should run command with double quoted arguments', async () => {
                await dock.run(`run ${reference} "yarn"`)

                expect(recorder.recording).toMatchSnapshot()
              })

              it('should run multiple commands with double quoted arguments', async () => {
                await dock.run(`run ${reference} yarn "spaced script"`)

                expect(recorder.recording).toMatchSnapshot()
              })

              it('should run multiple commands with varying quoted arguments', async () => {
                await dock.run(`run ${reference} 'yarn' "spaced script"`)

                expect(recorder.recording).toMatchSnapshot()
              })

              it('should run multiple commands with multiple options and varying quoted arguments', async () => {
                await dock.run(
                  `run ${reference} 'yarn' --production "custom install" --verbose`,
                )

                expect(recorder.recording).toMatchSnapshot()
              })
            })
          })

          it('should emit error if ran with no arguments', async () => {
            await dock.run('run')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should emit error if ran with reference argument only', async () => {
            await dock.run('run service')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should emit error if ran with unknown reference only', async () => {
            await dock.run('run unknown-reference')

            expect(recorder.recording).toMatchSnapshot()
          })


          it('should emit error if ran with unknown reference and commands', async () => {
            await dock.run('run unknown-reference yarn dev')

            expect(recorder.recording).toMatchSnapshot()
          })
        })

        describe('Project has no references', () => {
          let dock: Dock
          let recorder: Recorder

          beforeAll(async () => {
            const namespace = createNameSpace('no-reference')
            const instances = await createTestInstances(
              namespace,
              projectsMap.NO_REFERENCE,
            )

            dock = instances.dock
            recorder = instances.recorder
          })
          beforeEach(() => recorder.clearRecording())

          it('should emit error if ran with no arguments', async () => {
            await dock.run('run')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should emit error if ran with reference argument', async () => {
            await dock.run('run service')

            expect(recorder.recording).toMatchSnapshot()
          })

          it('should emit error if ran with reference argument and commands', async () => {
            await dock.run('run service yarn dev')

            expect(recorder.recording).toMatchSnapshot()
          })
        })
      })

      describe('Project is not used', () => {
        const namespace = createNameSpace('no-project-used')
        const { dock, recorder } = createTestInstances(namespace)
        const location = projectsMap.WITH_REFERENCES.location.ABSOLUTE

        beforeAll(async () => await dock.run(`register ${location}`))
        beforeEach(() => recorder.clearRecording())

        it('should emit error if ran with no arguments', async () => {
          await dock.run('run')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should emit error if ran with reference argument', async () => {
          await dock.run('run service')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should emit error if ran with reference argument and commands', async () => {
          await dock.run('run service yarn dev')

          expect(recorder.recording).toMatchSnapshot()
        })
      })
    })
  }

  return [engine, suite]
}
