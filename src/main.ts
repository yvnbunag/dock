import chalk from 'chalk'

import { bin } from '+/package.json'

import * as commands from '~/commands'
import { Emitter } from '~/clients/emitter/std'
import { ConfigSource } from '~/clients/config-store/file'
import * as Cached from '~/clients/config-store/cached'
import { OrchestrationEngineFactory } from '~/clients/orchestration-engine'
import { executeCommand } from '~/lib'
import { OptionMediator } from '~/lib/option-mediator'
import { environmentReader } from '~/lib/environment-reader'
import { ProgramMediator } from '~/lib/program-mediator'
import { DockError } from '~/errors'
import { exitCodes } from '~/config'

import type { Dependencies, NodeArguments } from '@/main'
import type { Emitter as EmitterInterface } from '@/clients'

/**
  * Main program function
  *
  * @param nodeArguments - Program arguments
  */
export async function run(
  nodeArguments: NodeArguments,
  {
    $emitter = new Emitter(),
    $environmentReader = environmentReader,
    $configSource,
    $executeCommand = executeCommand,
    $createErrorHandler = createErrorHandler,
  }: Dependencies = {},
): AsyncSideEffect {
  const configSource = $configSource
    ? $configSource
    : new ConfigSource('dock')
  const cachedConfigSource = new Cached.ConfigSource(configSource)
  const orchestrationEngineFactory = new OrchestrationEngineFactory(
    $executeCommand,
  )
  const provisions = {
    emitter: $emitter ,
    environmentReader: $environmentReader,
    optionMediator: new OptionMediator(),
    programMediator: new ProgramMediator(cachedConfigSource),
    orchestrationEngineFactory,
    nodeArguments,
  }
  const command = commands
    .create(provisions)
    .name(Object.keys(bin)[0])

  await command
    .parseAsync([...nodeArguments])
    .catch((error) => $createErrorHandler($emitter)(error))
}

export function createErrorHandler(
  emitter: EmitterInterface,
  nodeProcess: Pick<NodeJS.Process, 'exitCode'> = process,
): (error: Error)=> SideEffect | never {
  return (error) => {
    if (error instanceof DockError) {
      const { message, code } = error
      if (message) emitter.emitError(`${error.name}: ${error.message}`)

      nodeProcess.exitCode = code

      if (nodeProcess.exitCode !== exitCodes.SUCCESS) {
        emitter.emitError(`Command failed with exit code ${chalk.bold(code)}`)
      }

      return
    }

    throw error
  }
}
