import * as commander from 'commander'

import { createErrorHandler } from '~/main'
import { DockError } from '~/errors/dock-error'

import { Dock } from '#/utils/dock'
import { Recorder } from '#/utils/recorder'
import { emitter } from '#/utils/mocks/clients'

const parserSpy = jest.spyOn(commander.Command.prototype, 'parseAsync')

/**
 * Disable remapping of exit code keys to their values
 *  - See test/setup/setup-files-after-env.ts
 */
jest.unmock('~/config/exit-codes')

function createTestInstances() {
  const nodeProcess = { exitCode: 0 }
  const recorder = new Recorder()
  const $emitter = emitter.create()
    .subscribe(emitter.createRecorderHandler(recorder))
  const $createErrorHandler: typeof createErrorHandler = (
    emitterArgument = $emitter,
    nodeProcessArgument = nodeProcess,
  ) => {
    return createErrorHandler(emitterArgument, nodeProcessArgument)
  }
  const dock = new Dock({ $emitter, $createErrorHandler })

  return {
    dock,
    recorder,
    nodeProcess,
  }
}

function runWithError(dock: Dock, error: Error) {
  parserSpy.mockRejectedValueOnce(error)

  return dock.run('')
}

type TestInstances = ReturnType<typeof createTestInstances>

afterAll(jest.restoreAllMocks)

describe('Component - [command] unhandled error handling', () => {
  describe('Error is instance of DockError', () => {
    let dock: Dock
    let recorder: Recorder
    let nodeProcess: TestInstances['nodeProcess']

    beforeAll(async () => {
      jest.clearAllMocks()

      const instances = createTestInstances()

      dock = instances.dock
      recorder = instances.recorder
      nodeProcess = instances.nodeProcess
    })
    beforeEach(() => recorder.clearRecording())

    it('should not emit message if error has no message', async () => {
      const error = new DockError(0)

      await runWithError(dock, error)

      expect(recorder.recording).toBe('')
    })

    it('should emit message if error has message', async () => {
      const error = new DockError(0, 'dock-error-message')

      await runWithError(dock, error)

      expect(recorder.recording).toMatchSnapshot()
    })

    it('should exit with code if error has non successful exit code', async () => {
      const error = new DockError(1)

      await runWithError(dock, error)

      expect(recorder.recording).toMatchSnapshot()
      expect(nodeProcess.exitCode).toMatchInlineSnapshot(`1`)
    })

    it('should exit with default code if error has successful exit code', async () => {
      const error = new DockError(0)

      await runWithError(dock, error)

      expect(recorder.recording).toBe('')
      expect(nodeProcess.exitCode).toMatchInlineSnapshot(`0`)
    })
  })

  describe('Error is not instance of DockError', () => {
    const { dock, recorder, nodeProcess } = createTestInstances()
    const error = new Error('generic-error')
    let result: unknown

    beforeAll(() => {
      result = runWithError(dock, error)
    })

    it('should not emit error message', () => {
      expect(recorder.recording).toBe('')
    })

    it('should not modify exit code', () => {
      expect(nodeProcess.exitCode).toMatchInlineSnapshot(`0`)
    })

    it('should re-throw error', async () => {
      await expect(result).rejects.toThrow(error)
    })
  })
})
