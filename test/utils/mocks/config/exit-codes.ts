import type { exitCodes as exitCodesType } from '~/config'

type Keys = keyof typeof exitCodesType
type MockExitCodes = Record<Keys, Keys>

export function create(): MockExitCodes {
  const exitCodes = jest.requireActual('~/config/exit-codes')
  const exitKeys = Object.keys(exitCodes) as Array<Keys>
  const entries = exitKeys.map((exitKey) => [exitKey, exitKey] as const)

  return Object.fromEntries(entries) as MockExitCodes
}
