require('ts-node/register')
require('tsconfig-paths/register')

const path = require('path')
const TerserPlugin = require('terser-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const nodeExternals = require('webpack-node-externals')

const build = require('+/build')

const alias = (() => {
  const tsconfig = require('+/tsconfig.json')

  return Object.entries(tsconfig.compilerOptions.paths).reduce(
    (accumulatedAliases, [key, [firstPath]]) => {
      const extractedAlias = /^(?<alias>.*)\/\*$/.exec(key)?.groups?.alias
      const relativePath = /^\.\/(?<path>.*)\/\*$/.exec(firstPath)?.groups?.path
      const extractedPath = relativePath
        ? path.resolve(__dirname, relativePath)
        : path.resolve(__dirname)

      return {
        ...accumulatedAliases,
        [extractedAlias]: extractedPath,
      }
    },
    {},
  )
})()

module.exports = {
  target: 'node',
  mode: 'production',
  entry: './src/index.ts',
  module: {
    rules: [
      {
        test: /\.ts?$/,
        use: [
          { loader: 'shebang-loader' },
          { loader: 'ts-loader' },
        ],
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    ...build.plugins,
    new CleanWebpackPlugin(),
  ],
  resolve: {
    extensions: ['.ts', '.js'],
    alias,
  },
  optimization: {
    minimize: true,
    minimizer: [ new TerserPlugin({ parallel: true })],
  },
  externals: [nodeExternals()],
  output: {
    filename: build.filename,
    path: build.path,
  },
}
