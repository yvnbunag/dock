import { memory } from '~/clients/config-store'

import type { ConfigProvider } from '@/clients/config-store'

describe('memory', () => {
  function createConfigSource(alternateNamespace?: string) {
    const suffix = alternateNamespace ? `-${alternateNamespace}` : ''
    const namespace = `test-memory-config-source${suffix}`

    return new memory.ConfigSource(namespace)
  }

  describe('ConfigProvider', () => {
    describe('Methods', () => {
      describe('get', () => {
        let provider: ConfigProvider

        beforeAll(async () => {
          provider = await createConfigSource().getProvider('get')
        })

        afterAll(async () => {
          await provider.clear()
        })

        it('should return undefined if config is not set', async () => {
          await expect(provider.get('unset-key')).resolves.toBeUndefined()
        })

        it('should return config value if set', async () => {
          const key = 'get-key'
          const value = 'get-value'

          await provider.set(key, value)

          await expect(provider.get(key)).resolves.toBe(value)
        })
      })

      describe('set', () => {
        const key = 'set-key'
        const value = 'set-value'
        let provider: ConfigProvider
        let result: ReturnType<Class.Property<ConfigProvider, 'set'>>

        beforeAll(async () => {
          provider = await createConfigSource().getProvider('set')

          result = provider.set(key, value)
        })

        afterAll(async () => {
          await provider.clear()
        })

        it('should return set value', async () => {
          await expect(result).resolves.toBe(value)
        })

        it('should be stored', async () => {
          await expect(provider.get(key)).resolves.toBe(value)
        })
      })

      describe('getAll', () => {
        const source = createConfigSource()

        it('should return empty object if config is not set', async () => {
          const provider = await source.getProvider('get-all-unset')

          await expect(provider.getAll()).resolves.toStrictEqual({})

          await provider.clear()
        })

        it('should return config value if set', async () => {
          const provider = await source.getProvider('get-all')

          await provider.setAll({
            firstKey: 'get-all-first-value',
            lastKey: 'get-all-last-value',
          })

          await expect(provider.getAll()).resolves.toMatchSnapshot()

          await provider.clear()
        })
      })

      describe('setAll', () => {
        let provider: ConfigProvider
        let result: ReturnType<Class.Property<ConfigProvider, 'setAll'>>

        beforeAll(async () => {
          provider = await createConfigSource().getProvider('set-all')

          result = provider.setAll({
            firstKey: 'set-all-first-value',
            lastKey: 'set-all-last-value',
          })
        })

        afterAll(async () => {
          await provider.clear()
        })

        it('should return set value', async () => {
          await expect(result).resolves.toMatchSnapshot()
        })

        it('should be stored', async () => {
          await expect(provider.getAll()).resolves.toMatchSnapshot()
        })
      })

      describe('clear', () => {
        const name = 'clear'
        const source = createConfigSource()
        let provider: ConfigProvider

        beforeAll(async () => {
          provider = await source.getProvider(name)

          await provider.setAll({
            firstKey: 'clear-all-first-value',
            lastKey: 'clear-all-last-value',
          })
          await provider.clear()
        })

        it('should clear store', async () => {
          await expect(provider.getAll()).resolves.toStrictEqual({})
        })

        it('should be removed from memory storage', () => {
          const identifier = source.generateProviderIdentifier(name)

          expect(memory.ConfigSource.hasConfig(identifier)).toBeFalsy()
        })
      })
    })

    describe('Cross instance access', () => {
      describe('Same source', () => {
        const source = createConfigSource()

        it('should have a single data source when invoking set and get from separate instances', async () => {
          const name = 'set-and-get'
          const setInstance = await source.getProvider(name)
          const getInstance = await source.getProvider(name)
          const key = `${name}-key`
          const value = `${name}-value`

          await setInstance.set(key, value)

          await expect(getInstance.get(key)).resolves.toEqual(value)
        })

        it('should have a single data source when invoking setAll and getAll from separate instances', async () => {
          const name = 'set-and-get-all'
          const setInstance = await source.getProvider(name)
          const getInstance = await source.getProvider(name)
          const key = `${name}-key`
          const value = `${name}-value`
          const config = { [key]: value }

          await setInstance.setAll(config)

          await expect(getInstance.getAll()).resolves.toEqual(config)
        })

        it('should have a single data source when initializing separate instances', async () => {
          const name = 'initialize-and-get-all'
          const key = `${name}-key`
          const value = `${name}-value`
          const config = { [key]: value }

          await source.getProvider(name, config)

          const getInstance = await source.getProvider(name)

          await expect(getInstance.getAll()).resolves.toEqual(config)
        })
      })

      describe('Different sources', () => {
        const source = createConfigSource()
        const alternateSource = createConfigSource('alternate')

        it('should not have a single data source when invoking set and get from separate instances', async () => {
          const name = 'set-and-get'
          const instance = await source.getProvider(name)
          const alternateInstance = await alternateSource.getProvider(name)
          const key = `${name}-key`
          const value = `${name}-value`

          await instance.set(key, value)

          await expect(instance.get(key)).resolves.toEqual(value)
          await expect(alternateInstance.get(key)).resolves.not.toEqual(value)
        })

        it('should not have a single data source when invoking setAll and getAll from separate instances', async () => {
          const name = 'set-and-get-all'
          const instance = await source.getProvider(name)
          const alternateInstance = await alternateSource.getProvider(name)
          const key = `${name}-key`
          const value = `${name}-value`
          const config = { [key]: value }

          await instance.setAll(config)

          await expect(instance.getAll()).resolves.toEqual(config)
          await expect(alternateInstance.getAll()).resolves.not.toEqual(config)
        })

        it('should not have a single data source when initializing separate instances', async () => {
          const name = 'initialize-and-get-all'
          const key = `${name}-key`
          const value = `${name}-value`
          const config = { [key]: value }

          const instance = await source.getProvider(name, config)
          const alternateInstance = await alternateSource.getProvider(name)

          await expect(instance.getAll()).resolves.toEqual(config)
          await expect(alternateInstance.getAll()).resolves.not.toEqual(config)
        })
      })
    })
  })

  describe('ConfigSource', () => {
    describe('Methods', () => {
      describe('getProvider', () => {
        it('should return ConfigProvider instance', async () => {
          const provider = await createConfigSource().getProvider('root')

          expect(Object.keys(provider)).toMatchSnapshot()
        })
      })

      describe('generateProviderIdentifier', () => {
        it('should generate provider identifier', () => {
          const provider = createConfigSource().generateProviderIdentifier(
            'root',
          )

          expect(provider).toMatchInlineSnapshot(
            `"test-memory-config-source-root"`,
          )
        })
      })
    })

    describe('Static methods', () => {
      const { ConfigSource } = memory

      describe('hasConfig', () => {
        it('should return true if config exists', async () => {
          const source = createConfigSource()
          const identifier = source.generateProviderIdentifier('root')

          await source.getProvider('root')

          expect(ConfigSource.hasConfig(identifier)).toBe(true)
        })

        it('should return false if config does not exist', async() => {
          const source = createConfigSource()
          const identifier = source.generateProviderIdentifier('missing')

          expect(ConfigSource.hasConfig(identifier)).toBe(false)
        })
      })

      describe('wipe', () => {
        const source = createConfigSource()
        const identifier = source.generateProviderIdentifier('root')

        beforeAll(async () => {
          await source.getProvider('root')

          ConfigSource.wipe()
        })

        it('should wipe config', () => {
          expect(ConfigSource.hasConfig(identifier)).toBe(false)
        })
      })
    })
  })
})
