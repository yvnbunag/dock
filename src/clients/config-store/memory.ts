import type {
  ConfigProvider as ConfigProviderInterface,
  ConfigSource as ConfigSourceInterface,
  Config as ConfigInterface,
  Key as KeyInterface,
} from '@/clients/config-store'

function createConfigProvider<Config extends ConfigInterface = ConfigInterface>(
  store: Record<string, Config>,
  identifier: string,
): ConfigProviderInterface<Config> {
  return {
    async get<Key extends KeyInterface<Config>>(
      key: Key,
    ): Promise<Config[Key]> {
      return store[identifier][key] as Config[Key]
    },
    async set<Key extends KeyInterface<Config>>(
      key: Key,
      value: Config[Key],
    ): Promise<Config[Key]> {
      store[identifier][key] = value

      return value
    },
    async getAll(): Promise<Config> {
      const config = store[identifier] || {}

      return config as Config
    },
    async setAll(values: Config): Promise<Config> {
      store[identifier] = { ...values }

      return values
    },
    async clear(): AsyncSideEffect {
      delete store[identifier]
    },
  }
}
export class ConfigSource implements ConfigSourceInterface {
  private static _store: Record<string, ConfigInterface> = {}

  constructor(private readonly namespace: string) {}

  async getProvider<Config extends ConfigInterface = ConfigInterface>(
    name: string,
    defaultConfig: Config | Partial<Config> = {},
  ): Promise<ConfigProviderInterface<Config>> {
    const identifier = this.generateProviderIdentifier(name)

    ConfigSource.initializeConfig(identifier, { ...defaultConfig })

    return createConfigProvider<Config>(
      ConfigSource._store as Record<string, Config>,
      identifier,
    )
  }

  generateProviderIdentifier(name: string): string {
    return `${this.namespace}-${name}`
  }

  Class = ConfigSource

  static hasConfig(identifier: string): boolean {
    return Boolean(ConfigSource._store[identifier])
  }

  static wipe(): SideEffect {
    ConfigSource._store = {}
  }

  private static initializeConfig(
    identifier: string,
    value: ConfigInterface,
  ): SideEffect {
    if (!ConfigSource.hasConfig(identifier)) {
      ConfigSource._store[identifier] = value
    }
  }
}
