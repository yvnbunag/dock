###-begin-dock-completion-###
# Completion strategy inspired by https://www.npmjs.com/package/yargs
_dock_completion()
{
    local current_word arguments type position metadata completions

    # Current word to complete
    current_word="${COMP_WORDS[COMP_CWORD]}"

    # All arguments
    #   - First argument removed (script name)
    #   - Including whitespace at the end
    arguments="${COMP_WORDS[@]:1}"

    # Get completion options from dock
    completions=`dock --completion "$arguments" $arguments`

    COMPREPLY=( $(compgen -W "${completions}" -- ${current_word}) )

    # If no match is found, fallback to system's default completion
    # To trigger fallback, exit with code 0 with no message
    if [ ${#COMPREPLY[@]} -eq 0 ]; then unset COMPREPLY; fi

    return 0
}

complete -o default -F _dock_completion dock
###-end-dock-completion-###
