import { Emitter } from '~/clients/emitter/std'

import { getFirstArguments } from '#/utils/mock-helpers'

function createLoggerProcessMock() {
  return {
    stdout: { write: jest.fn() },
    stderr: { write: jest.fn() },
  }
}

describe('Console emitter', () => {
  describe('Methods', () => {
    describe('emitMessage', () => {
      const loggerProcessMock = createLoggerProcessMock()
      const emitter = new Emitter(loggerProcessMock)

      beforeAll(() => emitter.emitMessage('Emitted message'))

      it('should emit message', () => {
        const { write } = loggerProcessMock.stdout

        expect(write).toHaveBeenCalledTimes(1)
        expect(getFirstArguments(write)[0]).toMatchSnapshot()
      })
    })

    describe('emitWarning', () => {
      const loggerProcessMock = createLoggerProcessMock()
      const emitter = new Emitter(loggerProcessMock)

      beforeAll(() => emitter.emitWarning('Emitted warning'))

      it('should emit warning', () => {
        const { write } = loggerProcessMock.stdout

        expect(write).toHaveBeenCalledTimes(1)
        expect(getFirstArguments(write)[0]).toMatchSnapshot()
      })
    })

    describe('emitError', () => {
      const loggerProcessMock = createLoggerProcessMock()
      const emitter = new Emitter(loggerProcessMock)

      beforeAll(() => emitter.emitError('Emitted Error'))

      it('should emit error', () => {
        const { write } = loggerProcessMock.stderr

        expect(write).toHaveBeenCalledTimes(1)
        expect(getFirstArguments(write)[0]).toMatchSnapshot()
      })
    })
  })
})
