export interface ContextSwitch {
  scope: ()=> SideEffect,
  switch: ()=> SideEffect,
  reset: ()=> SideEffect,
}
export function createContextSwitch(to: string): ContextSwitch {
  const previousContext = process.cwd()

  return {
    scope: () => {
      const scopeContext = process.cwd()

      beforeAll(() => {
        process.chdir(to)
      })

      afterAll(() => {
        process.chdir(scopeContext)
      })
    },
    switch: () => { process.chdir(to) },
    reset: () => { process.chdir(previousContext) },
  }
}

export function mapContextSwitch<
  DestinationMap extends Record<string, string>,
  ContextSwitchMap = Record<keyof DestinationMap, ContextSwitch>,
>(destinationMap: DestinationMap): ContextSwitchMap {
  const contextSwitchList: Array<[keyof DestinationMap, ContextSwitch]> =
    Object.entries(destinationMap)
      .map(([key, to]) => [key, createContextSwitch(to)])
  const contextSwitchMap: ContextSwitchMap = contextSwitchList.reduce(
    (accumulatedMap, [key, contextSwitch]) => {
      return {
        ...accumulatedMap,
        [key]: contextSwitch,
      }
    },
    {} as ContextSwitchMap,
  )

  return contextSwitchMap
}
