import { exitCodes } from '~/config'

export class DockError extends Error {
  name = 'DockError'
  constructor(
    readonly code: number = exitCodes.FAILED,
    message?: string,
  ) {
    super(message)
  }
}
