import * as commander from 'commander'

import { unregister } from '~/handlers'
import { createActionHandler, decorateWithDefaults } from '~/commands'
import { requireArguments } from '~/middlewares'

import type { Create } from '@/command'

export const name = 'unregister'

const nameAndArgs = `${name} [project]`
const description = 'Unregister project'
const argumentDescriptions = { project: `Project name` }

export const create: Create = (provisions) => {
  const handler = createActionHandler(
    unregister.handle,
    unregister.complete,
    provisions,
    [requireArguments(nameAndArgs)],
  )
  const command = new commander.Command()
    .command(nameAndArgs)
    .description(description, argumentDescriptions)
    .action(handler)

  return decorateWithDefaults(command, provisions)
}
