import { version } from '+/package.json'

import type {
  GetTestInstances,
  TestInstances,
} from '#/specs/component/__lib__/case-factory'

export function createDefault(
  subCommand: string,
  getTestInstances: GetTestInstances<'dock'>,
): Method {
  return () => {
    let dock: TestInstances['dock']

    beforeAll(async () => {
      dock = (await getTestInstances()).dock
    })

    it('should return version', async () => {
      await expect(dock.getVersion(subCommand)).resolves.toBe(version)
    })
  }
}
