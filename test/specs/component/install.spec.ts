import { createTestInstances, caseFactory } from '#/specs/component/__lib__'
import { projects } from '#/specs/component/__lib__/test'
import { mockHrtime } from '#/utils/mocks/process'
import { Engine } from '@/config/project'

import type { Project } from '#/specs/component/__lib__/test/projects'
import type { Dock } from '#/utils/dock'
import type { Recorder } from '#/utils/recorder'

mockHrtime()

describe('Component - install [service...]', () => {
  describe('Engine', () => {
    const projectsMap: Record<
      Engine,
      Parameters<typeof createEngineSuite>[1]
    > = {
      [Engine.DOCKER_COMPOSE]: {
        WITH_SERVICE: {
          WITH_ALL_INSTALL: projects['install/engine/docker-compose/with-service/with-all-install'],
          WITH_NO_INSTALL: projects['install/engine/docker-compose/with-service/with-no-install'],
          WITH_ONE_INSTALL: projects['install/engine/docker-compose/with-service/with-one-install'],
          WITH_SOME_INSTALL: projects['install/engine/docker-compose/with-service/with-some-install'],
        },
        NO_SERVICE: projects['install/engine/docker-compose/no-service'],
      },
    }

    describe(
      ...createEngineSuite(
        Engine.DOCKER_COMPOSE,
        projectsMap[Engine.DOCKER_COMPOSE],
      ),
    )
  })

  describe('Completion', () => {
    describe('Project is used', () => {
      describe('Project has services', () => {
        let dock: Dock
        let recorder: Recorder

        beforeAll(async () => {
          const instances = await createTestInstances(
            'completion-with-services',
            projects['install/with-service/with-all-install'],
          )

          dock = instances.dock
          recorder = instances.recorder
        })
        beforeEach(() => { recorder.clearRecording() })

        it('should complete if ran with no arguments', async () => {
          await dock.complete('install ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete if ran with partial service', async () => {
          await dock.complete('install service-with')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should filtered complete if ran with complete service', async () => {
          await dock.complete('install service-with-multiple ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should filtered complete if ran with multiple services', async () => {
          await dock.complete('install service service-with-multiple ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete if ran with undefined service', async () => {
          await dock.complete('install undefined-1 ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete with system completion if ran with exhausted services', async () => {
          const services = [
            'service',
            'service-with-multiple',
            'service-with-entry-point',
            'service-with-shell',
            'container-same-service',
            'container-same-service-with-entry-point-and-shell',
          ]

          await dock.complete(`install ${services.join(' ')} `)

          expect(recorder.recording).toMatchSnapshot()
        })

        describe(
          'Options are provided before command arguments',
          caseFactory.completion.options.createProvidedBeforeArguments(
            'install',
            () => ({ dock, recorder }),
            ['service', 'service-with-multiple'],
          ),
        )

        describe(
          'Default options',
          caseFactory.completion.options.createDefault(
            'install',
            () => ({ dock, recorder }),
            ['service', 'service-with-multiple'],
          ),
        )
      })

      describe('Project has no services', () => {
        let dock: Dock
        let recorder: Recorder

        beforeAll(async () => {
          const instances = await createTestInstances(
            'completion-no-service',
            projects['install/no-service'],
          )

          dock = instances.dock
          recorder = instances.recorder
        })
        beforeEach(() => { recorder.clearRecording() })

        it('should complete with system completion if ran with no arguments', async () => {
          await dock.complete('install ')

          expect(recorder.recording).toMatchSnapshot()
        })

        it('should complete with system completion if ran with arguments', async () => {
          await dock.complete('install ./path ')

          expect(recorder.recording).toMatchSnapshot()
        })

        describe(
          'Options are provided before command arguments',
          caseFactory.completion.options.createProvidedBeforeArguments(
            'install',
            () => ({ dock, recorder }),
            ['service', 'service-with-multiple'],
          ),
        )

        describe(
          'Default options',
          caseFactory.completion.options.createDefault(
            'install',
            () => ({ dock, recorder }),
            ['service', 'service-with-multiple'],
          ),
        )
      })
    })

    describe('Project is not used', () => {
      const namespace = 'completion-no-project-used'
      const { dock, recorder } = createTestInstances(namespace)

      beforeEach(() => { recorder.clearRecording() })

      it('should complete with system completion if ran with no arguments', async () => {
        await dock.complete('install ')

        expect(recorder.recording).toMatchSnapshot()
      })

      it('should complete with system completion if ran with arguments', async () => {
        await dock.complete('install ./path ')

        expect(recorder.recording).toMatchSnapshot()
      })

      describe(
        'Options are provided before command arguments',
        caseFactory.completion.options.createProvidedBeforeArguments(
          'install',
          () => ({ dock, recorder }),
          ['service', 'service-with-multiple'],
        ),
      )

      describe(
        'Default options',
        caseFactory.completion.options.createDefault(
          'install',
          () => ({ dock, recorder }),
          ['service', 'service-with-multiple'],
        ),
      )
    })
  })


  describe(
    'Auto reload',
    caseFactory.autoReload.createIntercepted(
      {
        GENERAL: { project: projects['install/with-service/with-all-install'] },
        VALID_CHANGE: {
          project: projects['install/with-service/updated-valid'],
        },
        INVALID_CHANGE: {
          project: projects['install/with-service/updated-invalid'],
        },
        REMOVED: { project: projects['install/with-service/removed'] },
      },
      {
        runValid: ({ dock }) => dock.run('install service'),
        runInvalid: ({ dock }) => dock.run('install undefined'),
      },
    ),
  )

  describe(
    'Version',
    caseFactory.version.createDefault('install', createTestInstances),
  )

  describe(
    'Help',
    caseFactory.help.createDefault('install', createTestInstances),
  )
})

type WithServiceProjectKeys = 'WITH_ALL_INSTALL'
  | 'WITH_NO_INSTALL'
  | 'WITH_ONE_INSTALL'
  | 'WITH_SOME_INSTALL'

function createEngineSuite(
  engine: Engine,
  projectsMap: {
    WITH_SERVICE: Record<WithServiceProjectKeys, Project>,
    NO_SERVICE: Project,
  },
): [Engine, Method] {
  function createNameSpace(name: string) { return `${engine}-${name}`}

  function suite() {
    describe('Command', () => {
      describe('Project is used', () => {
        describe('Project has services', () => {
          describe('With no service with install configuration', () => {
            let dock: Dock
            let recorder: Recorder

            beforeAll(async () => {
              const namespace = createNameSpace('with-service-with-no-install')
              const instances = await createTestInstances(
                namespace,
                projectsMap.WITH_SERVICE.WITH_NO_INSTALL,
              )

              dock = instances.dock
              recorder = instances.recorder
            })
            beforeEach(() => recorder.clearRecording())

            describe('It should warn that services has no install configuration when', () => {
              test('ran with no argument', async () => {
                await dock.run('install')

                expect(recorder.recording).toMatchSnapshot()
              })

              test('ran with one service', async () => {
                await dock.run('install service')

                expect(recorder.recording).toMatchSnapshot()
              })

              test('ran with multiple services', async () => {
                await dock.run('install service service-with-multiple')

                expect(recorder.recording).toMatchSnapshot()
              })
            })

            it('should emit error if ran with undefined service', async () => {
              await dock.run('install undefined-service')

              expect(recorder.recording).toMatchSnapshot()
            })
          })

          describe('With', () => {
            type WithInstallCase = [
              string,
              WithServiceProjectKeys,
              Array<string>, // Install arguments
            ]

            const cases: Array<WithInstallCase> = [
              [
                'one',
                'WITH_ONE_INSTALL',
                ['service', 'service-with-none', 'service-another-with-none'],
              ],
              [
                'some',
                'WITH_SOME_INSTALL',
                ['service', 'service-with-multiple', 'service-with-none'],
              ],
              [
                'all',
                'WITH_ALL_INSTALL',
                ['service', 'service-with-multiple', 'service-with-shell'],
              ],
            ]

            describe.each<WithInstallCase>(cases)(
              '%s service/s with install configuration',
              (quantity, project, installArguments) => {
                let dock: Dock
                let recorder: Recorder

                beforeAll(async () => {
                  const namespace
                  = createNameSpace(`with-service-with-${quantity}-install`)
                  const instances = await createTestInstances(
                    namespace,
                    projectsMap.WITH_SERVICE[project],
                  )

                  dock = instances.dock
                  recorder = instances.recorder
                })
                beforeEach(() => recorder.clearRecording())


                it('should install if ran with no argument', async () => {
                  await dock.run('install')

                  expect(recorder.recording).toMatchSnapshot()
                })

                it('should install if ran with one service', async () => {
                  const [firstArgument] = installArguments

                  await dock.run(`install ${firstArgument}`)

                  expect(recorder.recording).toMatchSnapshot()
                })

                it('should install if ran with multiple services', async () => {
                  await dock.run(`install ${installArguments.join(' ')}`)

                  expect(recorder.recording).toMatchSnapshot()
                })

                it('should emit error if ran with undefined service', async () => {
                  await dock.run('install undefined-service')

                  expect(recorder.recording).toMatchSnapshot()
                })

                if (!installArguments.includes('service-with-none')) return

                it('should emit warning if ran with one service with no install script', async () => {
                  await dock.run('install service-with-none')

                  expect(recorder.recording).toMatchSnapshot()
                })
              },
            )
          })
        })

        describe('Project has no service', () => {
          let dock: Dock
          let recorder: Recorder

          beforeAll(async () => {
            const namespace = createNameSpace('no-service')
            const instances = await createTestInstances(
              namespace,
              projectsMap.NO_SERVICE,
            )

            dock = instances.dock
            recorder = instances.recorder
          })
          beforeEach(() => recorder.clearRecording())

          describe('It should emit error if ran with', () => {
            test('no argument', async () => {
              await dock.run('install')

              expect(recorder.recording).toMatchSnapshot()
            })

            test('one argument', async () => {
              await dock.run('install service')

              expect(recorder.recording).toMatchSnapshot()
            })

            test('multiple arguments', async () => {
              await dock.run('install service service-with-multiple')

              expect(recorder.recording).toMatchSnapshot()
            })
          })
        })
      })

      describe('Project is not used', () => {
        let dock: Dock
        let recorder: Recorder

        beforeAll(async () => {
          const namespace = createNameSpace('no-project-used')
          const instances = await createTestInstances(namespace)

          dock = instances.dock
          recorder = instances.recorder
        })
        beforeEach(() => recorder.clearRecording())

        describe('It should emit error if ran with', () => {
          test('no argument', async () => {
            await dock.run('install')

            expect(recorder.recording).toMatchSnapshot()
          })

          test('one argument', async () => {
            await dock.run('install service')

            expect(recorder.recording).toMatchSnapshot()
          })

          test('multiple arguments', async () => {
            await dock.run('install service service-with-multiple')

            expect(recorder.recording).toMatchSnapshot()
          })
        })
      })
    })
  }

  return [engine, suite]
}
