export const STRING = 'string'

export const NUMBER = 1

export const BOOLEAN = true

export const SYMBOL = Symbol('symbol')

export const OBJECT = { key: 'value' }

export const ARRAY = ['entry']

export const UNDEFINED = undefined

export const NULL = null
