import * as commander from 'commander'

import { register } from '~/handlers'
import { createActionHandler, decorateWithDefaults } from '~/commands'
import { PROCESS_FAILED } from '~/config/exit-codes'
import { handleError } from '~/lib/decorators'

import type { ErrorHandler } from '~/lib/decorators/handle-error'
import type { Create } from '@/command'
import type { Emitter } from '@/clients'

export const name = 'register'

const nameAndArgs = `${name} [path]`
const description = 'Register project'
const argumentDescriptions = {
  path:
    `(Optional) Path to project's dock config. Defaults to current working directory`,
}

export const create: Create = (provisions) => {
  const handler = createActionHandler(
    handleError(register.handle, createErrorHandler(provisions.emitter)),
    register.complete,
    provisions,
  )
  const command = new commander.Command()
    .command(nameAndArgs)
    .description(description, argumentDescriptions)
    .action(handler)

  return decorateWithDefaults(command, provisions)
}

function createErrorHandler(emitter: Emitter): ErrorHandler {
  return (error) => {
    return emitter
      .emitError(`Failed to register project with error:`)
      .emitError(error.message)
      .exit(PROCESS_FAILED)
  }
}
