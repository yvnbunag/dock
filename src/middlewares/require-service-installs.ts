import { isEmpty } from 'lodash'

import { NOT_FOUND } from '~/config/exit-codes'
import { services } from '~/lib/program-helpers/projects'

import type { Middleware } from '@/middleware'

type RequireServiceInstalls = Middleware<'emitter' | 'programMediator'>

export const requireServiceInstalls: RequireServiceInstalls = async function (
  commandArguments,
  { provisions },
) {
  const installMap = await services.getInstallMap(provisions)

  if (!isEmpty(installMap)) return

  const errorMessage
    = 'There are no defined service install scripts in the project config'

  return provisions.emitter
    .emitError(errorMessage)
    .exit(NOT_FOUND)
}
