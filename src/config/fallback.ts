import { version } from '~/config/config'
import { Project } from '@/config'

import type { Program } from '@/config'

type Config = {
  PROGRAM: Program.Config,
  PROJECT: Project.Config,
}

export const rawConfig: Project.RawConfig = {
  name: '',
  engine: Project.Engine.DOCKER_COMPOSE,
  containers: {},
  services: {},
  modes: {},
}

export const config: Config = {
  PROGRAM: {
    projects: {
      metadata: {},
      names: [],
      configPaths: [],
    },
    settings: {
      active: null,
    },
  },
  PROJECT: {
    version,
    configPath: '',
    updated: '',
    rawConfig: {
      name: '',
      engine: Project.Engine.DOCKER_COMPOSE,
      containers: {},
      services: {},
      modes: {},
    },
    aggregates: {
      containers: { values: [] },
      services: { values: [] },
      modes: { values: [] },
      references: { values: [] },
    },
  },
}

export const project = {
  CONFIG: {
    name: 'dock.config.yml',
  },
}
